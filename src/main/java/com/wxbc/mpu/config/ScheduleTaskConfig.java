package com.wxbc.mpu.config;

import com.alibaba.fastjson.JSON;
import com.wxbc.mpu.gateway.huobi.HuobitUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class ScheduleTaskConfig {
    private final static Logger logger = LoggerFactory.getLogger(ScheduleTaskConfig.class);

//    @Scheduled(cron = "*/10 * * * * ?")
//    private void huobiReMapContractTypeTask() {
//
//    }
}
