package com.wxbc.mpu.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "")
public class GlobalConfig {

    private Integer subSizeLimit;

    public Integer getSubSizeLimit() {
        return subSizeLimit;
    }

    public void setSubSizeLimit(Integer subSizeLimit) {
        this.subSizeLimit = subSizeLimit;
    }
}
