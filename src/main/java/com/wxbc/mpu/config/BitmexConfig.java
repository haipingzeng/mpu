package com.wxbc.mpu.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Set;

@Configuration
@ConfigurationProperties(prefix = "bitmex")
public class BitmexConfig {

    private String futureWssUrl;

    private Integer subSizeLimit;

    private Set<String> subSymbols;

    public String getFutureWssUrl() {
        return futureWssUrl;
    }

    public void setFutureWssUrl(String futureWssUrl) {
        this.futureWssUrl = futureWssUrl;
    }

    public Integer getSubSizeLimit() {
        return subSizeLimit;
    }

    public void setSubSizeLimit(Integer subSizeLimit) {
        this.subSizeLimit = subSizeLimit;
    }

    public Set<String> getSubSymbols() {
        return subSymbols;
    }

    public void setSubSymbols(Set<String> subSymbols) {
        this.subSymbols = subSymbols;
    }
}
