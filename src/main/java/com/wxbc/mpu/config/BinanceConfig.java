package com.wxbc.mpu.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Set;

@Configuration
@ConfigurationProperties(prefix = "binance")
public class BinanceConfig {

    private String spotWssUrl;

    private String usdtFutureWssUrl;

    private String coinFutureWssUrl;

    private Integer subSizeLimit;

    private Set<String> subSymbols;

    public String getSpotWssUrl() {
        return spotWssUrl;
    }

    public void setSpotWssUrl(String spotWssUrl) {
        this.spotWssUrl = spotWssUrl;
    }

    public String getUsdtFutureWssUrl() {
        return usdtFutureWssUrl;
    }

    public void setUsdtFutureWssUrl(String usdtFutureWssUrl) {
        this.usdtFutureWssUrl = usdtFutureWssUrl;
    }

    public String getCoinFutureWssUrl() {
        return coinFutureWssUrl;
    }

    public void setCoinFutureWssUrl(String coinFutureWssUrl) {
        this.coinFutureWssUrl = coinFutureWssUrl;
    }

    public Integer getSubSizeLimit() {
        return subSizeLimit;
    }

    public void setSubSizeLimit(Integer subSizeLimit) {
        this.subSizeLimit = subSizeLimit;
    }

    public Set<String> getSubSymbols() {
        return subSymbols;
    }

    public void setSubSymbols(Set<String> subSymbols) {
        this.subSymbols = subSymbols;
    }
}
