package com.wxbc.mpu.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Set;

@Configuration
@ConfigurationProperties(prefix = "deribit")
public class DeribitConfig {

    private String unifyWssUrl;

    private Integer subSizeLimit;

    private Set<String> subSymbols;

    public String getUnifyWssUrl() {
        return unifyWssUrl;
    }

    public void setUnifyWssUrl(String unifyWssUrl) {
        this.unifyWssUrl = unifyWssUrl;
    }

    public Integer getSubSizeLimit() {
        return subSizeLimit;
    }

    public void setSubSizeLimit(Integer subSizeLimit) {
        this.subSizeLimit = subSizeLimit;
    }

    public Set<String> getSubSymbols() {
        return subSymbols;
    }

    public void setSubSymbols(Set<String> subSymbols) {
        this.subSymbols = subSymbols;
    }
}
