package com.wxbc.mpu.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Set;

@Configuration
@ConfigurationProperties(prefix = "huobi")
public class HuobiConfig {

    private String spotDepthWssUrl;

    private String spotTradeWssUrl;

    private String coinDeliveryWssUrl;

    private String coinPerpetualWssUrl;

    private String usdtPerpetualWssUrl;

    private Integer subSizeLimit;

    private Set<String> subSymbols;

    public String getSpotDepthWssUrl() {
        return spotDepthWssUrl;
    }

    public void setSpotDepthWssUrl(String spotDepthWssUrl) {
        this.spotDepthWssUrl = spotDepthWssUrl;
    }

    public String getSpotTradeWssUrl() {
        return spotTradeWssUrl;
    }

    public void setSpotTradeWssUrl(String spotTradeWssUrl) {
        this.spotTradeWssUrl = spotTradeWssUrl;
    }

    public String getCoinDeliveryWssUrl() {
        return coinDeliveryWssUrl;
    }

    public void setCoinDeliveryWssUrl(String coinDeliveryWssUrl) {
        this.coinDeliveryWssUrl = coinDeliveryWssUrl;
    }

    public String getCoinPerpetualWssUrl() {
        return coinPerpetualWssUrl;
    }

    public void setCoinPerpetualWssUrl(String coinPerpetualWssUrl) {
        this.coinPerpetualWssUrl = coinPerpetualWssUrl;
    }

    public String getUsdtPerpetualWssUrl() {
        return usdtPerpetualWssUrl;
    }

    public void setUsdtPerpetualWssUrl(String usdtPerpetualWssUrl) {
        this.usdtPerpetualWssUrl = usdtPerpetualWssUrl;
    }

    public Integer getSubSizeLimit() {
        return subSizeLimit;
    }

    public void setSubSizeLimit(Integer subSizeLimit) {
        this.subSizeLimit = subSizeLimit;
    }

    public Set<String> getSubSymbols() {
        return subSymbols;
    }

    public void setSubSymbols(Set<String> subSymbols) {
        this.subSymbols = subSymbols;
    }
}
