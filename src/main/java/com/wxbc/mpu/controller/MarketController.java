package com.wxbc.mpu.controller;

import com.alibaba.fastjson.JSON;
import com.wxbc.mpu.service.MarketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.Map;

@RestController
@RequestMapping
public class MarketController {

    private final static Logger logger = LoggerFactory.getLogger(MarketController.class);

    @Autowired
    MarketService marketService;


    @RequestMapping(value = "/sub_market", method = RequestMethod.GET)
    public Boolean subMarket(@RequestParam Map<String, String> request) {
        logger.info("sub_market request: " + JSON.toJSONString(request));
        String exchange = request.get("exchange");
        String symbol = request.get("symbol");

        return marketService.subMarket(exchange, symbol);
    }

    @RequestMapping(value = "/unsub_market", method = RequestMethod.GET)
    public Boolean unsubMarket(@RequestParam Map<String, String> request) {
        logger.info("unsub_market request: " + JSON.toJSONString(request));
        String exchange = request.get("exchange");
        String symbol = request.get("symbol");
        return marketService.unsubMarket(exchange, symbol);
    }
}
