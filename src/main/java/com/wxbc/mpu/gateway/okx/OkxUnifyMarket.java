package com.wxbc.mpu.gateway.okx;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wxbc.mpu.config.OkxConfig;
import com.wxbc.mpu.market.MarketContext;
import com.wxbc.mpu.market.SymbolMgr;
import com.wxbc.mpu.struct.WxbcSymbol;
import com.wxbc.mpu.struct.WxbcType;
import com.wxbc.mpu.utils.DateTimeUtils;
import com.wxbc.mpu.utils.WxbcUtils;
import com.wxbc.mpu.wss.WssClient;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Component
@Scope("prototype")
public class OkxUnifyMarket extends WssClient {

    private final static Logger logger = LoggerFactory.getLogger(OkxUnifyMarket.class);

    @Autowired
    OkxConfig okxConfig;

    @Autowired
    MarketContext marketContext;

    @Autowired
    SymbolMgr symbolMgr;

    private Map<String, String> wxbcOkxSymbolMap = new HashMap<>();

    private Map<String, String> okxWxbcSymbolMap = new HashMap<>();

    private Integer id;

    private Set<String> subSymbols = new ConcurrentSkipListSet<>();



    private volatile Channel channel = null;

    private LocalDateTime lastReceiveTime = DateTimeUtils.utcnow();

    private ScheduledExecutorService eventloop = new ScheduledThreadPoolExecutor(1, new BasicThreadFactory.Builder().namingPattern("OkxUnifyMarket").daemon(true).build());

    private Map<String, OkxUnifyMarketHandler> okxUnifyMarketHandlerMap = new HashMap<>();


    // OKX 混合订阅存在错误的topic不影响正确的topic, 因此不用做特殊处理
    public OkxUnifyMarket(Integer id) {
        super("OkxUnifyMarket " + id);
        this.id = id;
        setAllowClientNoContext(true);
        setRequestedServerNoContext(false);

        // 检查行情是否很久没收到行情
        eventloop.scheduleWithFixedDelay(()->{
            LocalDateTime utcnow = DateTimeUtils.utcnow();

            if ( DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(lastReceiveTime) > 5000) {
                if (getRunning() && this.channel != null && subSize() > 0) {
                    this.channel.close();
                }
            }
        }, 5, 5, TimeUnit.SECONDS);

    }

    public void start() {
        super.connect(okxConfig.getUnifyWssUrl());
    }

    public void stop() {
        super.disconnect();
    }

    public void shutdown() {
        super.shutdownGracefully();
        eventloop.shutdown();
    }

    public int subSize() {
        return subSymbols.size();
    }

    public void subMarket(String symbol) {
        logger.info("subMarket symbol: " + symbol);
        String  key = new StringBuilder(WxbcType.EXCHANGE_OKX).append("|").append(symbol).toString();
        WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
        if (wxbcSymbol != null) {

            wxbcOkxSymbolMap.put(symbol, wxbcSymbol.getExchSymbol());
            okxWxbcSymbolMap.put(wxbcSymbol.getExchSymbol(), symbol);

            logger.info("subMarket symbol: " + symbol + " exchSymbol: " + wxbcSymbol.getExchSymbol());
            subSymbols.add(symbol);
            eventloop.execute(() -> {
                if (this.channel != null) {
                    JSONObject subReq = new JSONObject();
                    subReq.put("op", "subscribe");
                    List<Map<String, String>> args = new ArrayList<>();

                    Map<String, String> subDepthArg = new HashMap<>();
                    subDepthArg.put("channel", "books50-l2-tbt");
                    subDepthArg.put("instId", wxbcSymbol.getExchSymbol());

                    Map<String, String> subTradeArg = new HashMap<>();
                    subTradeArg.put("channel", "trades");
                    subTradeArg.put("instId", wxbcSymbol.getExchSymbol());

                    args.add(subDepthArg);
                    args.add(subTradeArg);
                    subReq.put("args", args);
                    this.channel.writeAndFlush(new TextWebSocketFrame(subReq.toJSONString()));
                }
            });
        }
    }

    public void unsubMarket(String symbol) {
        String exchSymbol = getExchSymbol(symbol);
        logger.info("unsubMarket symbol: " + symbol + " exchSymbol: " + exchSymbol);
        if (exchSymbol != null) {
            subSymbols.remove(symbol);
            eventloop.execute(() -> {

            });
        }
    }


    private String getExchSymbol(String symbol) {
        return null;
    }



    @Override
    protected void onOpen(Channel channel) {

        eventloop.execute(()->{
            this.channel = channel;
            JSONObject subReq = new JSONObject();
            subReq.put("op", "subscribe");
            List<Map<String, String>> args = new ArrayList<>();
            for (String symbol: subSymbols) {
                String  key = new StringBuilder(WxbcType.EXCHANGE_OKX).append("|").append(symbol).toString();
                WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
                if (null != wxbcSymbol) {
                    Map<String, String> subDepthArg = new HashMap<>();
                    subDepthArg.put("channel", "books50-l2-tbt");
                    subDepthArg.put("instId", wxbcSymbol.getExchSymbol());

                    Map<String, String> subTradeArg = new HashMap<>();
                    subTradeArg.put("channel", "trades");
                    subTradeArg.put("instId", wxbcSymbol.getExchSymbol());

                    args.add(subDepthArg);
                    args.add(subTradeArg);
                }
            }
            subReq.put("args", args);
            this.channel.writeAndFlush(new TextWebSocketFrame(subReq.toJSONString()));
        });

    }

    @Override
    protected void onMessage(Channel channel, byte[] msg) {
    }

    @Override
    protected void onMessage(Channel channel, String msg) {
        eventloop.execute(()->handleOnMessage(channel, msg, DateTimeUtils.utcnow()));
    }

    @Override
    protected void onClose(Channel channel) {
        this.channel = null;
    }

    @Override
    protected void onThrowable(Throwable e) {
    }

    private void handleOnMessage(Channel channel, String msg, LocalDateTime arrivedTime) {
        lastReceiveTime = DateTimeUtils.utcnow();
        try {
            JSONObject jmsg = JSON.parseObject(msg);
            if (jmsg.containsKey("event")) {
                logger.info(msg);
            } else if (jmsg.containsKey("arg")) {
                String topic = jmsg.getJSONObject("arg").getString("channel");
                String exchSymbol = jmsg.getJSONObject("arg").getString("instId");

                String symbol = okxWxbcSymbolMap.get(exchSymbol);
                if (null == symbol) return;
                if (!okxUnifyMarketHandlerMap.containsKey(symbol)) {
                    okxUnifyMarketHandlerMap.put(symbol, new OkxUnifyMarketHandler(symbol, marketContext));
                }
                if (topic.startsWith("books")) {
                    String action = jmsg.getString("action");
                    if ("snapshot".equals(action)) {
                        okxUnifyMarketHandlerMap.get(symbol).handleSnapshot(arrivedTime, jmsg.getJSONArray("data").toJavaList(JSONObject.class).get(0));
                    } else if ("update".equals(action)) {
                        okxUnifyMarketHandlerMap.get(symbol).handleDelta(arrivedTime, jmsg.getJSONArray("data").toJavaList(JSONObject.class).get(0));
                    }
                } else if ("trades".equals(topic)) {
                    okxUnifyMarketHandlerMap.get(symbol).handleTrade(arrivedTime, jmsg.getJSONArray("data").toJavaList(JSONObject.class));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(msg);
        }

    }
}
