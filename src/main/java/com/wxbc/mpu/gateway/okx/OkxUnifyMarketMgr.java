package com.wxbc.mpu.gateway.okx;

import com.wxbc.mpu.app.SpringContextUtils;
import com.wxbc.mpu.config.DeribitConfig;
import com.wxbc.mpu.config.OkxConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class OkxUnifyMarketMgr {

    private final static Logger logger = LoggerFactory.getLogger(OkxUnifyMarketMgr.class);

    @Autowired
    OkxConfig okxConfig;

    private int index = 0;

    private Map<Integer, OkxUnifyMarket> okxUnifyMarketMap = new HashMap<>();

    private Map<String, Integer> symbolMarketMap = new HashMap<>();

    private ScheduledExecutorService eventloop = Executors.newScheduledThreadPool(1);


    public void subMarket(String symbol) {
        eventloop.execute(()->{
            logger.info("sub " + symbol);
            if (symbolMarketMap.containsKey(symbol)) return;
            boolean success = false;
            for (Map.Entry<Integer, OkxUnifyMarket> entry: okxUnifyMarketMap.entrySet()) {
                OkxUnifyMarket okxUnifyMarket = entry.getValue();
                if (okxUnifyMarket.subSize() < okxConfig.getSubSizeLimit()) {
                    okxUnifyMarket.subMarket(symbol);
                    symbolMarketMap.put(symbol, entry.getKey());
                    success = true;
                }
            }
            if (!success) {
                int id = ++index;
                OkxUnifyMarket okxUnifyMarket = SpringContextUtils.getBean(OkxUnifyMarket.class, id);
                okxUnifyMarket.start();
                okxUnifyMarket.subMarket(symbol);
                okxUnifyMarketMap.put(id, okxUnifyMarket);
                symbolMarketMap.put(symbol, id);
                logger.info("start market " + Integer.toString(id));
            }
        });

    }

    public void subMarket(Set<String> symbols) {
        for (String symbol: symbols) {
            this.subMarket(symbol);
        }
    }

    public void unsubMarket(String symbol) {
        eventloop.execute(()->{
            if (symbolMarketMap.containsKey(symbol)) {
                Integer id = symbolMarketMap.get(symbol);
                OkxUnifyMarket deribitUnifyMarket = okxUnifyMarketMap.get(id);
                if (null != deribitUnifyMarket) deribitUnifyMarket.unsubMarket(symbol);
                symbolMarketMap.remove(symbol);
                if (deribitUnifyMarket.subSize() <= 0) {
                    deribitUnifyMarket.shutdown();
                    okxUnifyMarketMap.remove(id);
                }
            }
        });
    }

    public void unsubMarket(Set<String> symbols) {

    }
}

