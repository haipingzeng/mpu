package com.wxbc.mpu.gateway.deribit;

import com.wxbc.mpu.app.SpringContextUtils;
import com.wxbc.mpu.config.BybitConfig;
import com.wxbc.mpu.config.DeribitConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class DeribitUnifyMarketMgr {

    private final static Logger logger = LoggerFactory.getLogger(DeribitUnifyMarketMgr.class);

    @Autowired
    DeribitConfig deribitConfig;

    private int index = 0;

    private Map<Integer, DeribitUnifyMarket> deribitUnifyMarketMap = new HashMap<>();

    private Map<String, Integer> symbolMarketMap = new HashMap<>();

    private ScheduledExecutorService eventloop = Executors.newScheduledThreadPool(1);


    public void subMarket(String symbol) {
        eventloop.execute(()->{
            logger.info("sub " + symbol);
            if (symbolMarketMap.containsKey(symbol)) return;
            boolean success = false;
            for (Map.Entry<Integer, DeribitUnifyMarket> entry: deribitUnifyMarketMap.entrySet()) {
                DeribitUnifyMarket deribitUnifyMarket = entry.getValue();
                if (deribitUnifyMarket.subSize() < deribitConfig.getSubSizeLimit()) {
                    deribitUnifyMarket.subMarket(symbol);
                    symbolMarketMap.put(symbol, entry.getKey());
                    success = true;
                }
            }
            if (!success) {
                int id = ++index;
                DeribitUnifyMarket deribitUnifyMarket = SpringContextUtils.getBean(DeribitUnifyMarket.class, id);
                deribitUnifyMarket.start();
                deribitUnifyMarket.subMarket(symbol);
                deribitUnifyMarketMap.put(id, deribitUnifyMarket);
                symbolMarketMap.put(symbol, id);
                logger.info("start market " + Integer.toString(id));
            }
        });

    }

    public void subMarket(Set<String> symbols) {
        for (String symbol: symbols) {
            this.subMarket(symbol);
        }
    }

    public void unsubMarket(String symbol) {
        eventloop.execute(()->{
            if (symbolMarketMap.containsKey(symbol)) {
                Integer id = symbolMarketMap.get(symbol);
                DeribitUnifyMarket deribitUnifyMarket = deribitUnifyMarketMap.get(id);
                if (null != deribitUnifyMarket) deribitUnifyMarket.unsubMarket(symbol);
                symbolMarketMap.remove(symbol);
                if (deribitUnifyMarket.subSize() <= 0) {
                    deribitUnifyMarket.shutdown();
                    deribitUnifyMarketMap.remove(id);
                }
            }
        });
    }

    public void unsubMarket(Set<String> symbols) {

    }
}
