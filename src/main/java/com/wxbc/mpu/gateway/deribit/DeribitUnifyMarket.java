package com.wxbc.mpu.gateway.deribit;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wxbc.mpu.config.DeribitConfig;
import com.wxbc.mpu.gateway.binance.BinanceCoinFutureMarketHandler;
import com.wxbc.mpu.market.MarketContext;
import com.wxbc.mpu.market.SymbolMgr;
import com.wxbc.mpu.struct.WxbcSymbol;
import com.wxbc.mpu.struct.WxbcType;
import com.wxbc.mpu.utils.DateTimeUtils;
import com.wxbc.mpu.utils.WxbcUtils;
import com.wxbc.mpu.wss.WssClient;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Component
@Scope("prototype")
public class DeribitUnifyMarket extends WssClient {

    private final static Logger logger = LoggerFactory.getLogger(DeribitUnifyMarket.class);

    @Autowired
    DeribitConfig deribitConfig;

    @Autowired
    MarketContext marketContext;

    @Autowired
    SymbolMgr symbolMgr;

    private Map<String, String> wxbcDeribitSymbolMap = new HashMap<>();

    private Map<String, String> deribitWxbcSymbolMap = new HashMap<>();

    private Integer id;

    private Set<String> subSymbols = new ConcurrentSkipListSet<>();



    private volatile Channel channel = null;

    private LocalDateTime lastReceiveTime = DateTimeUtils.utcnow();

    private ScheduledExecutorService eventloop = new ScheduledThreadPoolExecutor(1, new BasicThreadFactory.Builder().namingPattern("DeribitUnifyMarket").daemon(true).build());

    private Map<String, DeribitUnifyMarketHandler> deribitUnifyMarketHandlerMap = new HashMap<>();

    public DeribitUnifyMarket(Integer id) {
        super("DeribitUnifyMarket " + id);
        this.id = id;
        setAllowClientNoContext(false);
        setRequestedServerNoContext(false);
        // 检查行情是否很久没收到行情
        eventloop.scheduleWithFixedDelay(()->{
            LocalDateTime utcnow = DateTimeUtils.utcnow();
            if (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(lastReceiveTime) > 5000) {
                if (getRunning() && this.channel != null && subSize() > 0) {
                    this.channel.close();
                }
            }
        }, 5, 5, TimeUnit.SECONDS);

    }

    public void start() {
        super.connect(deribitConfig.getUnifyWssUrl());
    }

    public void stop() {
        super.disconnect();
    }

    public void shutdown() {
        super.shutdownGracefully();
        eventloop.shutdown();
    }

    public int subSize() {
        return subSymbols.size();
    }

    public void subMarket(String symbol) {
        logger.info("subMarket symbol: " + symbol);
        String  key = new StringBuilder(WxbcType.EXCHANGE_DERIBIT).append("|").append(symbol).toString();
        WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
        if (wxbcSymbol != null) {

            wxbcDeribitSymbolMap.put(symbol, wxbcSymbol.getExchSymbol());
            deribitWxbcSymbolMap.put(wxbcSymbol.getExchSymbol(), symbol);

            logger.info("subMarket symbol: " + symbol + " exchSymbol: " + wxbcSymbol.getExchSymbol());
            subSymbols.add(symbol);
            eventloop.execute(() -> {
                if (this.channel != null) {
                    JSONObject subMsg = new JSONObject();
                    subMsg.put("jsonrpc", "2.0");
                    subMsg.put("method", "public/subscribe");
                    subMsg.put("id", WxbcUtils.nextSequence());
                    JSONObject params = new JSONObject();
                    List<String> channels = new ArrayList<>();
                    // todo 订阅raw无法成功
                    channels.add(new StringBuilder("book.").append(wxbcSymbol.getExchSymbol()).append("none.20.100ms").toString());
                    channels.add(new StringBuilder("trades.").append(wxbcSymbol.getExchSymbol()).append(".100ms").toString());

                    params.put("channels", channels);
                    subMsg.put("params", params);
                    this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));
                }
            });
        }
    }

    public void unsubMarket(String symbol) {
        String exchSymbol = getExchSymbol(symbol);
        logger.info("unsubMarket symbol: " + symbol + " exchSymbol: " + exchSymbol);
        if (exchSymbol != null) {
            subSymbols.remove(symbol);
            eventloop.execute(() -> {

            });
        }
    }


    private String getExchSymbol(String symbol) {
        return null;
    }



    @Override
    protected void onOpen(Channel channel) {
        eventloop.execute(()->{
            this.channel = channel;
            JSONObject hbReq = new JSONObject();
            hbReq.put("jsonrpc", "2.0");
            hbReq.put("id", WxbcUtils.nextSequence());
            hbReq.put("method", "public/set_heartbeat");
            JSONObject hbParams = new JSONObject();
            hbParams.put("interval", 30);
            hbReq.put("params", hbParams);
            this.channel.writeAndFlush(new TextWebSocketFrame(hbReq.toJSONString()));



            JSONObject subMsg = new JSONObject();
            subMsg.put("jsonrpc", "2.0");
            subMsg.put("method", "public/subscribe");
            subMsg.put("id", WxbcUtils.nextSequence());
            JSONObject params = new JSONObject();
            List<String> channels = new ArrayList<>();
            for (String symbol : subSymbols) {
                String  key = new StringBuilder(WxbcType.EXCHANGE_DERIBIT).append("|").append(symbol).toString();
                WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
                if (null != wxbcSymbol) {
                    // todo 订阅raw无法成功
                    channels.add(new StringBuilder("book.").append(wxbcSymbol.getExchSymbol()).append(".none.20.100ms").toString());
                    channels.add(new StringBuilder("trades.").append(wxbcSymbol.getExchSymbol()).append(".100ms").toString());
                }
            }
            params.put("channels", channels);
            subMsg.put("params", params);
            logger.info(subMsg.toJSONString());
            this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));
        });

    }

    @Override
    protected void onMessage(Channel channel, byte[] msg) {
    }

    @Override
    protected void onMessage(Channel channel, String msg) {
        eventloop.execute(()->handleOnMessage(channel, msg, DateTimeUtils.utcnow()));
    }

    @Override
    protected void onClose(Channel channel) {
        eventloop.execute(()->{
            this.channel = null;
        });

    }

    @Override
    protected void onThrowable(Throwable e) {
    }

    private void handleOnMessage(Channel channel, String msg, LocalDateTime arrivedTime) {
        lastReceiveTime = DateTimeUtils.utcnow();
        try {
            JSONObject jmsg = JSON.parseObject(msg);
            JSONObject params = jmsg.getJSONObject("params");
            if (null != params) {

                String type = params.getString("type");
                String topic = params.getString("channel");
                if ("test_request".equals(type)) {
                    JSONObject hbRsp = new JSONObject();
                    hbRsp.put("jsonrpc", "2.0");
                    hbRsp.put("id", WxbcUtils.nextSequence());
                    hbRsp.put("method", "public/test");
                    Map<String, Object> hbParams = new TreeMap<>();
                    hbRsp.put("params", hbParams);
                    this.channel.writeAndFlush(new TextWebSocketFrame(hbRsp.toJSONString()));
                    return;
                } else if (null != topic) {
                    if (topic.startsWith("book.")) {
                        JSONObject jDepth = params.getJSONObject("data");
                        String symbol = deribitWxbcSymbolMap.get(jDepth.getString("instrument_name"));
                        if (null != symbol) {
                            if (!deribitUnifyMarketHandlerMap.containsKey(symbol)) {
                                deribitUnifyMarketHandlerMap.put(symbol, new DeribitUnifyMarketHandler(symbol, marketContext));
                            }
                            deribitUnifyMarketHandlerMap.get(symbol).handleDepth(arrivedTime, jDepth);
                        }
                    } else if (topic.startsWith("trades.")) {
                        List<JSONObject> jTrades = params.getJSONArray("data").toJavaList(JSONObject.class);
                        for (JSONObject jTrade : jTrades) {
                            String symbol = deribitWxbcSymbolMap.get(jTrade.getString("instrument_name"));
                            if (null != symbol) {
                                if (!deribitUnifyMarketHandlerMap.containsKey(symbol)) {
                                    deribitUnifyMarketHandlerMap.put(symbol, new DeribitUnifyMarketHandler(symbol, marketContext));
                                }
                                deribitUnifyMarketHandlerMap.get(symbol).handleTrade(arrivedTime, jTrade);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(msg);
        }

    }
}
