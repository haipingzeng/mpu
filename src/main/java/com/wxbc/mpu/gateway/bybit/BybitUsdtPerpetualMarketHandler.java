package com.wxbc.mpu.gateway.bybit;

import com.alibaba.fastjson.JSONObject;
import com.wxbc.mpu.market.MarketContext;
import com.wxbc.mpu.struct.WxbcType;
import com.wxbc.mpu.utils.DateTimeUtils;
import com.wxbc.mpu.utils.WxbcUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

public class BybitUsdtPerpetualMarketHandler {

    private final static Logger logger = LoggerFactory.getLogger(BybitUsdtPerpetualMarketHandler.class);

    private MarketContext marketContext;

    private String exchange = WxbcType.EXCHANGE_BYBIT;

    private String symbol;

    private TreeMap<BigDecimal, BigDecimal> bidMap = null;

    private TreeMap<BigDecimal, BigDecimal> askMap = null;

    private LocalDateTime lastUpdateTime = null;

    public BybitUsdtPerpetualMarketHandler(String symbol, MarketContext marketContext) {
        this.marketContext = marketContext;
        this.symbol = symbol;
    }

    public void handleSnapshot(LocalDateTime arrivedTime, JSONObject jSnapshot) {
        // 保存先前order book信息
        TreeMap<BigDecimal, BigDecimal> beforeBidMap = bidMap;
        TreeMap<BigDecimal, BigDecimal> beforeAskMap = askMap;

        // 获取最新order book信息
        bidMap = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        askMap = new TreeMap<>();
        LocalDateTime updateTime = DateTimeUtils.microsToUtcDateTime(jSnapshot.getLong("timestamp_e6"));
        for (JSONObject info : jSnapshot.getJSONObject("data").getJSONArray("order_book").toJavaList(JSONObject.class)) {
            String side = info.getString("side");
            BigDecimal price = info.getBigDecimal("price");
            BigDecimal size = info.getBigDecimal("size");
            if ("Buy".equals(side)) {
                bidMap.put(price, size);
            } else if ("Sell".equals(side)) {
                askMap.put(price, size);
            }
        }
        // 统计先前order book和最新order book的差别信息（即增量信息）
        // delete的价位数据： 先前order book存在的，当前order book不存在的
        // insert的价位数据： 先前order book不存在，当前order book存在
        // update的价位数据： 先前order book，当前order book都存在，但是volume不一样
        TreeMap<String, BigDecimal> updateBidMap = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        TreeMap<String, BigDecimal> updateAskMap = new TreeMap<>();
        int idx = 0;
        if (beforeBidMap != null) {
            for (Map.Entry<BigDecimal, BigDecimal> entry : beforeBidMap.entrySet()) {
                if (!bidMap.containsKey(entry.getKey())) {
                    updateBidMap.put(entry.getKey().toString(), BigDecimal.ZERO);
                } else {
                    BigDecimal qty = bidMap.get(entry.getKey());
                    if (entry.getValue().compareTo(qty) != 0) {
                        updateBidMap.put(entry.getKey().toString(), qty);
                    }
                }
                if (++idx >= 25) break;
            }
        }
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry : bidMap.entrySet()) {
            if (beforeBidMap == null || !beforeBidMap.containsKey(entry.getKey())) {
                updateBidMap.put(entry.getKey().toString(), entry.getValue());
            }
            if (++idx >= 25) break;
        }
        idx = 0;
        if (beforeAskMap != null) {
            for (Map.Entry<BigDecimal, BigDecimal> entry : beforeAskMap.entrySet()) {
                if (!askMap.containsKey(entry.getKey())) {
                    updateAskMap.put(entry.getKey().toString(), BigDecimal.ZERO);
                } else {
                    BigDecimal qty = askMap.get(entry.getKey());
                    if (entry.getValue().compareTo(qty) != 0) {
                        updateAskMap.put(entry.getKey().toString(), qty);
                    }
                }
                if (++idx >= 25) break;
            }
        }
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry : askMap.entrySet()) {
            if (beforeAskMap == null || !beforeAskMap.containsKey(entry.getKey())) {
                updateAskMap.put(entry.getKey().toString(), entry.getValue());
            }
            if (++idx >= 25) break;
        }
        // 推送增量信息
        marketContext.publishDelta(WxbcUtils.nextSequence(), exchange, symbol, updateTime, arrivedTime, updateBidMap, updateAskMap);

        // 获取最新order book的前25档信息
        TreeMap<String, BigDecimal> bidDepth = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        TreeMap<String, BigDecimal> askDepth = new TreeMap<>();
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry: bidMap.entrySet()) {
            bidDepth.put(entry.getKey().toString(), entry.getValue());
            if (++idx >= 25) break;
        }
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry: askMap.entrySet()) {
            askDepth.put(entry.getKey().toString(), entry.getValue());
            if (++idx >= 25) break;
        }
        // 推送跨日的快照信息
        Long msgSeq = WxbcUtils.nextSequence();
        if (lastUpdateTime == null || !DateTimeUtils.fmtDate(lastUpdateTime, "yyyy-MM-dd").equals(DateTimeUtils.fmtDate(arrivedTime, "yyyy-MM-dd"))) {
            boolean ret = marketContext.updateCrossingSnapshot(msgSeq, exchange, symbol, updateTime, arrivedTime, bidDepth, askDepth);
            if (ret) lastUpdateTime = arrivedTime;
        }
        // 推送最新order book
        marketContext.publishDepth(msgSeq, exchange, symbol, updateTime, arrivedTime, bidDepth, askDepth);
    }

    public void handleDelta(LocalDateTime arrivedTime, JSONObject jDelta) {
        TreeMap<String, BigDecimal> updateBidMap = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        TreeMap<String, BigDecimal> updateAskMap = new TreeMap<>();

        LocalDateTime updateTime = DateTimeUtils.microsToUtcDateTime(jDelta.getLong("timestamp_e6"));
        for (JSONObject delete: jDelta.getJSONObject("data").getJSONArray("delete").toJavaList(JSONObject.class)) {
            String side = delete.getString("side");
            BigDecimal price = delete.getBigDecimal("price");
            if ("Buy".equals(side)) {
                bidMap.remove(price);
                updateBidMap.put(price.toString(), BigDecimal.ZERO);
            } else if ("Sell".equals(side)) {
                askMap.remove(price);
                updateAskMap.put(price.toString(), BigDecimal.ZERO);
            }
        }
        for (JSONObject delete: jDelta.getJSONObject("data").getJSONArray("update").toJavaList(JSONObject.class)) {
            String side = delete.getString("side");
            BigDecimal price = delete.getBigDecimal("price");
            BigDecimal size = delete.getBigDecimal("size");
            if ("Buy".equals(side)) {
                bidMap.put(price, size);
                updateBidMap.put(price.toString(), size);

                BigDecimal bfk = bidMap.firstKey();
                while (askMap.size() > 0 && askMap.firstKey().compareTo(bfk) <= 0) {
                    updateAskMap.put(askMap.firstKey().toString(), BigDecimal.ZERO);
                    askMap.remove(askMap.firstKey());

                }
            } else if ("Sell".equals(side)) {
                askMap.put(price, size);
                updateAskMap.put(price.toString(), size);

                BigDecimal afk = askMap.firstKey();
                while (bidMap.size() > 0 && bidMap.firstKey().compareTo(afk) >= 0) {
                    updateBidMap.put(bidMap.firstKey().toString(), BigDecimal.ZERO);
                    bidMap.remove(bidMap.firstKey());
                }
            }
        }
        for (JSONObject delete: jDelta.getJSONObject("data").getJSONArray("insert").toJavaList(JSONObject.class)) {
            String side = delete.getString("side");
            BigDecimal price = delete.getBigDecimal("price");
            BigDecimal size = delete.getBigDecimal("size");
            if ("Buy".equals(side)) {
                bidMap.put(price, size);
                updateBidMap.put(price.toString(), size);

                BigDecimal bfk = bidMap.firstKey();
                while (askMap.size() > 0 && askMap.firstKey().compareTo(bfk) <= 0) {
                    updateAskMap.put(askMap.firstKey().toString(), BigDecimal.ZERO);
                    askMap.remove(askMap.firstKey());

                }
            } else if ("Sell".equals(side)) {
                askMap.put(price, size);
                updateAskMap.put(price.toString(), size);

                BigDecimal afk = askMap.firstKey();
                while (bidMap.size() > 0 && bidMap.firstKey().compareTo(afk) >= 0) {
                    updateBidMap.put(bidMap.firstKey().toString(), BigDecimal.ZERO);
                    bidMap.remove(bidMap.firstKey());
                }
            }
        }
        marketContext.publishDelta(WxbcUtils.nextSequence(), exchange, symbol, updateTime, arrivedTime, updateBidMap, updateAskMap);

        TreeMap<String, BigDecimal> bidDepth = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        TreeMap<String, BigDecimal> askDepth = new TreeMap<>();
        int idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry: bidMap.entrySet()) {
            bidDepth.put(entry.getKey().toString(), entry.getValue());
            if (++idx >= 25) break;
        }
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry: askMap.entrySet()) {
            askDepth.put(entry.getKey().toString(), entry.getValue());
            if (++idx >= 25) break;
        }
        Long msgSeq = WxbcUtils.nextSequence();
        if (lastUpdateTime == null || !DateTimeUtils.fmtDate(lastUpdateTime, "yyyy-MM-dd").equals(DateTimeUtils.fmtDate(arrivedTime, "yyyy-MM-dd"))) {
            boolean ret = marketContext.updateCrossingSnapshot(msgSeq, exchange, symbol, updateTime, arrivedTime, bidDepth, askDepth);
            if (ret) lastUpdateTime = arrivedTime;
        }
        marketContext.publishDepth(msgSeq, exchange, symbol, updateTime, arrivedTime, bidDepth, askDepth);
    }

    public void handleTrade(LocalDateTime arrivedTime, List<JSONObject> jTrades) {
        for (JSONObject jTrade : jTrades) {
            LocalDateTime updateTime = DateTimeUtils.millisToUtcDateTime(jTrade.getLong("trade_time_ms"));
            String side = jTrade.getString("side");
            BigDecimal price = jTrade.getBigDecimal("price");
            BigDecimal size = jTrade.getBigDecimal("size");
            marketContext.publishTrade(exchange, symbol, updateTime, arrivedTime, side, price, size);
        }
    }
}
