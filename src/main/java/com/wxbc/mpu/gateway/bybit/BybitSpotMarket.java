package com.wxbc.mpu.gateway.bybit;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wxbc.mpu.config.BybitConfig;
import com.wxbc.mpu.market.MarketContext;
import com.wxbc.mpu.struct.SubRecord;
import com.wxbc.mpu.utils.DateTimeUtils;
import com.wxbc.mpu.utils.WxbcUtils;
import com.wxbc.mpu.wss.WssClient;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;

@Component
@Scope("prototype")
public class BybitSpotMarket extends WssClient {

    private final static Logger logger = LoggerFactory.getLogger(BybitSpotMarket.class);

    @Autowired
    BybitConfig bybitConfig;

    @Autowired
    MarketContext marketContext;

    private Integer id;

    private Set<String> subSymbols = new ConcurrentSkipListSet<>();

    private Map<String, String> wxbcBybitSymbolMap = new HashMap<>();

    private Map<String, String> bybitWxbcSymbolMap = new HashMap<>();

    private volatile Channel channel = null;

    private LocalDateTime lastReceiveTime = DateTimeUtils.utcnow();

    private ScheduledExecutorService eventloop = new ScheduledThreadPoolExecutor(1, new BasicThreadFactory.Builder().namingPattern("BybitSpotMarket").daemon(true).build());

    private Map<String, BybitSpotMarketHandler> bybitSpotMarketHandlerMap = new HashMap<>();

    private Map<String, SubRecord> subDepthRecordMap = new HashMap<>();

    private Map<String, SubRecord> subTradeRecordMap = new HashMap<>();

    public BybitSpotMarket(Integer id) {
        super("BybitSpotMarket " + id);
        this.id = id;
        setAllowClientNoContext(true);
        setRequestedServerNoContext(true);
        // 检查行情是否很久没收到行情
        eventloop.scheduleWithFixedDelay(()->{
            LocalDateTime utcnow = DateTimeUtils.utcnow();
            if (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(lastReceiveTime) > 5000) {
                if (getRunning() && this.channel != null && subSize() > 0) {
                    this.channel.close();
                }
            }
        }, 5, 5, TimeUnit.SECONDS);

        eventloop.scheduleWithFixedDelay(()->{
            if (this.channel != null) {
                JSONObject pingMsg = new JSONObject();
                pingMsg.put("ping", System.currentTimeMillis());
                this.channel.writeAndFlush(new TextWebSocketFrame(pingMsg.toJSONString()));
            }
        }, 8, 8, TimeUnit.SECONDS);


        // 订阅失败继续订阅，最多订阅三次
        eventloop.scheduleWithFixedDelay(()->{
            try {
                if (null == this.channel) return;
                LocalDateTime utcnow = DateTimeUtils.utcnow();
                {
                    var it = subDepthRecordMap.entrySet().iterator();
                    while (it.hasNext()) {
                        var entry = it.next();
                        if (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(entry.getValue().getTime()) > 8000) {
                            if (entry.getValue().getState() > 0) {
                                entry.getValue().setState(entry.getValue().getState() - 1);

                                JSONObject subMsg = new JSONObject();
                                subMsg.put("topic", "depth");
                                subMsg.put("event", "sub");
                                JSONObject params = new JSONObject();
                                params.put("symbol", entry.getValue().getSymbol());
                                params.put("binary", true);
                                subMsg.put("params", params);
                                this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));
                            }
                            if (entry.getValue().getState() <= 0) {
                                it.remove();
                            }
                        }
                    }
                }
                {
                    var it = subTradeRecordMap.entrySet().iterator();
                    while (it.hasNext()) {
                        var entry = it.next();
                        if (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(entry.getValue().getTime()) > 8000) {
                            if (entry.getValue().getState() > 0) {
                                entry.getValue().setState(entry.getValue().getState() - 1);

                                JSONObject subTradeMsg = new JSONObject();
                                subTradeMsg.put("topic", "trade");
                                subTradeMsg.put("event", "sub");
                                JSONObject tradeParams = new JSONObject();
                                tradeParams.put("symbol", entry.getValue().getSymbol());
                                tradeParams.put("binary", true);
                                subTradeMsg.put("params", tradeParams);
                                this.channel.writeAndFlush(new TextWebSocketFrame(subTradeMsg.toJSONString()));
                            }
                            if (entry.getValue().getState() <= 0) {
                                it.remove();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, 20, 20, TimeUnit.SECONDS);

    }

    public void start() {
        super.connect(bybitConfig.getSpotWssUrl());
    }

    public void stop() {
        super.disconnect();
    }

    public void shutdown() {
        super.shutdownGracefully();
        eventloop.shutdown();
    }

    public int subSize() {
        return subSymbols.size();
    }

    public void subMarket(String symbol) {
        String exchSymbol = getExchSymbol(symbol);
        logger.info("subMarket symbol: " + symbol + " exchSymbol: " + exchSymbol);
        if (exchSymbol != null) {
            subSymbols.add(symbol);
            eventloop.execute(() -> {
                subDepthRecordMap.put(exchSymbol, new SubRecord(exchSymbol, DateTimeUtils.utcnow()));
                subTradeRecordMap.put(exchSymbol, new SubRecord(exchSymbol, DateTimeUtils.utcnow()));
                if (this.channel != null) {
                    JSONObject subMsg = new JSONObject();
                    subMsg.put("topic", "depth");
                    subMsg.put("event", "sub");
                    JSONObject params = new JSONObject();
                    params.put("symbol", exchSymbol);
                    params.put("binary", true);
                    subMsg.put("params", params);
                    this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));
                    JSONObject subTradeMsg = new JSONObject();
                    subTradeMsg.put("topic", "trade");
                    subTradeMsg.put("event", "sub");
                    JSONObject tradeParams = new JSONObject();
                    tradeParams.put("symbol", exchSymbol);
                    tradeParams.put("binary", true);
                    subTradeMsg.put("params", tradeParams);
                    this.channel.writeAndFlush(new TextWebSocketFrame(subTradeMsg.toJSONString()));
                }
            });
        }
    }

    public void unsubMarket(String symbol) {
        String exchSymbol = getExchSymbol(symbol);
        logger.info("unsubMarket symbol: " + symbol + " exchSymbol: " + exchSymbol);
        if (exchSymbol != null) {
            subSymbols.remove(symbol);
            eventloop.execute(() -> {

            });
        }
    }


    private String getExchSymbol(String symbol) {
        try {
            if (wxbcBybitSymbolMap.containsKey(symbol)) return wxbcBybitSymbolMap.get(symbol);
            String exchSymbol = symbol.replace("_", "");
            wxbcBybitSymbolMap.put(symbol, exchSymbol);
            bybitWxbcSymbolMap.put(exchSymbol, symbol);
            return exchSymbol;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    @Override
    protected void onOpen(Channel channel) {
        eventloop.execute(()->{
            this.channel = channel;
            subDepthRecordMap = new HashMap<>();
            subTradeRecordMap = new HashMap<>();
            for (String symbol: subSymbols) {
                String exchSymbol = getExchSymbol(symbol);
                if (null == exchSymbol) continue;

                subDepthRecordMap.put(exchSymbol, new SubRecord(exchSymbol, DateTimeUtils.utcnow()));
                subTradeRecordMap.put(exchSymbol, new SubRecord(exchSymbol, DateTimeUtils.utcnow()));

                JSONObject subMsg = new JSONObject();
                subMsg.put("topic", "depth");
                subMsg.put("event", "sub");
                JSONObject params = new JSONObject();
                params.put("symbol", exchSymbol);
                params.put("binary", true);
                subMsg.put("params", params);
                this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));
                JSONObject subTradeMsg = new JSONObject();
                subTradeMsg.put("topic", "trade");
                subTradeMsg.put("event", "sub");
                JSONObject tradeParams = new JSONObject();
                tradeParams.put("symbol", exchSymbol);
                tradeParams.put("binary", true);
                subTradeMsg.put("params", params);
                this.channel.writeAndFlush(new TextWebSocketFrame(subTradeMsg.toJSONString()));
            }
        });

    }

    @Override
    protected void onMessage(Channel channel, byte[] msg) {
        eventloop.execute(()->handleOnMessage(channel, msg, DateTimeUtils.utcnow()));
    }

    @Override
    protected void onMessage(Channel channel, String msg) {
        eventloop.execute(()->handleOnMessage(channel, msg, DateTimeUtils.utcnow()));
    }

    @Override
    protected void onClose(Channel channel) {
        eventloop.execute(()->{
            this.channel = null;
        });

    }

    @Override
    protected void onThrowable(Throwable e) {
    }

    private void handleOnMessage(Channel channel, byte[] msg, LocalDateTime arrivedTime) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ByteArrayInputStream in = new ByteArrayInputStream(msg);
            GZIPInputStream gunzip = new GZIPInputStream(in);
            byte[] buffer = new byte[256];
            int n;
            while ((n = gunzip.read(buffer)) >= 0) {
                out.write(buffer, 0, n);
            }
            JSONObject jmsg = JSON.parseObject(out.toString());



            String event = jmsg.getString("event");
            String topic = jmsg.getString("topic");
            if ("sub".equals(event)) {
                logger.info(jmsg.toJSONString());
                if ("0".equals(jmsg.getString("code"))) {
                    if ("depth".equals(topic)) {
                        subDepthRecordMap.remove(jmsg.getJSONObject("params").getString("symbol"));
                    } else if ("trade".equals(topic)) {
                        subTradeRecordMap.remove(jmsg.getJSONObject("params").getString("symbol"));
                    }
                }
            } else if ("cancel".equals(event)) {

            } else if ("depth".equals(topic)) {
                JSONObject jData = jmsg.getJSONObject("data");
//                subDepthRecordMap.remove(jData.getString("s"));
                String symbol = bybitWxbcSymbolMap.get(jData.getString("s"));
                if (null != symbol) {
                    if (!bybitSpotMarketHandlerMap.containsKey(symbol)) {
                        bybitSpotMarketHandlerMap.put(symbol, new BybitSpotMarketHandler(symbol, marketContext));
                    }
                    bybitSpotMarketHandlerMap.get(symbol).handleDepth(arrivedTime, jData);
                }
            } else if ("trade".equals(topic)) {
                JSONObject jData = jmsg.getJSONObject("data");
//                subTradeRecordMap.remove(jmsg.getJSONObject("params").getString("symbol"));
                String symbol = bybitWxbcSymbolMap.get(jmsg.getJSONObject("params").getString("symbol"));
                if (null != symbol) {
                    if (!bybitSpotMarketHandlerMap.containsKey(symbol)) {
                        bybitSpotMarketHandlerMap.put(symbol, new BybitSpotMarketHandler(symbol, marketContext));
                    }
                    bybitSpotMarketHandlerMap.get(symbol).handleTrade(arrivedTime, jData);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        lastReceiveTime = DateTimeUtils.utcnow();
    }

    private void handleOnMessage(Channel channel, String msg, LocalDateTime arrivedTime) {
        try {
            JSONObject jmsg = JSON.parseObject(msg);
            if (!jmsg.containsKey("pong")) logger.info(jmsg.toJSONString());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(msg);
        }
    }
}
