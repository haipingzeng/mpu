package com.wxbc.mpu.gateway.bybit;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wxbc.mpu.config.BybitConfig;
import com.wxbc.mpu.gateway.huobi.HuobiSubRecord;
import com.wxbc.mpu.market.MarketContext;
import com.wxbc.mpu.struct.SubRecord;
import com.wxbc.mpu.utils.DateTimeUtils;
import com.wxbc.mpu.utils.WxbcUtils;
import com.wxbc.mpu.wss.WssClient;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Component
@Scope("prototype")
public class BybitCoinPerpetualMarket extends WssClient {

    private final static Logger logger = LoggerFactory.getLogger(BybitCoinPerpetualMarket.class);

    @Autowired
    BybitConfig bybitConfig;

    @Autowired
    MarketContext marketContext;

    private Integer id;

    private Set<String> subSymbols = new ConcurrentSkipListSet<>();

    private Map<String, String> wxbcBybitSymbolMap = new HashMap<>();

    private Map<String, String> bybitWxbcSymbolMap = new HashMap<>();

    private volatile Channel channel = null;

    private LocalDateTime lastReceiveTime = DateTimeUtils.utcnow();

    private ScheduledExecutorService eventloop = new ScheduledThreadPoolExecutor(1, new BasicThreadFactory.Builder().namingPattern("BybitCoinPerpetualMarket").daemon(true).build());

    private Map<String, BybitCoinPerpetualMarketHandler> bybitCoinPerpetualMarketHandlerMap = new HashMap<>();

    private Map<String, SubRecord> subRecordMap = new HashMap<>();

    public BybitCoinPerpetualMarket(Integer id) {
        super("BybitCoinPerpetualMarket " + id);
        this.id = id;
        setAllowClientNoContext(true);
        setRequestedServerNoContext(true);
        // 检查行情是否很久没收到行情
        eventloop.scheduleWithFixedDelay(()->{
            LocalDateTime utcnow = DateTimeUtils.utcnow();
            if (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(lastReceiveTime) > 5000) {
                if (getRunning() && this.channel != null && subSize() > 0) {
                    this.channel.close();
                }
            }
        }, 5, 5, TimeUnit.SECONDS);

        eventloop.scheduleWithFixedDelay(()->{
            if (this.channel != null) {
                JSONObject pingMsg = new JSONObject();
                pingMsg.put("op", "ping");
                this.channel.writeAndFlush(new TextWebSocketFrame(pingMsg.toJSONString()));
            }
        }, 28, 28, TimeUnit.SECONDS);

        // 订阅失败继续订阅，最多订阅三次
        eventloop.scheduleWithFixedDelay(()->{
            try {
                if (null == this.channel) return;
                LocalDateTime utcnow = DateTimeUtils.utcnow();

                var it = subRecordMap.entrySet().iterator();
                while (it.hasNext()) {
                    var entry = it.next();
                    if (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(entry.getValue().getTime()) > 8000) {
                        if (entry.getValue().getState() > 0) {
                            entry.getValue().setState(entry.getValue().getState() - 1);

                            JSONObject subMsg = new JSONObject();
                            subMsg.put("op", "subscribe");
                            List<String> topics = new ArrayList<>();
                            topics.add("orderBookL2_25." + entry.getValue().getSymbol());
                            topics.add("trade." + entry.getValue().getSymbol());
                            subMsg.put("args", topics);
                            this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));
                        }
                        if (entry.getValue().getState() <= 0) {
                            it.remove();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, 20, 20, TimeUnit.SECONDS);

    }

    public void start() {
        super.connect(bybitConfig.getCoinPerpetualWssUrl());
    }

    public void stop() {
        super.disconnect();
    }

    public void shutdown() {
        super.shutdownGracefully();
        eventloop.shutdown();
    }

    public int subSize() {
        return subSymbols.size();
    }

    public void subMarket(String symbol) {
        String exchSymbol = getExchSymbol(symbol);
        logger.info("subMarket symbol: " + symbol + " exchSymbol: " + exchSymbol);
        if (exchSymbol != null) {
            subSymbols.add(symbol);
            eventloop.execute(() -> {
                subRecordMap.put(exchSymbol, new SubRecord(exchSymbol, DateTimeUtils.utcnow()));
                if (this.channel != null) {
                    JSONObject subMsg = new JSONObject();
                    subMsg.put("op", "subscribe");
                    List<String> topics = new ArrayList<>();
                    topics.add("orderBookL2_25." + exchSymbol);
                    topics.add("trade." + exchSymbol);
                    subMsg.put("args", topics);
                    this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));
                }
            });
        }
    }

    public void unsubMarket(String symbol) {
        String exchSymbol = getExchSymbol(symbol);
        logger.info("unsubMarket symbol: " + symbol + " exchSymbol: " + exchSymbol);
        if (exchSymbol != null) {
            subSymbols.remove(symbol);
            eventloop.execute(() -> {

            });
        }
    }


    private String getExchSymbol(String symbol) {
        try {
            if (wxbcBybitSymbolMap.containsKey(symbol)) return wxbcBybitSymbolMap.get(symbol);
            String exchSymbol = symbol.replace("_USD@P", "USD");
            wxbcBybitSymbolMap.put(symbol, exchSymbol);
            bybitWxbcSymbolMap.put(exchSymbol, symbol);
            return exchSymbol;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    @Override
    protected void onOpen(Channel channel) {
        eventloop.execute(()->{
            this.channel = channel;
            subRecordMap = new HashMap<>();
            for (String symbol: subSymbols) {
                String exchSymbol = getExchSymbol(symbol);
                if (null == exchSymbol) continue;
                subRecordMap.put(exchSymbol, new SubRecord(exchSymbol, DateTimeUtils.utcnow()));
                JSONObject subMsg = new JSONObject();
                subMsg.put("op", "subscribe");
                List<String> topics = new ArrayList<>();

                topics.add("orderBookL2_25." + exchSymbol);
                topics.add("trade." + exchSymbol);
                subMsg.put("args", topics);
                this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));
            }

        });

    }

    @Override
    protected void onMessage(Channel channel, byte[] msg) {
    }

    @Override
    protected void onMessage(Channel channel, String msg) {
        eventloop.execute(()->handleOnMessage(channel, msg, DateTimeUtils.utcnow()));
    }

    @Override
    protected void onClose(Channel channel) {
        eventloop.execute(()->{
            this.channel = null;
        });

    }

    @Override
    protected void onThrowable(Throwable e) {
    }

    private void handleOnMessage(Channel channel, String msg, LocalDateTime arrivedTime) {
        try {
            JSONObject jmsg = JSON.parseObject(msg);
//            logger.info(jmsg.toJSONString());
            if (jmsg.containsKey("topic")) {
                String topicInfos[] = jmsg.getString("topic").split("\\.");
                if (topicInfos.length == 2) {
                    if ("orderBookL2_25".equals(topicInfos[0])) {
//                        subRecordMap.remove(topicInfos[1]);
                        String symbol = bybitWxbcSymbolMap.get(topicInfos[1]);
                        if ("snapshot".equals(jmsg.getString("type"))) {
                            if (null != symbol) {
                                if (!bybitCoinPerpetualMarketHandlerMap.containsKey(symbol)) {
                                    bybitCoinPerpetualMarketHandlerMap.put(symbol, new BybitCoinPerpetualMarketHandler(symbol, marketContext));
                                }
                                bybitCoinPerpetualMarketHandlerMap.get(symbol).handleSnapshot(arrivedTime, jmsg);
                            }
                        } else if ("delta".equals(jmsg.getString("type"))) {
                            bybitCoinPerpetualMarketHandlerMap.get(symbol).handleDelta(arrivedTime, jmsg);
                        }
                    } else if ("trade".equals(topicInfos[0])) {
//                        subRecordMap.remove(topicInfos[1]);
                        String symbol = bybitWxbcSymbolMap.get(topicInfos[1]);
                        if (null != symbol) {
                            if (!bybitCoinPerpetualMarketHandlerMap.containsKey(symbol)) {
                                bybitCoinPerpetualMarketHandlerMap.put(symbol, new BybitCoinPerpetualMarketHandler(symbol, marketContext));
                            }
                            bybitCoinPerpetualMarketHandlerMap.get(symbol).handleTrade(arrivedTime, jmsg.getJSONArray("data").toJavaList(JSONObject.class));
                        }
                    }
                }
            } else if (jmsg.containsKey("ret_msg") && !"pong".equals(jmsg.getString("ret_msg"))) {
                logger.info(id + " " + jmsg.toJSONString());
                if (jmsg.getBoolean("success")) {
                    JSONObject request = jmsg.getJSONObject("request");
                    if (null != request) {
                        List<String> args = request.getJSONArray("args").toJavaList(String.class);
                        if (null != args && args.size() > 0) {
                            String[] infos = args.get(0).split("\\.");
                            if (infos.length >= 2) {
                                subRecordMap.remove(infos[1]);
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(msg);
        }
        lastReceiveTime = DateTimeUtils.utcnow();
    }
}
