package com.wxbc.mpu.gateway.bybit;

import com.wxbc.mpu.app.SpringContextUtils;
import com.wxbc.mpu.config.BybitConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class BybitCoinPerpetualMarketMgr {

    private final static Logger logger = LoggerFactory.getLogger(BybitCoinPerpetualMarketMgr.class);

    @Autowired
    BybitConfig bybitConfig;

    private int index = 0;

    private Map<Integer, BybitCoinPerpetualMarket> bybitCoinPerpetualMarketMap = new HashMap<>();

    private Map<String, Integer> symbolMarketMap = new HashMap<>();

    private ScheduledExecutorService eventloop = Executors.newScheduledThreadPool(1);


    public void subMarket(String symbol) {
        eventloop.execute(()->{
            logger.info("sub " + symbol);
            if (symbolMarketMap.containsKey(symbol)) return;
            boolean success = false;
            for (Map.Entry<Integer, BybitCoinPerpetualMarket> entry: bybitCoinPerpetualMarketMap.entrySet()) {
                BybitCoinPerpetualMarket bybitCoinPerpetualMarket = entry.getValue();
                if (bybitCoinPerpetualMarket.subSize() < bybitConfig.getSubSizeLimit()) {
                    bybitCoinPerpetualMarket.subMarket(symbol);
                    symbolMarketMap.put(symbol, entry.getKey());
                    success = true;
                }
            }
            if (!success) {
                int id = ++index;
                BybitCoinPerpetualMarket bybitCoinPerpetualMarket = SpringContextUtils.getBean(BybitCoinPerpetualMarket.class, id);
                bybitCoinPerpetualMarket.start();
                bybitCoinPerpetualMarket.subMarket(symbol);
                bybitCoinPerpetualMarketMap.put(id, bybitCoinPerpetualMarket);
                symbolMarketMap.put(symbol, id);
                logger.info("start market " + Integer.toString(id));
            }
        });

    }

    public void subMarket(Set<String> symbols) {
        for (String symbol: symbols) {
            this.subMarket(symbol);
        }
    }

    public void unsubMarket(String symbol) {
        eventloop.execute(()->{
            if (symbolMarketMap.containsKey(symbol)) {
                Integer id = symbolMarketMap.get(symbol);
                BybitCoinPerpetualMarket bybitCoinPerpetualMarket = bybitCoinPerpetualMarketMap.get(id);
                if (null != bybitCoinPerpetualMarket) bybitCoinPerpetualMarket.unsubMarket(symbol);
                symbolMarketMap.remove(symbol);
                if (bybitCoinPerpetualMarket.subSize() <= 0) {
                    bybitCoinPerpetualMarket.shutdown();
                    bybitCoinPerpetualMarketMap.remove(id);
                }
            }
        });
    }

    public void unsubMarket(Set<String> symbols) {

    }
}
