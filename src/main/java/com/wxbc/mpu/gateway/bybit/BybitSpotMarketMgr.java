package com.wxbc.mpu.gateway.bybit;

import com.wxbc.mpu.app.SpringContextUtils;
import com.wxbc.mpu.config.BybitConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class BybitSpotMarketMgr {

    private final static Logger logger = LoggerFactory.getLogger(BybitSpotMarketMgr.class);

    @Autowired
    BybitConfig bybitConfig;

    private int index = 0;

    private Map<Integer, BybitSpotMarket> bybitSpotMarketMap = new HashMap<>();

    private Map<String, Integer> symbolMarketMap = new HashMap<>();

    private ScheduledExecutorService eventloop = Executors.newScheduledThreadPool(1);


    public void subMarket(String symbol) {
        eventloop.execute(()->{
            logger.info("sub " + symbol);
            if (symbolMarketMap.containsKey(symbol)) return;
            boolean success = false;
            for (Map.Entry<Integer, BybitSpotMarket> entry: bybitSpotMarketMap.entrySet()) {
                BybitSpotMarket bybitSpotMarket = entry.getValue();
                if (bybitSpotMarket.subSize() < bybitConfig.getSubSizeLimit()) {
                    bybitSpotMarket.subMarket(symbol);
                    symbolMarketMap.put(symbol, entry.getKey());
                    success = true;
                }
            }
            if (!success) {
                int id = ++index;
                BybitSpotMarket bybitSpotMarket = SpringContextUtils.getBean(BybitSpotMarket.class, id);
                bybitSpotMarket.start();
                bybitSpotMarket.subMarket(symbol);
                bybitSpotMarketMap.put(id, bybitSpotMarket);
                symbolMarketMap.put(symbol, id);
                logger.info("start market " + Integer.toString(id));
            }
        });

    }

    public void subMarket(Set<String> symbols) {
        for (String symbol: symbols) {
            this.subMarket(symbol);
        }
    }

    public void unsubMarket(String symbol) {
        eventloop.execute(()->{
            if (symbolMarketMap.containsKey(symbol)) {
                Integer id = symbolMarketMap.get(symbol);
                BybitSpotMarket bybitSpotMarket = bybitSpotMarketMap.get(id);
                if (null != bybitSpotMarket) bybitSpotMarket.unsubMarket(symbol);
                symbolMarketMap.remove(symbol);
                if (bybitSpotMarket.subSize() <= 0) {
                    bybitSpotMarket.shutdown();
                    bybitSpotMarketMap.remove(id);
                }
            }
        });
    }

    public void unsubMarket(Set<String> symbols) {

    }
}
