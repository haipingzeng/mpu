package com.wxbc.mpu.gateway.bybit;

import com.wxbc.mpu.app.SpringContextUtils;
import com.wxbc.mpu.config.BybitConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class BybitUsdtPerpetualMarketMgr {

    private final static Logger logger = LoggerFactory.getLogger(BybitUsdtPerpetualMarketMgr.class);

    @Autowired
    BybitConfig bybitConfig;

    private int index = 0;

    private Map<Integer, BybitUsdtPerpetualMarket> bybitUsdtPerpetualMarketMap = new HashMap<>();

    private Map<String, Integer> symbolMarketMap = new HashMap<>();

    private ScheduledExecutorService eventloop = Executors.newScheduledThreadPool(1);


    public void subMarket(String symbol) {
        eventloop.execute(()->{
            logger.info("sub " + symbol);
            if (symbolMarketMap.containsKey(symbol)) return;
            boolean success = false;
            for (Map.Entry<Integer, BybitUsdtPerpetualMarket> entry: bybitUsdtPerpetualMarketMap.entrySet()) {
                BybitUsdtPerpetualMarket bybitUsdtPerpetualMarket = entry.getValue();
                if (bybitUsdtPerpetualMarket.subSize() < bybitConfig.getSubSizeLimit()) {
                    bybitUsdtPerpetualMarket.subMarket(symbol);
                    symbolMarketMap.put(symbol, entry.getKey());
                    success = true;
                }
            }
            if (!success) {
                int id = ++index;
                BybitUsdtPerpetualMarket bybitUsdtPerpetualMarket = SpringContextUtils.getBean(BybitUsdtPerpetualMarket.class, id);
                bybitUsdtPerpetualMarket.start();
                bybitUsdtPerpetualMarket.subMarket(symbol);
                bybitUsdtPerpetualMarketMap.put(id, bybitUsdtPerpetualMarket);
                symbolMarketMap.put(symbol, id);
                logger.info("start market " + Integer.toString(id));
            }
        });

    }

    public void subMarket(Set<String> symbols) {
        for (String symbol: symbols) {
            this.subMarket(symbol);
        }
    }

    public void unsubMarket(String symbol) {
        eventloop.execute(()->{
            if (symbolMarketMap.containsKey(symbol)) {
                Integer id = symbolMarketMap.get(symbol);
                BybitUsdtPerpetualMarket bybitUsdtPerpetualMarket = bybitUsdtPerpetualMarketMap.get(id);
                if (null != bybitUsdtPerpetualMarket) bybitUsdtPerpetualMarket.unsubMarket(symbol);
                symbolMarketMap.remove(symbol);
                if (bybitUsdtPerpetualMarket.subSize() <= 0) {
                    bybitUsdtPerpetualMarket.shutdown();
                    bybitUsdtPerpetualMarketMap.remove(id);
                }
            }
        });
    }

    public void unsubMarket(Set<String> symbols) {

    }
}
