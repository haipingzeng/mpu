package com.wxbc.mpu.gateway.binance;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wxbc.mpu.config.BinanceConfig;
import com.wxbc.mpu.market.MarketContext;
import com.wxbc.mpu.market.SymbolMgr;
import com.wxbc.mpu.struct.WxbcSymbol;
import com.wxbc.mpu.struct.WxbcType;
import com.wxbc.mpu.utils.DateTimeUtils;
import com.wxbc.mpu.utils.WxbcUtils;
import com.wxbc.mpu.wss.WssClient;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Component
@Scope("prototype")
public class BinanceUsdtFutureMarket extends WssClient {

    private final static Logger logger = LoggerFactory.getLogger(BinanceUsdtFutureMarket.class);

    @Autowired
    BinanceConfig binanceConfig;

    @Autowired
    MarketContext marketContext;

    @Autowired
    SymbolMgr symbolMgr;

    private Integer id;

    private Set<String> subSymbols = new ConcurrentSkipListSet<>();

    private Map<String, String> wxbcBinanceSymbolMap = new HashMap<>();

    private Map<String, String> binanceWxbcSymbolMap = new HashMap<>();

    private volatile Channel channel = null;

    private LocalDateTime lastReceiveTime = DateTimeUtils.utcnow();

    private ScheduledExecutorService eventloop = new ScheduledThreadPoolExecutor(1, new BasicThreadFactory.Builder().namingPattern("BinanceUsdtFutureMarket").daemon(true).build());

    private Map<String, BinanceUsdtFutureMarketHandler> binanceUsdtFutureMarketHandlerMap = new HashMap<>();

    public BinanceUsdtFutureMarket(Integer id) {
        super("BinanceUsdtFutureMarket " + id);
        this.id = id;
        setAllowClientNoContext(true);
        setRequestedServerNoContext(false);
        // 检查行情是否很久没收到行情
        eventloop.scheduleWithFixedDelay(()->{
            LocalDateTime utcnow = DateTimeUtils.utcnow();
            if (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(lastReceiveTime) > 5000) {
                if (getRunning() && this.channel != null && subSize() > 0) {
                    this.channel.close();
                }
            }
        }, 5, 5, TimeUnit.SECONDS);

    }

    public void start() {
        super.connect(binanceConfig.getUsdtFutureWssUrl());
    }

    public void stop() {
        super.disconnect();
    }

    public void shutdown() {
        super.shutdownGracefully();
        eventloop.shutdown();
    }

    public int subSize() {
        return subSymbols.size();
    }


    //todo 币安增量推送，全量推送最快都是100ms聚合，为了方便且不容易出问题直接接入的是depth(全量)
    public void subMarket(String symbol) {
        logger.info("subMarket symbol: " + symbol);
        String  key = new StringBuilder(WxbcType.EXCHANGE_BINANCE).append("|").append(symbol).toString();
        WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
        if (wxbcSymbol != null) {

            wxbcBinanceSymbolMap.put(symbol, wxbcSymbol.getExchSymbol());
            binanceWxbcSymbolMap.put(wxbcSymbol.getExchSymbol(), symbol);

            logger.info("subMarket symbol: " + symbol + " exchSymbol: " + wxbcSymbol.getExchSymbol());
            subSymbols.add(symbol);
            eventloop.execute(() -> {
                if (this.channel != null) {
                    JSONObject subMsg = new JSONObject();
                    subMsg.put("method", "SUBSCRIBE");
                    subMsg.put("id", WxbcUtils.nextSequence());
                    JSONArray params = new JSONArray();
                    params.add(new StringBuilder().append(wxbcSymbol.getExchSymbol().toLowerCase(Locale.ROOT)).append("@depth20@100ms").toString());
                    params.add(new StringBuilder().append(wxbcSymbol.getExchSymbol().toLowerCase(Locale.ROOT)).append("@trade").toString());
                    subMsg.put("params", params);
                    this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));
                }
            });
        }
    }

    public void unsubMarket(String symbol) {
        logger.info("unsubMarket symbol: " + symbol);
        String  key = new StringBuilder(WxbcType.EXCHANGE_BINANCE).append("|").append(symbol).toString();
        WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
        if (wxbcSymbol != null) {
            logger.info("unsubMarket symbol: " + symbol + " exchSymbol: " + wxbcSymbol.getExchSymbol());
            subSymbols.remove(symbol);
            eventloop.execute(() -> {

            });
        }
    }

    @Override
    protected void onOpen(Channel channel) {
        eventloop.execute(()->{
            this.channel = channel;
            JSONObject subMsg = new JSONObject();
            subMsg.put("method", "SUBSCRIBE");
            subMsg.put("id", WxbcUtils.nextSequence());
            JSONArray params = new JSONArray();
            for (String symbol: subSymbols) {
                String exchSymbol = wxbcBinanceSymbolMap.get(symbol);
                if (null != exchSymbol) {
                    params.add(new StringBuilder().append(exchSymbol.toLowerCase(Locale.ROOT)).append("@depth20@100ms").toString());
                    params.add(new StringBuilder().append(exchSymbol.toLowerCase(Locale.ROOT)).append("@trade").toString());
                }
            }
            subMsg.put("params", params);
            this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));
        });

    }

    @Override
    protected void onMessage(Channel channel, byte[] msg) {
    }

    @Override
    protected void onMessage(Channel channel, String msg) {
        eventloop.execute(()->handleOnMessage(channel, msg, DateTimeUtils.utcnow()));
    }

    @Override
    protected void onClose(Channel channel) {
        eventloop.execute(()->{
            this.channel = null;
        });
    }

    @Override
    protected void onThrowable(Throwable e) {
    }

    private void handleOnMessage(Channel channel, String msg, LocalDateTime arrivedTime) {
//        logger.info(msg);
        lastReceiveTime = DateTimeUtils.utcnow();
        try {
            JSONObject jmsg = JSON.parseObject(msg);
            String event = jmsg.getString("e");
            if ("depthUpdate".equals(event)) {
                String symbol = binanceWxbcSymbolMap.get(jmsg.getString("s"));
                if (null != symbol) {
                    if (!binanceUsdtFutureMarketHandlerMap.containsKey(symbol)) {
                        binanceUsdtFutureMarketHandlerMap.put(symbol, new BinanceUsdtFutureMarketHandler(symbol, marketContext));
                    }
                    binanceUsdtFutureMarketHandlerMap.get(symbol).handleDepth(arrivedTime, jmsg);
                }
            } else if ("trade".equals(event)) {
                String symbol = binanceWxbcSymbolMap.get(jmsg.getString("s"));
                if (null != symbol) {
                    if (!binanceUsdtFutureMarketHandlerMap.containsKey(symbol)) {
                        binanceUsdtFutureMarketHandlerMap.put(symbol, new BinanceUsdtFutureMarketHandler(symbol, marketContext));
                    }
                    binanceUsdtFutureMarketHandlerMap.get(symbol).handleTrade(arrivedTime, jmsg);
                }
            } else {
                logger.info(msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            logger.error(msg);
        }

    }
}
