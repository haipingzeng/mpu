package com.wxbc.mpu.gateway.binance;

import com.wxbc.mpu.app.SpringContextUtils;
import com.wxbc.mpu.config.BinanceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class BinanceSpotMarketMgr {

    private final static Logger logger = LoggerFactory.getLogger(BinanceSpotMarketMgr.class);

    @Autowired
    BinanceConfig binanceConfig;

    private int index = 0;

    private Map<Integer, BinanceSpotMarket> binanceSpotMarketMap = new HashMap<>();

    private Map<String, Integer> symbolMarketMap = new HashMap<>();

    private ScheduledExecutorService eventloop = Executors.newScheduledThreadPool(1);


    public void subMarket(String symbol) {
        eventloop.execute(()->{
            logger.info("sub " + symbol);
            if (symbolMarketMap.containsKey(symbol)) return;
            boolean success = false;
            for (Map.Entry<Integer, BinanceSpotMarket> entry: binanceSpotMarketMap.entrySet()) {
                BinanceSpotMarket binanceSpotMarket = entry.getValue();
                if (binanceSpotMarket.subSize() < binanceConfig.getSubSizeLimit()) {
                    binanceSpotMarket.subMarket(symbol);
                    symbolMarketMap.put(symbol, entry.getKey());
                    success = true;
                }
            }
            if (!success) {
                int id = ++index;
                BinanceSpotMarket binanceSpotMarket = SpringContextUtils.getBean(BinanceSpotMarket.class, id);
                binanceSpotMarket.start();
                binanceSpotMarket.subMarket(symbol);
                binanceSpotMarketMap.put(id, binanceSpotMarket);
                symbolMarketMap.put(symbol, id);
                logger.info("start market " + Integer.toString(id));
            }
        });

    }

    public void subMarket(Set<String> symbols) {
        for (String symbol: symbols) {
            this.subMarket(symbol);
        }
    }

    public void unsubMarket(String symbol) {
        eventloop.execute(()->{
            if (symbolMarketMap.containsKey(symbol)) {
                Integer id = symbolMarketMap.get(symbol);
                BinanceSpotMarket binanceSpotMarket = binanceSpotMarketMap.get(id);
                if (null != binanceSpotMarket) binanceSpotMarket.unsubMarket(symbol);
                symbolMarketMap.remove(symbol);
                if (binanceSpotMarket.subSize() <= 0) {
                    binanceSpotMarket.shutdown();
                    binanceSpotMarketMap.remove(id);
                }
            }
        });
    }

    public void unsubMarket(Set<String> symbols) {

    }
}
