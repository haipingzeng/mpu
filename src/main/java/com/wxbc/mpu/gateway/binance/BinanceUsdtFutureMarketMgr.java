package com.wxbc.mpu.gateway.binance;

import com.wxbc.mpu.app.SpringContextUtils;
import com.wxbc.mpu.config.BinanceConfig;
import com.wxbc.mpu.config.DeribitConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class BinanceUsdtFutureMarketMgr {

    private final static Logger logger = LoggerFactory.getLogger(BinanceUsdtFutureMarketMgr.class);

    @Autowired
    BinanceConfig binanceConfig;

    private int index = 0;

    private Map<Integer, BinanceUsdtFutureMarket> binanceUsdtFutureMarketMap = new HashMap<>();

    private Map<String, Integer> symbolMarketMap = new HashMap<>();

    private ScheduledExecutorService eventloop = Executors.newScheduledThreadPool(1);


    public void subMarket(String symbol) {
        eventloop.execute(()->{
            logger.info("sub " + symbol);
            if (symbolMarketMap.containsKey(symbol)) return;
            boolean success = false;
            for (Map.Entry<Integer, BinanceUsdtFutureMarket> entry: binanceUsdtFutureMarketMap.entrySet()) {
                BinanceUsdtFutureMarket binanceUsdtFutureMarket = entry.getValue();
                if (binanceUsdtFutureMarket.subSize() < binanceConfig.getSubSizeLimit()) {
                    binanceUsdtFutureMarket.subMarket(symbol);
                    symbolMarketMap.put(symbol, entry.getKey());
                    success = true;
                }
            }
            if (!success) {
                int id = ++index;
                BinanceUsdtFutureMarket binanceUsdtFutureMarket = SpringContextUtils.getBean(BinanceUsdtFutureMarket.class, id);
                binanceUsdtFutureMarket.start();
                binanceUsdtFutureMarket.subMarket(symbol);
                binanceUsdtFutureMarketMap.put(id, binanceUsdtFutureMarket);
                symbolMarketMap.put(symbol, id);
                logger.info("start market " + Integer.toString(id));
            }
        });

    }

    public void subMarket(Set<String> symbols) {
        for (String symbol: symbols) {
            this.subMarket(symbol);
        }
    }

    public void unsubMarket(String symbol) {
        eventloop.execute(()->{
            if (symbolMarketMap.containsKey(symbol)) {
                Integer id = symbolMarketMap.get(symbol);
                BinanceUsdtFutureMarket binanceUsdtFutureMarket = binanceUsdtFutureMarketMap.get(id);
                if (null != binanceUsdtFutureMarket) binanceUsdtFutureMarket.unsubMarket(symbol);
                symbolMarketMap.remove(symbol);
                if (binanceUsdtFutureMarket.subSize() <= 0) {
                    binanceUsdtFutureMarket.shutdown();
                    binanceUsdtFutureMarketMap.remove(id);
                }
            }
        });
    }

    public void unsubMarket(Set<String> symbols) {

    }
}
