package com.wxbc.mpu.gateway.binance;

import com.alibaba.fastjson.JSONObject;
import com.wxbc.mpu.market.MarketContext;
import com.wxbc.mpu.struct.WxbcType;
import com.wxbc.mpu.utils.DateTimeUtils;
import com.wxbc.mpu.utils.WxbcUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class BinanceUsdtFutureMarketHandler {

    private final static Logger logger = LoggerFactory.getLogger(BinanceUsdtFutureMarketHandler.class);

    private MarketContext marketContext;

    private String exchange = WxbcType.EXCHANGE_BINANCE;

    private String symbol;

    private TreeMap<BigDecimal, BigDecimal> bidMap = null;

    private TreeMap<BigDecimal, BigDecimal> askMap = null;

    private LocalDateTime lastUpdateTime = null;

    public BinanceUsdtFutureMarketHandler(String symbol, MarketContext marketContext) {
        this.marketContext = marketContext;
        this.symbol = symbol;
    }

    public void handleDepth(LocalDateTime arrivedTime, JSONObject jDepth) {
        TreeMap<BigDecimal, BigDecimal> beforeBidMap = bidMap;
        TreeMap<BigDecimal, BigDecimal> beforeAskMap = askMap;
        bidMap = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        askMap = new TreeMap<>();

        LocalDateTime updateTime = DateTimeUtils.millisToUtcDateTime(jDepth.getLong("T"));
        for (BigDecimal[] bid : jDepth.getJSONArray("b").toJavaList(BigDecimal[].class)) {
            bidMap.put(bid[0], bid[1]);
        }
        for (BigDecimal[] ask : jDepth.getJSONArray("a").toJavaList(BigDecimal[].class)) {
            askMap.put(ask[0], ask[1]);
        }
        TreeMap<String, BigDecimal> updateBidMap = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        TreeMap<String, BigDecimal> updateAskMap = new TreeMap<>();
        int idx = 0;
        if (beforeBidMap != null) {
            for (Map.Entry<BigDecimal, BigDecimal> entry : beforeBidMap.entrySet()) {
                if (!bidMap.containsKey(entry.getKey())) {
                    updateBidMap.put(entry.getKey().toString(), BigDecimal.ZERO);
                } else {
                    BigDecimal qty = bidMap.get(entry.getKey());
                    if (entry.getValue().compareTo(qty) != 0) {
                        updateBidMap.put(entry.getKey().toString(), qty);
                    }
                }
                if (++idx >= 25) break;
            }
        }
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry : bidMap.entrySet()) {
            if (beforeBidMap == null || !beforeBidMap.containsKey(entry.getKey())) {
                updateBidMap.put(entry.getKey().toString(), entry.getValue());
            }
            if (++idx >= 25) break;
        }
        idx = 0;
        if (beforeAskMap != null) {
            for (Map.Entry<BigDecimal, BigDecimal> entry : beforeAskMap.entrySet()) {
                if (!askMap.containsKey(entry.getKey())) {
                    updateAskMap.put(entry.getKey().toString(), BigDecimal.ZERO);
                } else {
                    BigDecimal qty = askMap.get(entry.getKey());
                    if (entry.getValue().compareTo(qty) != 0) {
                        updateAskMap.put(entry.getKey().toString(), qty);
                    }
                }
                if (++idx >= 25) break;
            }
        }
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry : askMap.entrySet()) {
            if (beforeAskMap == null || !beforeAskMap.containsKey(entry.getKey())) {
                updateAskMap.put(entry.getKey().toString(), entry.getValue());
            }
            if (++idx >= 25) break;
        }
        marketContext.publishDelta(WxbcUtils.nextSequence(), exchange, symbol, updateTime, arrivedTime, updateBidMap, updateAskMap);

        TreeMap<String, BigDecimal> bidDepth = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        TreeMap<String, BigDecimal> askDepth = new TreeMap<>();
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry: bidMap.entrySet()) {
            bidDepth.put(entry.getKey().toString(), entry.getValue());
            if (++idx >= 25) break;
        }
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry: askMap.entrySet()) {
            askDepth.put(entry.getKey().toString(), entry.getValue());
            if (++idx >= 25) break;
        }
        Long msgSeq = WxbcUtils.nextSequence();
        if (lastUpdateTime == null || !DateTimeUtils.fmtDate(lastUpdateTime, "yyyy-MM-dd").equals(DateTimeUtils.fmtDate(arrivedTime, "yyyy-MM-dd"))) {
            boolean ret = marketContext.updateCrossingSnapshot(msgSeq, exchange, symbol, updateTime, arrivedTime, bidDepth, askDepth);
            if (ret) lastUpdateTime = arrivedTime;
        }
        marketContext.publishDepth(msgSeq, exchange, symbol, updateTime, arrivedTime, bidDepth, askDepth);
    }


    public void handleTrade(LocalDateTime arrivedTime, JSONObject jTrade) {
        LocalDateTime updateTime = DateTimeUtils.millisToUtcDateTime(jTrade.getLong("T"));
        String side = "Buy";
        if (jTrade.getBoolean("m")) {
            side = "Sell";
        }
        BigDecimal price = jTrade.getBigDecimal("p");
        BigDecimal size = jTrade.getBigDecimal("q");
        marketContext.publishTrade(exchange, symbol, updateTime, arrivedTime, side, price, size);

    }
}
