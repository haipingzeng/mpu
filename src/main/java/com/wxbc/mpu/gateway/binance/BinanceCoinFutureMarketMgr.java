package com.wxbc.mpu.gateway.binance;

import com.wxbc.mpu.app.SpringContextUtils;
import com.wxbc.mpu.config.BinanceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class BinanceCoinFutureMarketMgr {

    private final static Logger logger = LoggerFactory.getLogger(BinanceCoinFutureMarketMgr.class);

    @Autowired
    BinanceConfig binanceConfig;

    private int index = 0;

    private Map<Integer, BinanceCoinFutureMarket> binanceCoinFutureMarketMap = new HashMap<>();

    private Map<String, Integer> symbolMarketMap = new HashMap<>();

    private ScheduledExecutorService eventloop = Executors.newScheduledThreadPool(1);


    public void subMarket(String symbol) {
        eventloop.execute(()->{
            logger.info("sub " + symbol);
            if (symbolMarketMap.containsKey(symbol)) return;
            boolean success = false;
            for (Map.Entry<Integer, BinanceCoinFutureMarket> entry: binanceCoinFutureMarketMap.entrySet()) {
                BinanceCoinFutureMarket binanceCoinFutureMarket = entry.getValue();
                if (binanceCoinFutureMarket.subSize() < binanceConfig.getSubSizeLimit()) {
                    binanceCoinFutureMarket.subMarket(symbol);
                    symbolMarketMap.put(symbol, entry.getKey());
                    success = true;
                }
            }
            if (!success) {
                int id = ++index;
                BinanceCoinFutureMarket binanceCoinFutureMarket = SpringContextUtils.getBean(BinanceCoinFutureMarket.class, id);
                binanceCoinFutureMarket.start();
                binanceCoinFutureMarket.subMarket(symbol);
                binanceCoinFutureMarketMap.put(id, binanceCoinFutureMarket);
                symbolMarketMap.put(symbol, id);
                logger.info("start market " + Integer.toString(id));
            }
        });

    }

    public void subMarket(Set<String> symbols) {
        for (String symbol: symbols) {
            this.subMarket(symbol);
        }
    }

    public void unsubMarket(String symbol) {
        eventloop.execute(()->{
            if (symbolMarketMap.containsKey(symbol)) {
                Integer id = symbolMarketMap.get(symbol);
                BinanceCoinFutureMarket binanceCoinFutureMarket = binanceCoinFutureMarketMap.get(id);
                if (null != binanceCoinFutureMarket) binanceCoinFutureMarket.unsubMarket(symbol);
                symbolMarketMap.remove(symbol);
                if (binanceCoinFutureMarket.subSize() <= 0) {
                    binanceCoinFutureMarket.shutdown();
                    binanceCoinFutureMarketMap.remove(id);
                }
            }
        });
    }

    public void unsubMarket(Set<String> symbols) {

    }
}
