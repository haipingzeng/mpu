package com.wxbc.mpu.gateway.ftx;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wxbc.mpu.config.FtxConfig;
import com.wxbc.mpu.market.MarketContext;
import com.wxbc.mpu.market.SymbolMgr;
import com.wxbc.mpu.struct.SubRecord;
import com.wxbc.mpu.struct.WxbcSymbol;
import com.wxbc.mpu.struct.WxbcType;
import com.wxbc.mpu.utils.DateTimeUtils;
import com.wxbc.mpu.wss.WssClient;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Component
@Scope("prototype")
public class FtxUnifyMarket extends WssClient {
    private final static Logger logger = LoggerFactory.getLogger(FtxUnifyMarket.class);

    @Autowired
    FtxConfig ftxConfig;

    @Autowired
    MarketContext marketContext;

    @Autowired
    SymbolMgr symbolMgr;

    private Map<String, String> wxbcFtxSymbolMap = new HashMap<>();

    private Map<String, String> ftxWxbcSymbolMap = new HashMap<>();

    private Integer id;

    private Set<String> subSymbols = new ConcurrentSkipListSet<>();



    private volatile Channel channel = null;

    private LocalDateTime lastReceiveTime = DateTimeUtils.utcnow();

    private ScheduledExecutorService eventloop = new ScheduledThreadPoolExecutor(1, new BasicThreadFactory.Builder().namingPattern("FtxUnifyMarket").daemon(true).build());

    private Map<String, FtxUnifyMarketHandler> ftxUnifyMarketHandlerMap = new HashMap<>();

    private Map<String, SubRecord> subTradeRecordMap = new HashMap<>();
    private Map<String, SubRecord> subDepthRecordMap = new HashMap<>();


    public FtxUnifyMarket(Integer id) {
        super("FtxUnifyMarket " + id);
        this.id = id;
        setAllowClientNoContext(true);
        setRequestedServerNoContext(false);

        // 检查行情是否很久没收到行情
        eventloop.scheduleWithFixedDelay(()->{
            LocalDateTime utcnow = DateTimeUtils.utcnow();

            if ( DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(lastReceiveTime) > 5000) {
                if (getRunning() && this.channel != null && subSize() > 0) {
                    this.channel.close();
                }
            }
        }, 5, 5, TimeUnit.SECONDS);

        eventloop.scheduleWithFixedDelay(()->{
            if (this.channel != null) {
                JSONObject pingMsg = new JSONObject();
                pingMsg.put("op", "ping");
                this.channel.writeAndFlush(new TextWebSocketFrame(pingMsg.toJSONString()));
            }
        }, 28, 28, TimeUnit.SECONDS);


        // 订阅失败继续订阅，最多订阅三次
        eventloop.scheduleWithFixedDelay(()->{
            try {
                if (null == this.channel) return;
                LocalDateTime utcnow = DateTimeUtils.utcnow();
                {
                    var it = subDepthRecordMap.entrySet().iterator();
                    while (it.hasNext()) {
                        var entry = it.next();
                        if (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(entry.getValue().getTime()) > 8000) {
                            if (entry.getValue().getState() > 0) {
                                entry.getValue().setState(entry.getValue().getState() - 1);

                                JSONObject subOrderBookReq = new JSONObject();
                                subOrderBookReq.put("op", "subscribe");
                                subOrderBookReq.put("channel", "orderbook");
                                subOrderBookReq.put("market", entry.getValue().getSymbol());
                                this.channel.writeAndFlush(new TextWebSocketFrame(subOrderBookReq.toJSONString()));
                            }
                            if (entry.getValue().getState() <= 0) {
                                it.remove();
                            }
                        }
                    }
                }
                {
                    var it = subTradeRecordMap.entrySet().iterator();
                    while (it.hasNext()) {
                        var entry = it.next();
                        if (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(entry.getValue().getTime()) > 8000) {
                            if (entry.getValue().getState() > 0) {
                                entry.getValue().setState(entry.getValue().getState() - 1);

                                JSONObject subTradeReq = new JSONObject();
                                subTradeReq.put("op", "subscribe");
                                subTradeReq.put("channel", "trades");
                                subTradeReq.put("market", entry.getValue().getSymbol());
                                this.channel.writeAndFlush(new TextWebSocketFrame(subTradeReq.toJSONString()));
                            }
                            if (entry.getValue().getState() <= 0) {
                                it.remove();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, 20, 20, TimeUnit.SECONDS);

    }

    public void start() {
        super.connect(ftxConfig.getUnifyWssUrl());
    }

    public void stop() {
        super.disconnect();
    }

    public void shutdown() {
        super.shutdownGracefully();
        eventloop.shutdown();
    }

    public int subSize() {
        return subSymbols.size();
    }

    public void subMarket(String symbol) {
        logger.info("subMarket symbol: " + symbol);
        String  key = new StringBuilder(WxbcType.EXCHANGE_FTX).append("|").append(symbol).toString();
        WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
        if (wxbcSymbol != null) {
            wxbcFtxSymbolMap.put(symbol, wxbcSymbol.getExchSymbol());
            ftxWxbcSymbolMap.put(wxbcSymbol.getExchSymbol(), symbol);

            logger.info("subMarket symbol: " + symbol + " exchSymbol: " + wxbcSymbol.getExchSymbol());

            eventloop.execute(() -> {
                subSymbols.add(symbol);
                subTradeRecordMap.put(wxbcSymbol.getExchSymbol(), new SubRecord(wxbcSymbol.getExchSymbol(), DateTimeUtils.utcnow()));
                subDepthRecordMap.put(wxbcSymbol.getExchSymbol(), new SubRecord(wxbcSymbol.getExchSymbol(), DateTimeUtils.utcnow()));
                if (this.channel != null) {
                    JSONObject subOrderBookReq = new JSONObject();
                    subOrderBookReq.put("op", "subscribe");
                    subOrderBookReq.put("channel", "orderbook");
                    subOrderBookReq.put("market", wxbcSymbol.getExchSymbol());
                    this.channel.writeAndFlush(new TextWebSocketFrame(subOrderBookReq.toJSONString()));

                    JSONObject subTradeReq = new JSONObject();
                    subTradeReq.put("op", "subscribe");
                    subTradeReq.put("channel", "orderbook");
                    subTradeReq.put("market", wxbcSymbol.getExchSymbol());
                    this.channel.writeAndFlush(new TextWebSocketFrame(subTradeReq.toJSONString()));
                }
            });
        }
    }

    public void unsubMarket(String symbol) {
    }


    @Override
    protected void onOpen(Channel channel) {

        eventloop.execute(()->{
            this.channel = channel;
            for (String symbol: subSymbols) {
                String exchSymbol = wxbcFtxSymbolMap.get(symbol);

                subDepthRecordMap.put(exchSymbol, new SubRecord(exchSymbol, DateTimeUtils.utcnow()));
                JSONObject subOrderBookReq = new JSONObject();
                subOrderBookReq.put("op", "subscribe");
                subOrderBookReq.put("channel", "orderbook");
                subOrderBookReq.put("market", exchSymbol);
                this.channel.writeAndFlush(new TextWebSocketFrame(subOrderBookReq.toJSONString()));

                subTradeRecordMap.put(exchSymbol, new SubRecord(exchSymbol, DateTimeUtils.utcnow()));
                JSONObject subTradeReq = new JSONObject();
                subTradeReq.put("op", "subscribe");
                subTradeReq.put("channel", "trades");
                subTradeReq.put("market", exchSymbol);
                this.channel.writeAndFlush(new TextWebSocketFrame(subTradeReq.toJSONString()));
            }
        });

    }

    @Override
    protected void onMessage(Channel channel, byte[] msg) {
    }

    @Override
    protected void onMessage(Channel channel, String msg) {
        eventloop.execute(()->handleOnMessage(channel, msg, DateTimeUtils.utcnow()));
    }

    @Override
    protected void onClose(Channel channel) {
        eventloop.execute(()->{

        });
        this.channel = null;
    }

    @Override
    protected void onThrowable(Throwable e) {
    }

    private void handleOnMessage(Channel channel, String msg, LocalDateTime arrivedTime) {
        lastReceiveTime = DateTimeUtils.utcnow();
        try {
            JSONObject jmsg = JSON.parseObject(msg);
            String msgType = jmsg.getString("type");
            if ("error".equals(msgType)) {
                logger.info(msg);
            } else if ("update".equals(msgType)) {
                String symbol = ftxWxbcSymbolMap.get(jmsg.getString("market"));
                if (null != symbol) {
                    if (!ftxUnifyMarketHandlerMap.containsKey(symbol)) {
                        ftxUnifyMarketHandlerMap.put(symbol, new FtxUnifyMarketHandler(symbol, marketContext));
                    }
                    String topic = jmsg.getString("channel");
                    if ("orderbook".equals(topic)) {
                        JSONObject jdata = jmsg.getJSONObject("data");
                        ftxUnifyMarketHandlerMap.get(symbol).handleDelta(arrivedTime, jdata);

                    } else if ("trades".equals(topic)) {
                        ftxUnifyMarketHandlerMap.get(symbol).handleTrade(arrivedTime, jmsg.getJSONArray("data").toJavaList(JSONObject.class));
                    }
                }
            } else if ("partial".equals(msgType)) {
                String symbol = ftxWxbcSymbolMap.get(jmsg.getString("market"));
                if (null != symbol) {
                    if (!ftxUnifyMarketHandlerMap.containsKey(symbol)) {
                        ftxUnifyMarketHandlerMap.put(symbol, new FtxUnifyMarketHandler(symbol, marketContext));
                    }
                    String topic = jmsg.getString("channel");
                    if ("orderbook".equals(topic)) {
                        ftxUnifyMarketHandlerMap.get(symbol).handleSnapshot(arrivedTime, jmsg.getJSONObject("data"));
                    }
                }

            } else if ("subscribed".equals(msgType)) {
                logger.info(msg);
                String topic = jmsg.getString("channel");
                if ("orderbook".equals(topic)) {
                    subDepthRecordMap.remove(jmsg.getString("market"));
                } else if ("trades".equals(topic)) {
                    subTradeRecordMap.remove(jmsg.getString("market"));
                }
            } else if (!"pong".equals(msgType)) {
                logger.info(msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(msg);
        }

    }
}
