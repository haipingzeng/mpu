package com.wxbc.mpu.gateway.ftx;

import com.wxbc.mpu.app.SpringContextUtils;
import com.wxbc.mpu.config.FtxConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class FtxUnifyMarketMgr {

    private final static Logger logger = LoggerFactory.getLogger(FtxUnifyMarketMgr.class);

    @Autowired
    FtxConfig ftxConfig;

    private int index = 0;

    private Map<Integer, FtxUnifyMarket> ftxUnifyMarketMap = new HashMap<>();

    private Map<String, Integer> symbolMarketMap = new HashMap<>();

    private ScheduledExecutorService eventloop = Executors.newScheduledThreadPool(1);


    public void subMarket(String symbol) {
        eventloop.execute(()->{
            logger.info("sub " + symbol);
            if (symbolMarketMap.containsKey(symbol)) return;
            boolean success = false;
            for (Map.Entry<Integer, FtxUnifyMarket> entry: ftxUnifyMarketMap.entrySet()) {
                FtxUnifyMarket ftxUnifyMarket = entry.getValue();
                if (ftxUnifyMarket.subSize() < ftxConfig.getSubSizeLimit()) {
                    ftxUnifyMarket.subMarket(symbol);
                    symbolMarketMap.put(symbol, entry.getKey());
                    success = true;
                }
            }
            if (!success) {
                int id = ++index;
                FtxUnifyMarket ftxUnifyMarket = SpringContextUtils.getBean(FtxUnifyMarket.class, id);
                ftxUnifyMarket.start();
                ftxUnifyMarket.subMarket(symbol);
                ftxUnifyMarketMap.put(id, ftxUnifyMarket);
                symbolMarketMap.put(symbol, id);
                logger.info("start market " + Integer.toString(id));
            }
        });

    }

    public void subMarket(Set<String> symbols) {
        for (String symbol: symbols) {
            this.subMarket(symbol);
        }
    }

    public void unsubMarket(String symbol) {
        eventloop.execute(()->{
            if (symbolMarketMap.containsKey(symbol)) {
                Integer id = symbolMarketMap.get(symbol);
                FtxUnifyMarket deribitUnifyMarket = ftxUnifyMarketMap.get(id);
                if (null != deribitUnifyMarket) deribitUnifyMarket.unsubMarket(symbol);
                symbolMarketMap.remove(symbol);
                if (deribitUnifyMarket.subSize() <= 0) {
                    deribitUnifyMarket.shutdown();
                    ftxUnifyMarketMap.remove(id);
                }
            }
        });
    }

    public void unsubMarket(Set<String> symbols) {

    }
}
