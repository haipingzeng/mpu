package com.wxbc.mpu.gateway.bitmex;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wxbc.mpu.config.BitmexConfig;
import com.wxbc.mpu.market.MarketContext;
import com.wxbc.mpu.struct.SubRecord;
import com.wxbc.mpu.utils.DateTimeUtils;
import com.wxbc.mpu.utils.WxbcUtils;
import com.wxbc.mpu.wss.WssClient;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.*;

@Component
@Scope("prototype")
public class BitmexFutureMarket extends WssClient {

    private final static Logger logger = LoggerFactory.getLogger(BitmexFutureMarket.class);

    @Autowired
    BitmexConfig bitmexConfig;

    @Autowired
    MarketContext marketContext;

    private Integer id;

    private Set<String> subSymbols = new ConcurrentSkipListSet<>();

    private Map<String, String> wxbcBitmexSymbolMap = new HashMap<>();

    private Map<String, String> bitmexWxbcSymbolMap = new HashMap<>();

    private volatile Channel channel = null;

    private LocalDateTime lastReceiveTime = DateTimeUtils.utcnow();

//    private ScheduledExecutorService eventloop = Executors.newScheduledThreadPool(1);

    private ScheduledExecutorService eventloop = new ScheduledThreadPoolExecutor(1, new BasicThreadFactory.Builder().namingPattern("BitmexFutureMarket").daemon(true).build());


    private Map<String, BitmexFutureMarketHandler> bitmexFutureMarketHandlerMap = new HashMap<>();

    private Map<String, SubRecord> subRecordMap = new HashMap<>();

    public BitmexFutureMarket(Integer id) {
        super("BitmexFutureMarket " + id);
        this.id = id;
        setAllowClientNoContext(true);
        setRequestedServerNoContext(false);
        // 检查行情是否很久没收到行情
        eventloop.scheduleWithFixedDelay(()->{
            LocalDateTime utcnow = DateTimeUtils.utcnow();
            if (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(lastReceiveTime) > 5000) {
                if (getRunning() && this.channel != null && subSize() > 0) {
                    this.channel.close();
                }
            }
        }, 5, 5, TimeUnit.SECONDS);


        // 订阅失败继续订阅，最多订阅三次
        eventloop.scheduleWithFixedDelay(()->{
            try {
                if (null == this.channel) return;
                LocalDateTime utcnow = DateTimeUtils.utcnow();

                var it = subRecordMap.entrySet().iterator();
                while (it.hasNext()) {
                    var entry = it.next();
                    if (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(entry.getValue().getTime()) > 8000) {
                        if (entry.getValue().getState() > 0) {
                            entry.getValue().setState(entry.getValue().getState() - 1);

                            JSONObject subMsg = new JSONObject();
                            subMsg.put("op", "subscribe");
                            List<String> args = new ArrayList<>();
                            args.add("orderBookL2:" + entry.getValue().getSymbol());
                            args.add("trade:" + entry.getValue().getSymbol());
                            subMsg.put("args", args);
                            logger.info("sub market: " + subMsg.toJSONString());
                            this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));
                        }
                        if (entry.getValue().getState() <= 0) {
                            it.remove();
                        }
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }, 20, 20, TimeUnit.SECONDS);

//        eventloop.scheduleWithFixedDelay(()->{
//            if (this.channel != null) {
//                this.channel.writeAndFlush(new TextWebSocketFrame("ping"));
//            }
//        }, 30, 30, TimeUnit.SECONDS);
    }

    public void start() {
        super.connect(bitmexConfig.getFutureWssUrl());
    }

    public void stop() {
        super.disconnect();
    }

    public void shutdown() {
        super.shutdownGracefully();
        eventloop.shutdown();
    }

    public int subSize() {
        return subSymbols.size();
    }

    public void subMarket(String symbol) {
        String exchSymbol = getExchSymbol(symbol);
        logger.info("subMarket symbol: " + symbol + " exchSymbol: " + exchSymbol);
        if (exchSymbol != null) {
            subSymbols.add(symbol);
            eventloop.execute(() -> {
                subRecordMap.put(exchSymbol, new SubRecord(exchSymbol, DateTimeUtils.utcnow()));
                if (this.channel != null) {
                    JSONObject subMsg = new JSONObject();
                    subMsg.put("op", "subscribe");
                    List<String> args = new ArrayList<>();
                    args.add("orderBookL2:" + exchSymbol);
                    args.add("trade:" + exchSymbol);
                    subMsg.put("args", args);
                    logger.info("sub market: " + subMsg.toJSONString());
                    this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));

                }
            });
        }
    }

    public void unsubMarket(String symbol) {
        String exchSymbol = getExchSymbol(symbol);
        logger.info("unsubMarket symbol: " + symbol + " exchSymbol: " + exchSymbol);
        if (exchSymbol != null) {
            subSymbols.remove(symbol);
            eventloop.execute(() -> {
                if (this.channel != null) {
                    JSONObject subMsg = new JSONObject();
                    subMsg.put("op", "unsubscribe");
                    List<String> args = new ArrayList<>();
                    args.add("orderBookL2:" + exchSymbol);
                    args.add("trade:" + exchSymbol);
                    subMsg.put("args", args);
                    logger.info("unsub market: " + subMsg.toJSONString());
                    this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));

                }
            });
        }
    }

    private String getExchSymbol(String symbol) {
        try {
            if (wxbcBitmexSymbolMap.containsKey(symbol)) return wxbcBitmexSymbolMap.get(symbol);
            String fields[] = symbol.split("_|@");
            if (fields.length != 3) return null;
            String retSymbol = null;
            if ("BTC".equals(fields[0])) {
                if ("USD".equals(fields[1])) {
                    if ("P".equals(fields[2])) {
                        retSymbol =  "XBTUSD";
                    } else {
                        String year = fields[2].substring(0, 2);
                        int month = Integer.parseInt(fields[2].substring(2, 4));
                        if (month == 3) {
                            retSymbol = "XBTH" + year;
                        } else if (month == 6) {
                            retSymbol =  "XBTM" + year;
                        } else if (month == 9) {
                            retSymbol = "XBTU" + year;
                        } else if (month == 12) {
                            retSymbol = "XBTZ" + year;
                        }
                    }
                } else if ("EUR".equals(fields[1])) {
                    if ("P".equals(fields[2])) {
                        retSymbol = "XBTEUR";
                    }
                }
            } else {
                if ("USD".equals(fields[1]) || "USDT".equals(fields[1])) {
                    if ("P".equals(fields[2])) {
                        retSymbol = fields[0] + fields[1];
                    } else {
                        String year = fields[2].substring(0, 2);
                        int month = Integer.parseInt(fields[2].substring(2, 4));
                        if (month == 3) {
                            retSymbol = fields[0] + fields[1] + "H" + year;
                        } else if (month == 6) {
                            retSymbol =  fields[0] + fields[1] + "M" + year;
                        } else if (month == 9) {
                            retSymbol = fields[0] + fields[1] + "U" + year;
                        } else if (month == 12) {
                            retSymbol = fields[0] + fields[1] + "Z" + year;
                        }
                    }
                } else if ("BTC".equals(fields[1])) {
                    if ("P".equals(fields[2])) {

                    } else {
                        String year = fields[2].substring(0, 2);
                        int month = Integer.parseInt(fields[2].substring(2, 4));
                        if (month == 3) {
                            retSymbol = fields[0] + "H" + year;
                        } else if (month == 6) {
                            retSymbol =  fields[0] + "M" + year;
                        } else if (month == 9) {
                            retSymbol = fields[0] + "U" + year;
                        } else if (month == 12) {
                            retSymbol = fields[0] + "Z" + year;
                        }
                    }
                }
            }
            if (retSymbol != null) {
                wxbcBitmexSymbolMap.put(symbol, retSymbol);
                bitmexWxbcSymbolMap.put(retSymbol, symbol);
            }
            return retSymbol;
        } catch (Exception e) {

        }
        return null;
    }


    @Override
    protected void onOpen(Channel channel) {
        eventloop.execute(()->{
            this.channel = channel;


            subRecordMap = new HashMap<>();
            for (String symbol: subSymbols) {
                String exchSymbol = getExchSymbol(symbol);
                if (null == exchSymbol) continue;
                subRecordMap.put(exchSymbol, new SubRecord(exchSymbol, DateTimeUtils.utcnow()));

                JSONObject subMsg = new JSONObject();
                subMsg.put("op", "subscribe");
                List<String> args = new ArrayList<>();
                args.add("orderBookL2:" + exchSymbol);
                args.add("trade:" + exchSymbol);
                subMsg.put("args", args);
                logger.info("sub market: " + subMsg.toJSONString());
                this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));
            }

        });

    }

    @Override
    protected void onMessage(Channel channel, byte[] msg) {
    }

    @Override
    protected void onMessage(Channel channel, String msg) {
        eventloop.execute(()->handleOnMessage(channel, msg, DateTimeUtils.utcnow()));
    }

    @Override
    protected void onClose(Channel channel) {
        eventloop.execute(()->{
            this.channel = null;
        });
    }

    @Override
    protected void onThrowable(Throwable e) {
    }

    private void handleOnMessage(Channel channel, String msg, LocalDateTime arrivedTime) {
        try {
            lastReceiveTime = DateTimeUtils.utcnow();
            if ("pong".equals(msg)) return;
            JSONObject jmsg = JSON.parseObject(msg);
            if (jmsg.containsKey("action") && jmsg.containsKey("table")) {
                String table = jmsg.getString("table");
                String action = jmsg.getString("action");
                if ("trade".equals(table)) {
                    List<JSONObject> dataList = jmsg.getJSONArray("data").toJavaList(JSONObject.class);
                    if (dataList.size() > 0) {
                        String symbol = bitmexWxbcSymbolMap.get(dataList.get(0).getString("symbol"));
                        if (bitmexFutureMarketHandlerMap.containsKey(symbol)) {
                            bitmexFutureMarketHandlerMap.get(symbol).handleTrade(arrivedTime, dataList);
                        }
                    }
                } else if ("orderBookL2".equals(table)) {
                    if ("partial".equals(action)) {
                        List<JSONObject> dataList = jmsg.getJSONArray("data").toJavaList(JSONObject.class);
                        if (dataList.size() > 0) {
                            String symbol = bitmexWxbcSymbolMap.get(dataList.get(0).getString("symbol"));
                            if (null != symbol) {
                                if (!bitmexFutureMarketHandlerMap.containsKey(symbol)) {
                                    bitmexFutureMarketHandlerMap.put(symbol, new BitmexFutureMarketHandler(symbol, marketContext));
                                }
                                bitmexFutureMarketHandlerMap.get(symbol).handlePartial(arrivedTime, dataList);
                            }
                        }
                    } else if ("update".equals(action)) {
                        List<JSONObject> dataList = jmsg.getJSONArray("data").toJavaList(JSONObject.class);
                        if (dataList.size() > 0) {
                            String symbol = bitmexWxbcSymbolMap.get(dataList.get(0).getString("symbol"));
                            if (bitmexFutureMarketHandlerMap.containsKey(symbol)) {
                                bitmexFutureMarketHandlerMap.get(symbol).handleUpdate(arrivedTime, dataList);
                            }
                        }
                    } else if ("insert".equals(action)) {
                        List<JSONObject> dataList = jmsg.getJSONArray("data").toJavaList(JSONObject.class);
                        if (dataList.size() > 0) {
                            String symbol = bitmexWxbcSymbolMap.get(dataList.get(0).getString("symbol"));
                            if (bitmexFutureMarketHandlerMap.containsKey(symbol)) {
                                bitmexFutureMarketHandlerMap.get(symbol).handlInsert(arrivedTime, dataList);
                            }
                        }
                    } else if ("delete".equals(action)) {
                        List<JSONObject> dataList = jmsg.getJSONArray("data").toJavaList(JSONObject.class);
                        if (dataList.size() > 0) {
                            String symbol = bitmexWxbcSymbolMap.get(dataList.get(0).getString("symbol"));
                            if (bitmexFutureMarketHandlerMap.containsKey(symbol)) {
                                bitmexFutureMarketHandlerMap.get(symbol).handleDelete(arrivedTime, dataList);
                            }
                        }
                    }
                }
            } else {
                if (jmsg.containsKey("success") && jmsg.getBoolean("success") && jmsg.containsKey("subscribe")) {
                    String subscribe = jmsg.getString("subscribe");
                    String[] infos = subscribe.split(":");
                    if (infos.length >= 2) {
                        subRecordMap.remove(infos[1]);
                    }
                }
                logger.info(id + " " + msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString());
        }
    }
}
