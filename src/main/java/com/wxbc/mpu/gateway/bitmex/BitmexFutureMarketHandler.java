package com.wxbc.mpu.gateway.bitmex;

import com.alibaba.fastjson.JSONObject;
import com.wxbc.mpu.market.MarketContext;
import com.wxbc.mpu.struct.WxbcType;
import com.wxbc.mpu.utils.DateTimeUtils;
import com.wxbc.mpu.utils.WxbcUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

public class BitmexFutureMarketHandler {

    private MarketContext marketContext;

    private String exchange = WxbcType.EXCHANGE_BITMEX;

    private String symbol;

    private TreeMap<BigDecimal, BigDecimal> bidMap = null;

    private TreeMap<BigDecimal, BigDecimal> askMap = null;

    private LocalDateTime lastUpdateTime = null;

    private Map<Long, BigDecimal> bidCacheMap = null;

    private Map<Long, BigDecimal> askCacheMap = null;

    public BitmexFutureMarketHandler(String symbol, MarketContext marketContext) {
        this.marketContext = marketContext;
        this.symbol = symbol;
    }

    public void handlePartial(LocalDateTime arrivedTime, List<JSONObject> dataList) {
        LocalDateTime updateTime = arrivedTime;
        TreeMap<BigDecimal, BigDecimal> beforeBidMap = bidMap;
        TreeMap<BigDecimal, BigDecimal> beforeAskMap = askMap;
        bidMap = new TreeMap<>((o1, o2) -> {return o2.compareTo(o1);});
        askMap = new TreeMap<>();
        bidCacheMap = new HashMap<>();
        askCacheMap = new HashMap<>();
        for (JSONObject info : dataList) {
            String side = info.getString("side");
            if ("Buy".equals(side)) {
                BigDecimal price = info.getBigDecimal("price");
                BigDecimal size = info.getBigDecimal("size");
                bidMap.put(price, size);
                bidCacheMap.put(info.getLong("id"), price);
            } else if ("Sell".equals(side)) {
                BigDecimal price = info.getBigDecimal("price");
                BigDecimal size = info.getBigDecimal("size");
                askMap.put(price, size);
                askCacheMap.put(info.getLong("id"), price);
            }
        }


        TreeMap<String, BigDecimal> updateBidMap = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        TreeMap<String, BigDecimal> updateAskMap = new TreeMap<>();
        int idx = 0;
        if (beforeBidMap != null) {
            for (Map.Entry<BigDecimal, BigDecimal> entry : beforeBidMap.entrySet()) {
                if (!bidMap.containsKey(entry.getKey())) {
                    updateBidMap.put(entry.getKey().toString(), BigDecimal.ZERO);
                } else {
                    BigDecimal qty = bidMap.get(entry.getKey());
                    if (entry.getValue().compareTo(qty) != 0) {
                        updateBidMap.put(entry.getKey().toString(), qty);
                    }
                }
                if (++idx >= 25) break;
            }
        }
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry : bidMap.entrySet()) {
            if (beforeBidMap == null || !beforeBidMap.containsKey(entry.getKey())) {
                updateBidMap.put(entry.getKey().toString(), entry.getValue());
            }
            if (++idx >= 25) break;
        }
        idx = 0;
        if (beforeAskMap != null) {
            for (Map.Entry<BigDecimal, BigDecimal> entry : beforeAskMap.entrySet()) {
                if (!askMap.containsKey(entry.getKey())) {
                    updateAskMap.put(entry.getKey().toString(), BigDecimal.ZERO);
                } else {
                    BigDecimal qty = askMap.get(entry.getKey());
                    if (entry.getValue().compareTo(qty) != 0) {
                        updateAskMap.put(entry.getKey().toString(), qty);
                    }
                }
                if (++idx >= 25) break;
            }
        }
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry : askMap.entrySet()) {
            if (beforeAskMap == null || !beforeAskMap.containsKey(entry.getKey())) {
                updateAskMap.put(entry.getKey().toString(), entry.getValue());
            }
            if (++idx >= 25) break;
        }
        marketContext.publishDelta(WxbcUtils.nextSequence(), exchange, symbol, updateTime, arrivedTime, updateBidMap, updateAskMap);

        TreeMap<String, BigDecimal> bidDepth = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        TreeMap<String, BigDecimal> askDepth = new TreeMap<>();
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry: bidMap.entrySet()) {
            bidDepth.put(entry.getKey().toString(), entry.getValue());
            if (++idx >= 25) break;
        }
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry: askMap.entrySet()) {
            askDepth.put(entry.getKey().toString(), entry.getValue());
            if (++idx >= 25) break;
        }
        Long msgSeq = WxbcUtils.nextSequence();
        if (lastUpdateTime == null || !DateTimeUtils.fmtDate(lastUpdateTime, "yyyy-MM-dd").equals(DateTimeUtils.fmtDate(arrivedTime, "yyyy-MM-dd"))) {
            boolean ret = marketContext.updateCrossingSnapshot(msgSeq, exchange, symbol, updateTime, arrivedTime, bidDepth, askDepth);
            if (ret) lastUpdateTime = arrivedTime;
        }
        marketContext.publishDepth(msgSeq, exchange, symbol, updateTime, arrivedTime, bidDepth, askDepth);
    }

    public void handleUpdate(LocalDateTime arrivedTime, List<JSONObject> dataList) {
        LocalDateTime updateTime = arrivedTime;

        TreeMap<String, BigDecimal> updateBidMap = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        TreeMap<String, BigDecimal> updateAskMap = new TreeMap<>();

        for (JSONObject info : dataList) {
            String side = info.getString("side");
            if ("Buy".equals(side)) {
                Long id = info.getLong("id");
                if (bidCacheMap.containsKey(id)) {
                    BigDecimal price = bidCacheMap.get(id);
                    BigDecimal size = info.getBigDecimal("size");
                    bidMap.put(price, size);
                    updateBidMap.put(price.toString(), size);
                }
            } else if ("Sell".equals(side)) {
                Long id = info.getLong("id");
                if (askCacheMap.containsKey(id)) {
                    BigDecimal price = askCacheMap.get(id);
                    BigDecimal size = info.getBigDecimal("size");
                    askMap.put(price, size);
                    updateAskMap.put(price.toString(), size);
                }
            }
        }

        marketContext.publishDelta(WxbcUtils.nextSequence(), exchange, symbol, updateTime, arrivedTime, updateBidMap, updateAskMap);

        TreeMap<String, BigDecimal> bidDepth = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        TreeMap<String, BigDecimal> askDepth = new TreeMap<>();
        int idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry: bidMap.entrySet()) {
            bidDepth.put(entry.getKey().toString(), entry.getValue());
            if (++idx >= 25) break;
        }
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry: askMap.entrySet()) {
            askDepth.put(entry.getKey().toString(), entry.getValue());
            if (++idx >= 25) break;
        }
        Long msgSeq = WxbcUtils.nextSequence();
        if (lastUpdateTime == null || !DateTimeUtils.fmtDate(lastUpdateTime, "yyyy-MM-dd").equals(DateTimeUtils.fmtDate(arrivedTime, "yyyy-MM-dd"))) {
            boolean ret = marketContext.updateCrossingSnapshot(msgSeq, exchange, symbol, updateTime, arrivedTime, bidDepth, askDepth);
            if (ret) lastUpdateTime = arrivedTime;
        }
        marketContext.publishDepth(msgSeq, exchange, symbol, updateTime, arrivedTime, bidDepth, askDepth);
    }

    public void handlInsert(LocalDateTime arrivedTime, List<JSONObject> dataList) {
        LocalDateTime updateTime = arrivedTime;

        TreeMap<String, BigDecimal> updateBidMap = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        TreeMap<String, BigDecimal> updateAskMap = new TreeMap<>();

        for (JSONObject info : dataList) {
            String side = info.getString("side");
            if ("Buy".equals(side)) {
                BigDecimal price = info.getBigDecimal("price");
                BigDecimal size = info.getBigDecimal("size");
                bidMap.put(price, size);
                bidCacheMap.put(info.getLong("id"), price);
                updateBidMap.put(price.toString(), size);
                BigDecimal bfk = bidMap.firstKey();
                while (askMap.size() > 0 && askMap.firstKey().compareTo(bfk) <= 0) {
                    updateAskMap.put(askMap.firstKey().toString(), BigDecimal.ZERO);
                    askMap.remove(askMap.firstKey());

                }
            } else if ("Sell".equals(side)) {
                BigDecimal price = info.getBigDecimal("price");
                BigDecimal size = info.getBigDecimal("size");
                askMap.put(price, size);
                askCacheMap.put(info.getLong("id"), price);
                updateAskMap.put(price.toString(), size);
                BigDecimal afk = askMap.firstKey();
                while (bidMap.size() > 0 && bidMap.firstKey().compareTo(afk) >= 0) {
                    updateBidMap.put(bidMap.firstKey().toString(), BigDecimal.ZERO);
                    bidMap.remove(bidMap.firstKey());
                }
            }
        }

        marketContext.publishDelta(WxbcUtils.nextSequence(), exchange, symbol, updateTime, arrivedTime, updateBidMap, updateAskMap);

        TreeMap<String, BigDecimal> bidDepth = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        TreeMap<String, BigDecimal> askDepth = new TreeMap<>();
        int idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry: bidMap.entrySet()) {
            bidDepth.put(entry.getKey().toString(), entry.getValue());
            if (++idx >= 25) break;
        }
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry: askMap.entrySet()) {
            askDepth.put(entry.getKey().toString(), entry.getValue());
            if (++idx >= 25) break;
        }
        Long msgSeq = WxbcUtils.nextSequence();
        if (lastUpdateTime == null || !DateTimeUtils.fmtDate(lastUpdateTime, "yyyy-MM-dd").equals(DateTimeUtils.fmtDate(arrivedTime, "yyyy-MM-dd"))) {
            boolean ret = marketContext.updateCrossingSnapshot(msgSeq, exchange, symbol, updateTime, arrivedTime, bidDepth, askDepth);
            if (ret) lastUpdateTime = arrivedTime;
        }
        marketContext.publishDepth(msgSeq, exchange, symbol, updateTime, arrivedTime, bidDepth, askDepth);
    }

    public void handleDelete(LocalDateTime arrivedTime, List<JSONObject> dataList) {
        LocalDateTime updateTime = arrivedTime;

        TreeMap<String, BigDecimal> updateBidMap = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        TreeMap<String, BigDecimal> updateAskMap = new TreeMap<>();

        for (JSONObject info : dataList) {
            String side = info.getString("side");
            if ("Buy".equals(side)) {
                Long id = info.getLong("id");
                if (bidCacheMap.containsKey(id)) {
                    BigDecimal price = bidCacheMap.get(id);
                    bidMap.remove(price);
                    bidCacheMap.remove(id);
                    updateBidMap.put(price.toString(), BigDecimal.ZERO);
                }
            } else if ("Sell".equals(side)) {
                Long id = info.getLong("id");
                if (askCacheMap.containsKey(id)) {
                    BigDecimal price = askCacheMap.get(id);
                    askMap.remove(price);
                    askCacheMap.remove(id);
                    updateAskMap.put(price.toString(), BigDecimal.ZERO);
                }
            }
        }

        marketContext.publishDelta(WxbcUtils.nextSequence(), exchange, symbol, updateTime, arrivedTime, updateBidMap, updateAskMap);

        TreeMap<String, BigDecimal> bidDepth = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        TreeMap<String, BigDecimal> askDepth = new TreeMap<>();
        int idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry: bidMap.entrySet()) {
            bidDepth.put(entry.getKey().toString(), entry.getValue());
            if (++idx >= 25) break;
        }
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry: askMap.entrySet()) {
            askDepth.put(entry.getKey().toString(), entry.getValue());
            if (++idx >= 25) break;
        }
        Long msgSeq = WxbcUtils.nextSequence();
        if (lastUpdateTime == null || !DateTimeUtils.fmtDate(lastUpdateTime, "yyyy-MM-dd").equals(DateTimeUtils.fmtDate(arrivedTime, "yyyy-MM-dd"))) {
            boolean ret = marketContext.updateCrossingSnapshot(msgSeq, exchange, symbol, updateTime, arrivedTime, bidDepth, askDepth);
            if (ret) lastUpdateTime = arrivedTime;
        }
        marketContext.publishDepth(msgSeq, exchange, symbol, updateTime, arrivedTime, bidDepth, askDepth);
    }

    public void handleTrade(LocalDateTime arrivedTime, List<JSONObject> dataList) {
        LocalDateTime updateTime = arrivedTime;
        for (JSONObject info : dataList) {
            String side = info.getString("side");
            BigDecimal price = info.getBigDecimal("price");
            BigDecimal size = info.getBigDecimal("size");
            marketContext.publishTrade(exchange, symbol, updateTime, arrivedTime, side, price, size);
        }
    }

}
