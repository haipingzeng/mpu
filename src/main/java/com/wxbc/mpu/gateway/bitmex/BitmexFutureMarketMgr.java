package com.wxbc.mpu.gateway.bitmex;

import com.wxbc.mpu.app.SpringContextUtils;
import com.wxbc.mpu.config.BitmexConfig;
import com.wxbc.mpu.config.HuobiConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class BitmexFutureMarketMgr {

    private final static Logger logger = LoggerFactory.getLogger(BitmexFutureMarketMgr.class);

    @Autowired
    BitmexConfig bitmexConfig;

    private int index = 0;

    private Map<Integer, BitmexFutureMarket> bitmexFutureMarketMap = new HashMap<>();

    private Map<String, Integer> symbolMarketMap = new HashMap<>();

    private ScheduledExecutorService eventloop = Executors.newScheduledThreadPool(1);


    public void subMarket(String symbol) {
        eventloop.execute(()->{
            logger.info("sub " + symbol);
            if (symbolMarketMap.containsKey(symbol)) return;
            boolean success = false;
            for (Map.Entry<Integer, BitmexFutureMarket> entry: bitmexFutureMarketMap.entrySet()) {
                BitmexFutureMarket bitmexFutureMarket = entry.getValue();
                if (bitmexFutureMarket.subSize() < bitmexConfig.getSubSizeLimit()) {
                    bitmexFutureMarket.subMarket(symbol);
                    symbolMarketMap.put(symbol, entry.getKey());
                    success = true;
                }
            }
            if (!success) {
                int id = ++index;
                BitmexFutureMarket bitmexFutureMarket = SpringContextUtils.getBean(BitmexFutureMarket.class, id);
                bitmexFutureMarket.start();
                bitmexFutureMarket.subMarket(symbol);
                bitmexFutureMarketMap.put(id, bitmexFutureMarket);
                symbolMarketMap.put(symbol, id);
                logger.info("start market " + Integer.toString(id));
            }
        });

    }

    public void subMarket(Set<String> symbols) {
        for (String symbol: symbols) {
            this.subMarket(symbol);
        }
    }

    public void unsubMarket(String symbol) {
        eventloop.execute(()->{
            if (symbolMarketMap.containsKey(symbol)) {
                Integer id = symbolMarketMap.get(symbol);
                BitmexFutureMarket bitmexFutureMarket = bitmexFutureMarketMap.get(id);
                if (null != bitmexFutureMarket) bitmexFutureMarket.unsubMarket(symbol);
                symbolMarketMap.remove(symbol);
                if (bitmexFutureMarket.subSize() <= 0) {
                    bitmexFutureMarket.shutdown();
                    bitmexFutureMarketMap.remove(id);
                }
            }
        });
    }

    public void unsubMarket(Set<String> symbols) {

    }
}
