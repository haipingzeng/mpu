package com.wxbc.mpu.gateway.huobi;

import com.wxbc.mpu.app.SpringContextUtils;
import com.wxbc.mpu.config.HuobiConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class HuobiCoinPerpetualMarketMgr {

    private final static Logger logger = LoggerFactory.getLogger(HuobiCoinPerpetualMarketMgr.class);

    @Autowired
    HuobiConfig huobiConfig;

    private int index = 0;

    private Map<Integer, HuobiCoinPerpetualMarket> huobiCoinPerpetualMarketMap = new HashMap<>();

    private Map<String, Integer> symbolMarketMap = new HashMap<>();

    private ScheduledExecutorService eventloop = Executors.newScheduledThreadPool(1);


    public void subMarket(String symbol) {
        eventloop.execute(()->{
            logger.info("sub " + symbol);
            if (symbolMarketMap.containsKey(symbol)) return;
            boolean success = false;
            for (Map.Entry<Integer, HuobiCoinPerpetualMarket> entry: huobiCoinPerpetualMarketMap.entrySet()) {
                HuobiCoinPerpetualMarket huobiCoinPerpetualMarket = entry.getValue();
                if (huobiCoinPerpetualMarket.subSize() < huobiConfig.getSubSizeLimit()) {
                    huobiCoinPerpetualMarket.subMarket(symbol);
                    symbolMarketMap.put(symbol, entry.getKey());
                    success = true;
                }
            }
            if (!success) {
                int id = ++index;
                HuobiCoinPerpetualMarket huobiCoinPerpetualMarket = SpringContextUtils.getBean(HuobiCoinPerpetualMarket.class, id);
                huobiCoinPerpetualMarket.start();
                huobiCoinPerpetualMarket.subMarket(symbol);
                huobiCoinPerpetualMarketMap.put(id, huobiCoinPerpetualMarket);
                symbolMarketMap.put(symbol, id);
                logger.info("start market " + Integer.toString(id));
            }
        });

    }

    public void subMarket(Set<String> symbols) {
        for (String symbol: symbols) {
            this.subMarket(symbol);
        }
    }

    public void unsubMarket(String symbol) {
        eventloop.execute(()->{
            if (symbolMarketMap.containsKey(symbol)) {
                Integer id = symbolMarketMap.get(symbol);
                HuobiCoinPerpetualMarket huobiCoinPerpetualMarket = huobiCoinPerpetualMarketMap.get(id);
                if (null != huobiCoinPerpetualMarket) huobiCoinPerpetualMarket.unsubMarket(symbol);
                symbolMarketMap.remove(symbol);
                if (huobiCoinPerpetualMarket.subSize() <= 0) {
                    huobiCoinPerpetualMarket.shutdown();
                    huobiCoinPerpetualMarketMap.remove(id);
                }
            }
        });
    }

    public void unsubMarket(Set<String> symbols) {

    }
}
