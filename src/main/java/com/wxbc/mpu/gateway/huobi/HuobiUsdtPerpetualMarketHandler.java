package com.wxbc.mpu.gateway.huobi;

import com.alibaba.fastjson.JSONObject;
import com.wxbc.mpu.market.MarketContext;
import com.wxbc.mpu.struct.WxbcType;
import com.wxbc.mpu.utils.DateTimeUtils;
import com.wxbc.mpu.utils.WxbcUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.TreeMap;

public class HuobiUsdtPerpetualMarketHandler {

    private MarketContext marketContext;

    private String exchange = WxbcType.EXCHANGE_HUOBI;

    private String symbol;

    private TreeMap<BigDecimal, BigDecimal> bidMap = null;

    private TreeMap<BigDecimal, BigDecimal> askMap = null;

    private LocalDateTime lastUpdateTime = null;

    public HuobiUsdtPerpetualMarketHandler(String symbol, MarketContext marketContext) {
        this.marketContext = marketContext;
        this.symbol = symbol;
    }

    public void handleSnapshot(LocalDateTime arrivedTime, JSONObject jData) {
        TreeMap<BigDecimal, BigDecimal> beforeBidMap = bidMap;
        TreeMap<BigDecimal, BigDecimal> beforeAskMap = askMap;

        bidMap = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        askMap = new TreeMap<>();
        LocalDateTime updateTime = DateTimeUtils.millisToUtcDateTime(jData.getLong("ts"));
        for (BigDecimal[] bid : jData.getJSONArray("bids").toJavaList(BigDecimal[].class)) {
            bidMap.put(bid[0], bid[1]);
        }
        for (BigDecimal[] ask : jData.getJSONArray("asks").toJavaList(BigDecimal[].class)) {
            askMap.put(ask[0], ask[1]);
        }

        TreeMap<String, BigDecimal> updateBidMap = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        TreeMap<String, BigDecimal> updateAskMap = new TreeMap<>();
        int idx = 0;
        if (beforeBidMap != null) {
            for (Map.Entry<BigDecimal, BigDecimal> entry : beforeBidMap.entrySet()) {
                if (!bidMap.containsKey(entry.getKey())) {
                    updateBidMap.put(entry.getKey().toString(), BigDecimal.ZERO);
                } else {
                    BigDecimal qty = bidMap.get(entry.getKey());
                    if (entry.getValue().compareTo(qty) != 0) {
                        updateBidMap.put(entry.getKey().toString(), qty);
                    }
                }
                if (++idx >= 25) break;
            }
        }
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry : bidMap.entrySet()) {
            if (beforeBidMap == null || !beforeBidMap.containsKey(entry.getKey())) {
                updateBidMap.put(entry.getKey().toString(), entry.getValue());
            }
            if (++idx >= 25) break;
        }
        idx = 0;
        if (beforeAskMap != null) {
            for (Map.Entry<BigDecimal, BigDecimal> entry : beforeAskMap.entrySet()) {
                if (!askMap.containsKey(entry.getKey())) {
                    updateAskMap.put(entry.getKey().toString(), BigDecimal.ZERO);
                } else {
                    BigDecimal qty = askMap.get(entry.getKey());
                    if (entry.getValue().compareTo(qty) != 0) {
                        updateAskMap.put(entry.getKey().toString(), qty);
                    }
                }
                if (++idx >= 25) break;
            }
        }
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry : askMap.entrySet()) {
            if (beforeAskMap == null || !beforeAskMap.containsKey(entry.getKey())) {
                updateAskMap.put(entry.getKey().toString(), entry.getValue());
            }
            if (++idx >= 25) break;
        }
        marketContext.publishDelta(WxbcUtils.nextSequence(), exchange, symbol, updateTime, arrivedTime, updateBidMap, updateAskMap);

        TreeMap<String, BigDecimal> bidDepth = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        TreeMap<String, BigDecimal> askDepth = new TreeMap<>();
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry : bidMap.entrySet()) {
            bidDepth.put(entry.getKey().toString(), entry.getValue());
            if (++idx >= 25) break;
        }
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry : askMap.entrySet()) {
            askDepth.put(entry.getKey().toString(), entry.getValue());
            if (++idx >= 25) break;
        }
        Long msgSeq = WxbcUtils.nextSequence();
        if (lastUpdateTime == null || !DateTimeUtils.fmtDate(lastUpdateTime, "yyyy-MM-dd").equals(DateTimeUtils.fmtDate(arrivedTime, "yyyy-MM-dd"))) {
            boolean ret = marketContext.updateCrossingSnapshot(msgSeq, exchange, symbol, updateTime, arrivedTime, bidDepth, askDepth);
            if (ret) lastUpdateTime = arrivedTime;
        }
        marketContext.publishDepth(msgSeq, exchange, symbol, updateTime, arrivedTime, bidDepth, askDepth);
    }

    public void handleUpdate(LocalDateTime arrivedTime, JSONObject jData) {
        TreeMap<String, BigDecimal> updateBidMap = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        TreeMap<String, BigDecimal> updateAskMap = new TreeMap<>();

        LocalDateTime updateTime = DateTimeUtils.millisToUtcDateTime(jData.getLong("ts"));
        for (BigDecimal[] bid : jData.getJSONArray("bids").toJavaList(BigDecimal[].class)) {
            if (bid[1].compareTo(BigDecimal.ZERO) <= 0) {
                bidMap.remove(bid[0]);
                updateBidMap.put(bid[0].toString(), BigDecimal.ZERO);
            } else {
                bidMap.put(bid[0], bid[1]);
                updateBidMap.put(bid[0].toString(), bid[1]);
            }
        }
        BigDecimal bfk = bidMap.firstKey();
        while (askMap.size() > 0 && askMap.firstKey().compareTo(bfk) <= 0) {
            askMap.remove(askMap.firstKey());
        }
        for (BigDecimal[] ask : jData.getJSONArray("asks").toJavaList(BigDecimal[].class)) {
            if (ask[1].compareTo(BigDecimal.ZERO) <= 0) {
                askMap.remove(ask[0]);
                updateAskMap.put(ask[0].toString(), BigDecimal.ZERO);
            } else {
                askMap.put(ask[0], ask[1]);
                updateAskMap.put(ask[0].toString(), ask[1]);
            }
        }
        BigDecimal afk = askMap.firstKey();
        while (bidMap.size() > 0 && bidMap.firstKey().compareTo(afk) >= 0) {
            bidMap.remove(bidMap.firstKey());
        }

        marketContext.publishDelta(WxbcUtils.nextSequence(), exchange, symbol, updateTime, arrivedTime, updateBidMap, updateAskMap);
        TreeMap<String, BigDecimal> bidDepth = new TreeMap<>((o1, o2) -> {
            return o2.compareTo(o1);
        });
        TreeMap<String, BigDecimal> askDepth = new TreeMap<>();
        int idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry : bidMap.entrySet()) {
            bidDepth.put(entry.getKey().toString(), entry.getValue());
            if (++idx >= 25) break;
        }
        idx = 0;
        for (Map.Entry<BigDecimal, BigDecimal> entry : askMap.entrySet()) {
            askDepth.put(entry.getKey().toString(), entry.getValue());
            if (++idx >= 25) break;
        }
        Long msgSeq = WxbcUtils.nextSequence();
        if (lastUpdateTime == null || !DateTimeUtils.fmtDate(lastUpdateTime, "yyyy-MM-dd").equals(DateTimeUtils.fmtDate(arrivedTime, "yyyy-MM-dd"))) {
            boolean ret = marketContext.updateCrossingSnapshot(msgSeq, exchange, symbol, updateTime, arrivedTime, bidDepth, askDepth);
            if (ret) lastUpdateTime = arrivedTime;
        }
        marketContext.publishDepth(msgSeq, exchange, symbol, updateTime, arrivedTime, bidDepth, askDepth);
    }


    public void handleTrade(LocalDateTime arrivedTime, JSONObject jData) {
        LocalDateTime updateTime = DateTimeUtils.millisToUtcDateTime(jData.getLong("ts"));
        for (JSONObject info : jData.getJSONArray("data").toJavaList(JSONObject.class)) {
            String direction = info.getString("direction");
            if ("buy".equals(direction)) {
                direction = "Buy";
            } else if ("sell".equals(direction)) {
                direction = "Sell";
            }
            BigDecimal price = info.getBigDecimal("price");
            BigDecimal quantity = info.getBigDecimal("quantity");
            marketContext.publishTrade(exchange, symbol, updateTime, arrivedTime, direction, price, quantity);
        }
    }
}
