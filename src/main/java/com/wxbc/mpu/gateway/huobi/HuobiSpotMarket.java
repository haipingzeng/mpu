package com.wxbc.mpu.gateway.huobi;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wxbc.mpu.config.HuobiConfig;
import com.wxbc.mpu.market.MarketContext;
import com.wxbc.mpu.utils.DateTimeUtils;
import com.wxbc.mpu.utils.WxbcUtils;
import com.wxbc.mpu.wss.WssClient;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.*;
import java.util.zip.GZIPInputStream;

/**
 * HUOBI现货行情对接，增量盘口和成交分别有不同的端点
 */
@Component
@Scope("prototype")
public class HuobiSpotMarket {

    private final static Logger logger = LoggerFactory.getLogger(HuobiSpotMarket.class);

    @Autowired
    HuobiConfig huobiConfig;

    @Autowired
    MarketContext marketContext;

    private Integer id;

    private Set<String> subSymbols = new ConcurrentSkipListSet<>();

    private Map<String, String> wxbcHuobiSymbolMap = new HashMap<>();

    private Map<String, String> huobiWxbcSymbolMap = new HashMap<>();

    private ScheduledExecutorService eventloop = new ScheduledThreadPoolExecutor(1, new BasicThreadFactory.Builder().namingPattern("HuobiSpotMarket").daemon(true).build());



    private Map<String, HuobiSpotMarketHandler> huobiSpotMarketHandlerMap = new HashMap<>();

    private HuobiSpotDepthWssClient huobiSpotDepthWssClient = null;

    private HuobiSpotTradeWssClient huobiSpotTradeWssClient = null;

    public HuobiSpotMarket(Integer id) {
        this.id = id;
        huobiSpotDepthWssClient = new HuobiSpotDepthWssClient(id);
        huobiSpotTradeWssClient = new HuobiSpotTradeWssClient(id);


    }

    public void start() {
        huobiSpotDepthWssClient.start();
        huobiSpotTradeWssClient.start();
    }

    public void stop() {
        huobiSpotDepthWssClient.stop();
        huobiSpotTradeWssClient.stop();
    }

    public void shutdown() {
        huobiSpotDepthWssClient.shutdown();
        huobiSpotTradeWssClient.shutdown();
        eventloop.shutdown();
    }

    public int subSize() {
        return subSymbols.size();
    }

    public void subMarket(String symbol) {
        String exchSymbol = getExchSymbol(symbol);
        logger.info("subMarket symbol: " + symbol + " exchSymbol: " + exchSymbol);
        if (exchSymbol != null) {
            subSymbols.add(symbol);
            huobiSpotDepthWssClient.subMarket(exchSymbol);
            huobiSpotTradeWssClient.subMarket(exchSymbol);
        }
    }

    public void unsubMarket(String symbol) {
        String exchSymbol = getExchSymbol(symbol);
        logger.info("unsubMarket symbol: " + symbol + " exchSymbol: " + exchSymbol);
        if (exchSymbol != null) {
            subSymbols.remove(symbol);
            huobiSpotDepthWssClient.unsubMarket(exchSymbol);
            huobiSpotTradeWssClient.unsubMarket(exchSymbol);
        }
    }


    private String getExchSymbol(String symbol) {
        try {
            if (wxbcHuobiSymbolMap.containsKey(symbol)) return wxbcHuobiSymbolMap.get(symbol);
            String exchSymbol = symbol.replace("_", "").toLowerCase();
            wxbcHuobiSymbolMap.put(symbol, exchSymbol);
            huobiWxbcSymbolMap.put(exchSymbol, symbol);
            return exchSymbol;
        } catch (Exception e) {

        }
        return null;
    }



    class HuobiSpotDepthWssClient extends WssClient {

        private volatile Channel channel = null;

        private LocalDateTime lastReceiveTime = DateTimeUtils.utcnow();

        private Map<String, HuobiSubRecord> recordMap = new HashMap<>();

        private Map<String, LocalDateTime> lastReqTime = new HashMap<>();

        public HuobiSpotDepthWssClient(Integer id) {
            super("HuobiSpotDepth " + id);

            // 检查行情是否很久没收到行情
            eventloop.scheduleWithFixedDelay(()->{
                LocalDateTime utcnow = DateTimeUtils.utcnow();
                if (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(lastReceiveTime) > 5000) {
                    if (getRunning() && this.channel != null && subSize() > 0) {
                        this.channel.close();
                    }
                }
            }, 5, 5, TimeUnit.SECONDS);

            // 订阅失败继续订阅，最多订阅三次
            eventloop.scheduleWithFixedDelay(()->{
                try {
                    if (null == this.channel) return;
                    LocalDateTime utcnow = DateTimeUtils.utcnow();
                    var it = recordMap.entrySet().iterator();
                    while (it.hasNext()) {
                        var entry = it.next();
                        if (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(entry.getValue().getTime()) > 8000) {
                            if (entry.getValue().getState() > 0) {
                                entry.getValue().setState(entry.getValue().getState() - 1);
                                JSONObject subMsg = new JSONObject();
                                subMsg.put("sub", new StringBuilder("market.").append(entry.getValue().getSymbol()).append(".mbp.20").toString());
                                subMsg.put("id", WxbcUtils.nextSequence().toString());
                                this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));
                            }
                            if (entry.getValue().getState() <= 0) {
                                it.remove();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, 20, 20, TimeUnit.SECONDS);
        }

        public void start() {
            super.connect(huobiConfig.getSpotDepthWssUrl());
        }

        public void stop() {
            super.disconnect();
        }

        public void shutdown() {
            super.shutdownGracefully();
        }

        public void subMarket(String exchSymbol) {
            eventloop.execute(() -> {
                recordMap.put(exchSymbol, new HuobiSubRecord(exchSymbol, DateTimeUtils.utcnow()));
                if (this.channel != null) {

                    JSONObject subMsg = new JSONObject();
                    subMsg.put("sub", new StringBuilder("market.").append(exchSymbol).append(".mbp.20").toString());
                    subMsg.put("id", WxbcUtils.nextSequence().toString());
                    this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));
                }
            });
        }

        public void unsubMarket(String exchSymbol) {

        }

        @Override
        protected void onOpen(Channel channel) {
            eventloop.execute(()->{
                this.channel = channel;
                for (HuobiSpotMarketHandler huobiSpotMarketHandler : huobiSpotMarketHandlerMap.values()) {
                    huobiSpotMarketHandler.setReadyStatus(0);
                }
                recordMap = new HashMap<>();
                for (String symbol: subSymbols) {
                    String exchSymbol = getExchSymbol(symbol);
                    if (null == exchSymbol) continue;

                    recordMap.put(exchSymbol, new HuobiSubRecord(exchSymbol, DateTimeUtils.utcnow()));
                    JSONObject subMsg = new JSONObject();
                    subMsg.put("sub", new StringBuilder("market.").append(exchSymbol).append(".mbp.20").toString());
                    subMsg.put("id", WxbcUtils.nextSequence().toString());
                    this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));
                }
            });

        }

        @Override
        protected void onMessage(Channel channel, byte[] msg) {
            eventloop.execute(()->handleOnMessage(channel, msg, DateTimeUtils.utcnow()));
        }

        @Override
        protected void onMessage(Channel channel, String msg) {

        }

        @Override
        protected void onClose(Channel channel) {
            eventloop.execute(()->{
                this.channel = null;
            });

        }

        @Override
        protected void onThrowable(Throwable e) {
        }

        private void handleOnMessage(Channel channel, byte[] msg, LocalDateTime arrivedTime) {
            try {
                lastReceiveTime = DateTimeUtils.utcnow();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                ByteArrayInputStream in = new ByteArrayInputStream(msg);
                GZIPInputStream gunzip = new GZIPInputStream(in);
                byte[] buffer = new byte[256];
                int n;
                while ((n = gunzip.read(buffer)) >= 0) {
                    out.write(buffer, 0, n);
                }
                JSONObject jmsg = JSON.parseObject(out.toString());
                if (jmsg.containsKey("ping")) {
                    jmsg.put("pong", jmsg.get("ping"));
                    jmsg.remove("ping");
                    channel.writeAndFlush(new TextWebSocketFrame(jmsg.toJSONString()));
                } else if (jmsg.containsKey("ch")) {
                    String topicInfos[] = jmsg.getString("ch").split("\\.");
                    if (topicInfos.length == 4) {
                        if ("market".equals(topicInfos[0]) && "mbp".equals(topicInfos[2])) {
//                        logger.info("mbp: " + jmsg.toJSONString());
                            String symbol = huobiWxbcSymbolMap.get(topicInfos[1]);
                            if (null != symbol) {
                                if (!huobiSpotMarketHandlerMap.containsKey(symbol)) {
                                    huobiSpotMarketHandlerMap.put(symbol, new HuobiSpotMarketHandler(symbol, marketContext));
                                }
                                HuobiSpotMarketHandler huobiSpotMarketHandler = huobiSpotMarketHandlerMap.get(symbol);
                                if (huobiSpotMarketHandler.getReadyStatus() == 0) {
                                    if (!lastReqTime.containsKey(topicInfos[1]) || DateTimeUtils.toMillisOfUtc(lastReceiveTime) - DateTimeUtils.toMillisOfUtc(lastReqTime.get(topicInfos[1])) > 1000) {
                                        lastReqTime.put(topicInfos[1], lastReceiveTime);
                                        huobiSpotMarketHandler.setReadyStatus(1);
                                        JSONObject req = new JSONObject();
                                        req.put("req", new StringBuilder("market.").append(topicInfos[1]).append(".mbp.20").toString());
                                        req.put("id", WxbcUtils.nextSequence().toString());
                                        this.channel.writeAndFlush(new TextWebSocketFrame(req.toJSONString()));
                                    }
                                }
                                huobiSpotMarketHandler.handleUpdate(arrivedTime, jmsg);
                            }
                        }
                    }
                } else if (jmsg.containsKey("rep")) {
                    logger.info("rep: " + jmsg.toJSONString());
                    String topicInfos[] = jmsg.getString("rep").split("\\.");
                    if (topicInfos.length == 4) {
                        if ("market".equals(topicInfos[0]) && "mbp".equals(topicInfos[2])) {
                            JSONObject data = jmsg.getJSONObject("data");
                            String symbol = huobiWxbcSymbolMap.get(topicInfos[1]);
                            if (null != symbol) {
                                HuobiSpotMarketHandler huobiSpotMarketHandler = huobiSpotMarketHandlerMap.get(symbol);
                                huobiSpotMarketHandler.setReadyStatus(2);
                                huobiSpotMarketHandler.handleSnapshot(arrivedTime, jmsg);
                            }
                        }
                    }
                } else if (jmsg.containsKey("subbed")) {
                    logger.info(jmsg.toJSONString());
                    String subbed = jmsg.getString("subbed");
                    String[] details = subbed.split("\\.");
                    recordMap.remove(details[1]);
                } else if (jmsg.containsKey("err-msg")) {
                    logger.info(jmsg.toJSONString());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    class HuobiSpotTradeWssClient extends WssClient {

        private volatile Channel channel = null;

        private LocalDateTime lastReceiveTime = DateTimeUtils.utcnow();

        private Map<String, HuobiSubRecord> recordMap = new HashMap<>();

        public HuobiSpotTradeWssClient(Integer id) {
            super("HuobiSpotTrade " + id);

            // 检查行情是否很久没收到行情
            eventloop.scheduleWithFixedDelay(()->{
                LocalDateTime utcnow = DateTimeUtils.utcnow();
                if (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(lastReceiveTime) > 5000) {
                    if (getRunning() && this.channel != null && subSize() > 0) {
                        this.channel.close();
                    }
                }
            }, 5, 5, TimeUnit.SECONDS);

            // 订阅失败继续订阅，最多订阅三次
            eventloop.scheduleWithFixedDelay(()->{
                try {
                    if (null == this.channel) return;
                    LocalDateTime utcnow = DateTimeUtils.utcnow();
                    var it = recordMap.entrySet().iterator();
                    while (it.hasNext()) {
                        var entry = it.next();
                        if (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(entry.getValue().getTime()) > 8000) {
                            if (entry.getValue().getState() > 0) {
                                entry.getValue().setState(entry.getValue().getState() - 1);
                                JSONObject subTradeMsg = new JSONObject();
                                subTradeMsg.put("sub", new StringBuilder("market.").append(entry.getValue().getSymbol()).append(".trade.detail").toString());
                                subTradeMsg.put("id", WxbcUtils.nextSequence().toString());
                                this.channel.writeAndFlush(new TextWebSocketFrame(subTradeMsg.toJSONString()));
                            }
                            if (entry.getValue().getState() <= 0) {
                                it.remove();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, 20, 20, TimeUnit.SECONDS);
        }

        public void start() {
            super.connect(huobiConfig.getSpotTradeWssUrl());
        }

        public void stop() {
            super.disconnect();
        }

        public void shutdown() {
            super.shutdownGracefully();
        }

        public void subMarket(String exchSymbol) {
            eventloop.execute(() -> {
                recordMap.put(exchSymbol, new HuobiSubRecord(exchSymbol, DateTimeUtils.utcnow()));
                if (this.channel != null) {
                    JSONObject subTradeMsg = new JSONObject();
                    subTradeMsg.put("sub", new StringBuilder("market.").append(exchSymbol).append(".trade.detail").toString());
                    subTradeMsg.put("id", WxbcUtils.nextSequence().toString());
                    this.channel.writeAndFlush(new TextWebSocketFrame(subTradeMsg.toJSONString()));

                }
            });
        }

        public void unsubMarket(String exchSymbol) {

        }

        @Override
        protected void onOpen(Channel channel) {
            eventloop.execute(()->{
                this.channel = channel;
                for (HuobiSpotMarketHandler huobiSpotMarketHandler : huobiSpotMarketHandlerMap.values()) {
                    huobiSpotMarketHandler.setReadyStatus(0);
                }
                recordMap = new HashMap<>();
                for (String symbol: subSymbols) {
                    String exchSymbol = getExchSymbol(symbol);
                    if (null == exchSymbol) continue;
                    recordMap.put(exchSymbol, new HuobiSubRecord(exchSymbol, DateTimeUtils.utcnow()));
                    JSONObject subTradeMsg = new JSONObject();
                    subTradeMsg.put("sub", new StringBuilder("market.").append(exchSymbol).append(".trade.detail").toString());
                    subTradeMsg.put("id", WxbcUtils.nextSequence().toString());
                    this.channel.writeAndFlush(new TextWebSocketFrame(subTradeMsg.toJSONString()));
                }
            });

        }

        @Override
        protected void onMessage(Channel channel, byte[] msg) {
            eventloop.execute(()->handleOnMessage(channel, msg, DateTimeUtils.utcnow()));
        }

        @Override
        protected void onMessage(Channel channel, String msg) {

        }

        @Override
        protected void onClose(Channel channel) {
            eventloop.execute(()->{
                this.channel = null;
            });

        }

        @Override
        protected void onThrowable(Throwable e) {
        }

        private void handleOnMessage(Channel channel, byte[] msg, LocalDateTime arrivedTime) {
            try {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                ByteArrayInputStream in = new ByteArrayInputStream(msg);
                GZIPInputStream gunzip = new GZIPInputStream(in);
                byte[] buffer = new byte[256];
                int n;
                while ((n = gunzip.read(buffer)) >= 0) {
                    out.write(buffer, 0, n);
                }
                JSONObject jmsg = JSON.parseObject(out.toString());
                if (jmsg.containsKey("ping")) {
                    jmsg.put("pong", jmsg.get("ping"));
                    jmsg.remove("ping");
                    channel.writeAndFlush(new TextWebSocketFrame(jmsg.toJSONString()));
                } else if (jmsg.containsKey("ch")) {
                    String topicInfos[] = jmsg.getString("ch").split("\\.");
                    if (topicInfos.length == 4) {
                        if ("market".equals(topicInfos[0]) && "trade".equals(topicInfos[2])) {
                            String symbol = huobiWxbcSymbolMap.get(topicInfos[1]);
                            if (null != symbol) {
                                JSONObject tick = jmsg.getJSONObject("tick");
                                if (!huobiSpotMarketHandlerMap.containsKey(symbol)) {
                                    huobiSpotMarketHandlerMap.put(symbol, new HuobiSpotMarketHandler(symbol, marketContext));
                                }
                                HuobiSpotMarketHandler huobiSpotMarketHandler = huobiSpotMarketHandlerMap.get(symbol);
                                huobiSpotMarketHandler.handleTrade(arrivedTime, tick);
                            }
                        }
                    }
                } else if (jmsg.containsKey("subbed")) {
                    logger.info(jmsg.toJSONString());
                    String subbed = jmsg.getString("subbed");
                    String[] details = subbed.split("\\.");
                    recordMap.remove(details[1]);
                } else if (jmsg.containsKey("err-msg")) {
                    logger.info(jmsg.toJSONString());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            lastReceiveTime = DateTimeUtils.utcnow();
        }
    }
}
