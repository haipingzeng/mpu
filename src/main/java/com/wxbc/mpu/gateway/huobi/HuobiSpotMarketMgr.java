package com.wxbc.mpu.gateway.huobi;

import com.wxbc.mpu.app.SpringContextUtils;
import com.wxbc.mpu.config.HuobiConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class HuobiSpotMarketMgr {

    private final static Logger logger = LoggerFactory.getLogger(HuobiSpotMarketMgr.class);

    @Autowired
    HuobiConfig huobiConfig;

    private int index = 0;

    private Map<Integer, HuobiSpotMarket> huobiSpotMarketMap = new HashMap<>();

    private Map<String, Integer> symbolMarketMap = new HashMap<>();

    private ScheduledExecutorService eventloop = Executors.newScheduledThreadPool(1);


    public void subMarket(String symbol) {
        eventloop.execute(()->{
            logger.info("sub " + symbol);
            if (symbolMarketMap.containsKey(symbol)) return;
            boolean success = false;
            for (Map.Entry<Integer, HuobiSpotMarket> entry: huobiSpotMarketMap.entrySet()) {
                HuobiSpotMarket huobiSpotMarket = entry.getValue();
                if (huobiSpotMarket.subSize() < huobiConfig.getSubSizeLimit()) {
                    huobiSpotMarket.subMarket(symbol);
                    symbolMarketMap.put(symbol, entry.getKey());
                    success = true;
                }
            }
            if (!success) {
                int id = ++index;
                HuobiSpotMarket huobiSpotMarket = SpringContextUtils.getBean(HuobiSpotMarket.class, id);
                huobiSpotMarket.start();
                huobiSpotMarket.subMarket(symbol);
                huobiSpotMarketMap.put(id, huobiSpotMarket);
                symbolMarketMap.put(symbol, id);
                logger.info("start market " + Integer.toString(id));
            }
        });

    }

    public void subMarket(Set<String> symbols) {
        for (String symbol: symbols) {
            this.subMarket(symbol);
        }
    }

    public void unsubMarket(String symbol) {
        eventloop.execute(()->{
            if (symbolMarketMap.containsKey(symbol)) {
                Integer id = symbolMarketMap.get(symbol);
                HuobiSpotMarket huobiSpotMarket = huobiSpotMarketMap.get(id);
                if (null != huobiSpotMarket) huobiSpotMarket.unsubMarket(symbol);
                symbolMarketMap.remove(symbol);
                if (huobiSpotMarket.subSize() <= 0) {
                    huobiSpotMarket.shutdown();
                    huobiSpotMarketMap.remove(id);
                }
            }
        });
    }

    public void unsubMarket(Set<String> symbols) {

    }
}
