package com.wxbc.mpu.gateway.huobi;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wxbc.mpu.config.HuobiConfig;
import com.wxbc.mpu.market.MarketContext;
import com.wxbc.mpu.utils.DateTimeUtils;
import com.wxbc.mpu.utils.WxbcUtils;
import com.wxbc.mpu.wss.WssClient;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.*;
import java.util.zip.GZIPInputStream;

/**
 * HUOBI交割合约行情的对接
 */

@Component
@Scope("prototype")
public class HuobiCoinDeliveryMarket extends WssClient {

    private final static Logger logger = LoggerFactory.getLogger(HuobiCoinDeliveryMarket.class);

    @Autowired
    HuobiConfig huobiConfig;

    @Autowired
    MarketContext marketContext;

    private Integer id;

    private Set<String> subSymbols = new ConcurrentSkipListSet<>();

    private Map<String, String> wxbcHuobiSymbolMap = new HashMap<>();

    private Map<String, String> huobiWxbcSymbolMap = new HashMap<>();

    private volatile Channel channel = null;

    private LocalDateTime lastReceiveTime = DateTimeUtils.utcnow();

    private ScheduledExecutorService eventloop = new ScheduledThreadPoolExecutor(1, new BasicThreadFactory.Builder().namingPattern("HuobiDeliveryMarket").daemon(true).build());


    private Map<String, HuobiCoinDeliveryMarketHandler> huobiCoinDeliveryMarketHandlerHashMap = new HashMap<>();


    private Map<String, HuobiSubRecord> subTradeRecordMap = new HashMap<>();
    private Map<String, HuobiSubRecord> subDepthRecordMap = new HashMap<>();

    public HuobiCoinDeliveryMarket(Integer id) {
        super("HuobiDeliveryMarket " + id);
        this.id = id;

        // 检查行情是否很久没收到行情
        eventloop.scheduleWithFixedDelay(()->{
            LocalDateTime utcnow = DateTimeUtils.utcnow();
            if (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(lastReceiveTime) > 5000) {
                if (getRunning() && this.channel != null && subSize() > 0) {
                    this.channel.close();
                }
            }
        }, 5, 5, TimeUnit.SECONDS);


        eventloop.scheduleWithFixedDelay(()->{
            if (this.channel != null) {
                for (String symbol : subSymbols) {
                    String exchSymbol = getExchSymbol(symbol);
                    JSONObject subMsg = new JSONObject();
                    subMsg.put("sub", new StringBuilder("market.").append(exchSymbol).append(".depth.size_20.high_freq"));
                    subMsg.put("id", WxbcUtils.nextSequence().toString());
                    subMsg.put("data_type", "incremental");
                    this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));
                }
            }
        }, 1800, 1800, TimeUnit.SECONDS);

        // 订阅失败继续订阅，最多订阅三次
        eventloop.scheduleWithFixedDelay(()->{
            try {
                if (null == this.channel) return;
                LocalDateTime utcnow = DateTimeUtils.utcnow();
                {
                    var it = subDepthRecordMap.entrySet().iterator();
                    while (it.hasNext()) {
                        var entry = it.next();
                        if (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(entry.getValue().getTime()) > 8000) {
                            if (entry.getValue().getState() > 0) {
                                entry.getValue().setState(entry.getValue().getState() - 1);
                                JSONObject subMsg = new JSONObject();
                                subMsg.put("sub", new StringBuilder("market.").append(entry.getValue().getSymbol()).append(".depth.size_20.high_freq"));
                                subMsg.put("id", WxbcUtils.nextSequence().toString());
                                subMsg.put("data_type", "incremental");
                                this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));
                            }
                            if (entry.getValue().getState() <= 0) {
                                it.remove();
                            }
                        }
                    }
                }
                {
                    var it = subTradeRecordMap.entrySet().iterator();
                    while (it.hasNext()) {
                        var entry = it.next();
                        if (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(entry.getValue().getTime()) > 8000) {
                            if (entry.getValue().getState() > 0) {
                                entry.getValue().setState(entry.getValue().getState() - 1);
                                JSONObject subTradeMsg = new JSONObject();
                                subTradeMsg.put("sub", new StringBuilder("market.").append(entry.getValue().getSymbol()).append(".trade.detail"));
                                subTradeMsg.put("id", WxbcUtils.nextSequence().toString());
                                this.channel.writeAndFlush(new TextWebSocketFrame(subTradeMsg.toJSONString()));
                            }
                            if (entry.getValue().getState() <= 0) {
                                it.remove();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, 20, 20, TimeUnit.SECONDS);
    }

    public void start() {
        super.connect(huobiConfig.getCoinDeliveryWssUrl());
    }

    public void stop() {
        super.disconnect();
    }

    public void shutdown() {
        super.shutdownGracefully();
        eventloop.shutdown();
    }

    public int subSize() {
        return subSymbols.size();
    }

    public void subMarket(String symbol) {
        String exchSymbol = getExchSymbol(symbol);
        logger.info("subMarket symbol: " + symbol + " exchSymbol: " + exchSymbol);
        if (exchSymbol != null) {
            subSymbols.add(symbol);
            eventloop.execute(() -> {
                subDepthRecordMap.put(exchSymbol, new HuobiSubRecord(exchSymbol, DateTimeUtils.utcnow()));
                subTradeRecordMap.put(exchSymbol, new HuobiSubRecord(exchSymbol, DateTimeUtils.utcnow()));
                if (this.channel != null) {

                    JSONObject subMsg = new JSONObject();
                    subMsg.put("sub", new StringBuilder("market.").append(exchSymbol).append(".depth.size_20.high_freq"));
                    subMsg.put("id", WxbcUtils.nextSequence().toString());
                    subMsg.put("data_type", "incremental");
                    this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));

                    JSONObject subTradeMsg = new JSONObject();
                    subTradeMsg.put("sub", new StringBuilder("market.").append(exchSymbol).append(".trade.detail"));
                    subTradeMsg.put("id", WxbcUtils.nextSequence().toString());
                    this.channel.writeAndFlush(new TextWebSocketFrame(subTradeMsg.toJSONString()));
                }
            });
        }
    }

    public void unsubMarket(String symbol) {
        String exchSymbol = getExchSymbol(symbol);
        logger.info("unsubMarket symbol: " + symbol + " exchSymbol: " + exchSymbol);
        if (exchSymbol != null) {
            subSymbols.remove(symbol);
            eventloop.execute(() -> {

            });
        }
    }


    private String getExchSymbol(String symbol) {
        try {
            if (wxbcHuobiSymbolMap.containsKey(symbol)) return wxbcHuobiSymbolMap.get(symbol);
            String exchSymbol = symbol.replace("_USD@", "");
            wxbcHuobiSymbolMap.put(symbol, exchSymbol);
            huobiWxbcSymbolMap.put(exchSymbol, symbol);
            return exchSymbol;
        } catch (Exception e) {

        }
        return null;
    }



    @Override
    protected void onOpen(Channel channel) {
        eventloop.execute(()->{
            this.channel = channel;
            subDepthRecordMap = new HashMap<>();
            subTradeRecordMap = new HashMap<>();
            for (String symbol: subSymbols) {
                String exchSymbol = getExchSymbol(symbol);
                if (null == exchSymbol) continue;
                subDepthRecordMap.put(exchSymbol, new HuobiSubRecord(exchSymbol, DateTimeUtils.utcnow()));
                JSONObject subMsg = new JSONObject();
                subMsg.put("sub", new StringBuilder("market.").append(exchSymbol).append(".depth.size_20.high_freq"));
                subMsg.put("id", WxbcUtils.nextSequence().toString());
                subMsg.put("data_type", "incremental");
                this.channel.writeAndFlush(new TextWebSocketFrame(subMsg.toJSONString()));

                subTradeRecordMap.put(exchSymbol, new HuobiSubRecord(exchSymbol, DateTimeUtils.utcnow()));
                JSONObject subTradeMsg = new JSONObject();
                subTradeMsg.put("sub", new StringBuilder("market.").append(exchSymbol).append(".trade.detail"));
                subTradeMsg.put("id", WxbcUtils.nextSequence().toString());
                this.channel.writeAndFlush(new TextWebSocketFrame(subTradeMsg.toJSONString()));
            }
        });

    }

    @Override
    protected void onMessage(Channel channel, byte[] msg) {
        eventloop.execute(()->handleOnMessage(channel, msg, DateTimeUtils.utcnow()));
    }

    @Override
    protected void onMessage(Channel channel, String msg) {

    }

    @Override
    protected void onClose(Channel channel) {
        eventloop.execute(()->{
            this.channel = null;
        });

    }

    @Override
    protected void onThrowable(Throwable e) {
    }

    private void handleOnMessage(Channel channel, byte[] msg, LocalDateTime arrivedTime) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ByteArrayInputStream in = new ByteArrayInputStream(msg);
            GZIPInputStream gunzip = new GZIPInputStream(in);
            byte[] buffer = new byte[256];
            int n;
            while ((n = gunzip.read(buffer)) >= 0) {
                out.write(buffer, 0, n);
            }
            JSONObject jmsg = JSON.parseObject(out.toString());
//            logger.info(jmsg.toJSONString());
            if (jmsg.containsKey("ping")) {
                jmsg.put("pong", jmsg.get("ping"));
                jmsg.remove("ping");
                channel.writeAndFlush(new TextWebSocketFrame(jmsg.toJSONString()));
            } else if (jmsg.containsKey("ch")) {
                String topicInfos[] = jmsg.getString("ch").split("\\.");
                if (topicInfos.length == 5) {
                    if ("market".equals(topicInfos[0]) && "depth".equals(topicInfos[2])) {
                        JSONObject tick = jmsg.getJSONObject("tick");
                        String symbol = huobiWxbcSymbolMap.get(topicInfos[1]);
                        if (null != symbol) {
                            if (!huobiCoinDeliveryMarketHandlerHashMap.containsKey(symbol)) {
                                huobiCoinDeliveryMarketHandlerHashMap.put(symbol, new HuobiCoinDeliveryMarketHandler(symbol, marketContext));
                            }
                            String event = tick.getString("event");
                            if ("snapshot".equals(event)) {
                                huobiCoinDeliveryMarketHandlerHashMap.get(symbol).handleSnapshot(arrivedTime, tick);
                            } else if ("update".equals(event)) {
                                huobiCoinDeliveryMarketHandlerHashMap.get(symbol).handleUpdate(arrivedTime, tick);
                            }
                        }
                    }
                } else if (topicInfos.length == 4) {
                    if ("market".equals(topicInfos[0]) && "trade".equals(topicInfos[2])) {
                        JSONObject tick = jmsg.getJSONObject("tick");
                        String symbol = huobiWxbcSymbolMap.get(topicInfos[1]);
                        if (null != symbol) {
                            if (!huobiCoinDeliveryMarketHandlerHashMap.containsKey(symbol)) {
                                huobiCoinDeliveryMarketHandlerHashMap.put(symbol, new HuobiCoinDeliveryMarketHandler(symbol, marketContext));
                            }
                            huobiCoinDeliveryMarketHandlerHashMap.get(symbol).handleTrade(arrivedTime, tick);
                        }

                    }
                }
            } else if (jmsg.containsKey("subbed")){
                logger.info(jmsg.toJSONString());
                String subbed = jmsg.getString("subbed");
                String[] details = subbed.split("\\.");
                if ("depth".equals(details[2])) {
                    subDepthRecordMap.remove(details[1]);
                } else if ("trade".equals(details[2])) {
                    subTradeRecordMap.remove(details[1]);
                }
            } else {
                logger.info("HuobiDeliveryMarket " + id + " " + jmsg.toJSONString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        lastReceiveTime = DateTimeUtils.utcnow();
    }
}
