package com.wxbc.mpu.gateway.huobi;

import com.wxbc.mpu.app.SpringContextUtils;
import com.wxbc.mpu.config.HuobiConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class HuobiUsdtPerpetualMarketMgr {

    private final static Logger logger = LoggerFactory.getLogger(HuobiUsdtPerpetualMarketMgr.class);

    @Autowired
    HuobiConfig huobiConfig;

    private int index = 0;

    private Map<Integer, HuobiUsdtPerpetualMarket> huobiUsdtPerpetualMarketMap = new HashMap<>();

    private Map<String, Integer> symbolMarketMap = new HashMap<>();

    private ScheduledExecutorService eventloop = Executors.newScheduledThreadPool(1);


    public void subMarket(String symbol) {
        eventloop.execute(()->{
            logger.info("sub " + symbol);
            if (symbolMarketMap.containsKey(symbol)) return;
            boolean success = false;
            for (Map.Entry<Integer, HuobiUsdtPerpetualMarket> entry: huobiUsdtPerpetualMarketMap.entrySet()) {
                HuobiUsdtPerpetualMarket huobiUsdtPerpetualMarket = entry.getValue();
                if (huobiUsdtPerpetualMarket.subSize() < huobiConfig.getSubSizeLimit()) {
                    huobiUsdtPerpetualMarket.subMarket(symbol);
                    symbolMarketMap.put(symbol, entry.getKey());
                    success = true;
                }
            }
            if (!success) {
                int id = ++index;
                HuobiUsdtPerpetualMarket huobiUsdtPerpetualMarket = SpringContextUtils.getBean(HuobiUsdtPerpetualMarket.class, id);
                huobiUsdtPerpetualMarket.start();
                huobiUsdtPerpetualMarket.subMarket(symbol);
                huobiUsdtPerpetualMarketMap.put(id, huobiUsdtPerpetualMarket);
                symbolMarketMap.put(symbol, id);
                logger.info("start market " + Integer.toString(id));
            }
        });

    }

    public void subMarket(Set<String> symbols) {
        for (String symbol: symbols) {
            this.subMarket(symbol);
        }
    }

    public void unsubMarket(String symbol) {
        eventloop.execute(()->{
            if (symbolMarketMap.containsKey(symbol)) {
                Integer id = symbolMarketMap.get(symbol);
                HuobiUsdtPerpetualMarket huobiUsdtPerpetualMarket = huobiUsdtPerpetualMarketMap.get(id);
                if (null != huobiUsdtPerpetualMarket) huobiUsdtPerpetualMarket.unsubMarket(symbol);
                symbolMarketMap.remove(symbol);
                if (huobiUsdtPerpetualMarket.subSize() <= 0) {
                    huobiUsdtPerpetualMarket.shutdown();
                    huobiUsdtPerpetualMarketMap.remove(id);
                }
            }
        });
    }

    public void unsubMarket(Set<String> symbols) {

    }
}
