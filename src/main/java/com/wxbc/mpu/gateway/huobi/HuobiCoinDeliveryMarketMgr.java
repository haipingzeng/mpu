package com.wxbc.mpu.gateway.huobi;

import com.wxbc.mpu.app.SpringContextUtils;
import com.wxbc.mpu.config.HuobiConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class HuobiCoinDeliveryMarketMgr {

    private final static Logger logger = LoggerFactory.getLogger(HuobiCoinDeliveryMarketMgr.class);

    @Autowired
    HuobiConfig huobiConfig;

    private int index = 0;

    private Map<Integer, HuobiCoinDeliveryMarket> huobiCoinDeliveryMarketMap = new HashMap<>();

    private Map<String, Integer> symbolMarketMap = new HashMap<>();

    private ScheduledExecutorService eventloop = Executors.newScheduledThreadPool(1);


    public void subMarket(String symbol) {
        eventloop.execute(()->{
            logger.info("sub " + symbol);
            if (symbolMarketMap.containsKey(symbol)) return;
            boolean success = false;
            for (Map.Entry<Integer, HuobiCoinDeliveryMarket> entry: huobiCoinDeliveryMarketMap.entrySet()) {
                HuobiCoinDeliveryMarket huobiCoinDeliveryMarket = entry.getValue();
                if (huobiCoinDeliveryMarket.subSize() < huobiConfig.getSubSizeLimit()) {
                    huobiCoinDeliveryMarket.subMarket(symbol);
                    symbolMarketMap.put(symbol, entry.getKey());
                    success = true;
                }
            }
            if (!success) {
                int id = ++index;
                HuobiCoinDeliveryMarket huobiCoinDeliveryMarket = SpringContextUtils.getBean(HuobiCoinDeliveryMarket.class, id);
                huobiCoinDeliveryMarket.start();
                huobiCoinDeliveryMarket.subMarket(symbol);
                huobiCoinDeliveryMarketMap.put(id, huobiCoinDeliveryMarket);
                symbolMarketMap.put(symbol, id);
                logger.info("start market " + Integer.toString(id));
            }
        });

    }

    public void subMarket(Set<String> symbols) {
        for (String symbol: symbols) {
            this.subMarket(symbol);
        }
    }

    public void unsubMarket(String symbol) {
        eventloop.execute(()->{
            if (symbolMarketMap.containsKey(symbol)) {
                Integer id = symbolMarketMap.get(symbol);
                HuobiCoinDeliveryMarket huobiCoinDeliveryMarket = huobiCoinDeliveryMarketMap.get(id);
                if (null != huobiCoinDeliveryMarket) huobiCoinDeliveryMarket.unsubMarket(symbol);
                symbolMarketMap.remove(symbol);
                if (huobiCoinDeliveryMarket.subSize() <= 0) {
                    huobiCoinDeliveryMarket.shutdown();
                    huobiCoinDeliveryMarketMap.remove(id);
                }
            }
        });
    }

    public void unsubMarket(Set<String> symbols) {

    }
}
