package com.wxbc.mpu.service.impl;

import com.wxbc.mpu.market.MarketContext;
import com.wxbc.mpu.service.MarketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MarketServiceImpl implements MarketService {

    @Autowired
    MarketContext marketContext;

    public boolean subMarket(String exchange, String symbol) {
        return marketContext.subMarket(exchange, symbol);
    }

    public boolean unsubMarket(String exchange, String symbol) {
        return marketContext.unsubMarket(exchange, symbol);
    }
}
