package com.wxbc.mpu.service;

public interface MarketService {

    boolean subMarket(String exchange, String symbol);

    boolean unsubMarket(String exchange, String symbol);
}
