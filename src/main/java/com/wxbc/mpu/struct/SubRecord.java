package com.wxbc.mpu.struct;

import java.time.LocalDateTime;

public class SubRecord {
    public SubRecord(String symbol, LocalDateTime time) {
        this.symbol = symbol;
        this.time = time;
    }

    private String symbol;

    private LocalDateTime time;

    private Integer state = 3;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}

