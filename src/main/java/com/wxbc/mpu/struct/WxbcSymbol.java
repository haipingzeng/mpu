package com.wxbc.mpu.struct;

import java.math.BigDecimal;

public class WxbcSymbol {

    private String symbol;

    private String exchSymbol;

    private String exchange;

    private char symbolType;

    private char keySymbolType;

    private BigDecimal pxIncrement;

    private Integer pxScale;

    private BigDecimal qtyIncrement;

    private Integer qtyScale;

    private BigDecimal faceValue;

    private String settleCurrency;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getExchSymbol() {
        return exchSymbol;
    }

    public void setExchSymbol(String exchSymbol) {
        this.exchSymbol = exchSymbol;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public char getSymbolType() {
        return symbolType;
    }

    public void setSymbolType(char symbolType) {
        this.symbolType = symbolType;
    }

    public char getKeySymbolType() {
        return keySymbolType;
    }

    public void setKeySymbolType(char keySymbolType) {
        this.keySymbolType = keySymbolType;
    }

    public BigDecimal getPxIncrement() {
        return pxIncrement;
    }

    public void setPxIncrement(BigDecimal pxIncrement) {
        this.pxIncrement = pxIncrement;
    }

    public Integer getPxScale() {
        return pxScale;
    }

    public void setPxScale(Integer pxScale) {
        this.pxScale = pxScale;
    }

    public BigDecimal getQtyIncrement() {
        return qtyIncrement;
    }

    public void setQtyIncrement(BigDecimal qtyIncrement) {
        this.qtyIncrement = qtyIncrement;
    }

    public Integer getQtyScale() {
        return qtyScale;
    }

    public void setQtyScale(Integer qtyScale) {
        this.qtyScale = qtyScale;
    }

    public BigDecimal getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(BigDecimal faceValue) {
        this.faceValue = faceValue;
    }

    public String getSettleCurrency() {
        return settleCurrency;
    }

    public void setSettleCurrency(String settleCurrency) {
        this.settleCurrency = settleCurrency;
    }
}
