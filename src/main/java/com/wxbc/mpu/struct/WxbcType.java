package com.wxbc.mpu.struct;

public class WxbcType {

    // ========================== EXCHANGE =============================
    public static final String EXCHANGE_BINANCE = "BINANCE";

    public static final String EXCHANGE_FTX = "FTX";

    public static final String EXCHANGE_BYBIT = "BYBIT";

    public static final String EXCHANGE_OKX = "OKX";

    public static final String EXCHANGE_BITMEX = "BITMEX";

    public static final String EXCHANGE_DERIBIT = "DERIBIT";

    public static final String EXCHANGE_HUOBI = "HUOBI";

    // ============================ API TYPE =============================
    public static final String API_SPOT = "SPOT";                            // 现货

    public static final String API_FUTURE = "FUTURE";                        // 期货

    public static final String API_USDT_FUTURE = "USDT_FUTURE";              // U本位期货

    public static final String API_COIN_FUTURE = "COIN_FUTURE";              // 币本位期货

    public static final String API_DELIVERY = "DELIVERY";                    // 交割期货

    public static final String API_USDT_DELIVERY = "USDT_DELIVERY";          // U本位交割期货

    public static final String API_COIN_DELIVERY = "COIN_DELIVERY";          // 币本文交割期货

    public static final String API_PERPETUAL = "PERPETUAL";                  // 永续期货

    public static final String API_USDT_PERPETUAL = "USDT_PERPETUAL";        // U本位永续期货

    public static final String API_COIN_PERPETUAL = "COIN_PERPETUAL";        // 币本位永续期货

    public static final String API_UNIFY = "UNIFY";                          // 统一

    public static final String API_OPTION = "OPTION";                        // 期权



    // ============================ SYMBOL TYPE =============================
    public static final Character SYMBOL_SPOT = 'a';                         // 现货

    public static final Character SYMBOL_FUTURE = 'b';                       // 期货

    public static final Character SYMBOL_USDT_FUTURE = 'c';                  // U本位期货

    public static final Character SYMBOL_COIN_FUTURE = 'd';                  // 币本位期货

    public static final Character SYMBOL_DELIVERY = 'e';                      // 交割期货

    public static final Character SYMBOL_USDT_DELIVERY = 'f';                 // U本位交割期货

    public static final Character SYMBOL_COIN_DELIVERY = 'g';                 // 币本文交割期货

    public static final Character SYMBOL_PERPETUAL = 'h';                     // 永续期货

    public static final Character SYMBOL_USDT_PERPETUAL = 'i';                // U本位永续期货

    public static final Character SYMBOL_COIN_PERPETUAL = 'j';                // 币本位永续期货
    public static final Character SYMBOL_UNIFY = 'k';                         // 统一

    public static final Character SYMBOL_OPTION = 'l';                        // 期权
}
