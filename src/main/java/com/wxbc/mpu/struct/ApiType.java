package com.wxbc.mpu.struct;

/**
 *  每个交易都按照自己的规则定义API，没有一个统一的标准，这里定义兼容各个交易所的API类型
 */
public enum ApiType {
    SPOT,                  // 现货
    FUTURE,                // 期货
    USDT_FUTURE,           // U本位期货
    COIN_FUTURE,           // 币本位期货
    DELIVERY,              // 交割期货
    USDT_DELIVERY,         // U本位交割期货
    COIN_DELIVERY,         // 币本文交割期货
    PERPETUAL,             // 永续期货
    USDT_PERPETUAL,        // U本位永续期货
    COIN_PERPETUAL,        // 币本位永续期货
    UNIFY,                 //
}
