package com.wxbc.mpu.market;

import com.alibaba.fastjson.JSONObject;
import com.wxbc.mpu.config.*;
import com.wxbc.mpu.gateway.binance.BinanceCoinFutureMarketMgr;
import com.wxbc.mpu.gateway.binance.BinanceSpotMarket;
import com.wxbc.mpu.gateway.binance.BinanceSpotMarketMgr;
import com.wxbc.mpu.gateway.binance.BinanceUsdtFutureMarketMgr;
import com.wxbc.mpu.gateway.bitmex.BitmexFutureMarketMgr;
import com.wxbc.mpu.gateway.bybit.BybitCoinDeliveryMarketMgr;
import com.wxbc.mpu.gateway.bybit.BybitCoinPerpetualMarketMgr;
import com.wxbc.mpu.gateway.bybit.BybitSpotMarketMgr;
import com.wxbc.mpu.gateway.bybit.BybitUsdtPerpetualMarketMgr;
import com.wxbc.mpu.gateway.deribit.DeribitUnifyMarketMgr;
import com.wxbc.mpu.gateway.ftx.FtxUnifyMarketMgr;
import com.wxbc.mpu.gateway.huobi.*;
import com.wxbc.mpu.gateway.okx.OkxUnifyMarketMgr;
import com.wxbc.mpu.struct.TypeHelper;
import com.wxbc.mpu.struct.WxbcSymbol;
import com.wxbc.mpu.struct.WxbcType;
import com.wxbc.mpu.utils.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;

@Component
public class MarketContext {

    private final static Logger logger = LoggerFactory.getLogger(MarketContext.class);

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    SymbolMgr symbolMgr;

    @Autowired
    BitmexConfig bitmexConfig;

    @Autowired
    BitmexFutureMarketMgr bitmexFutureMarketMgr;

    @Autowired
    HuobiConfig huobiConfig;

    @Autowired
    HuobiCoinDeliveryMarketMgr huobiDeliveryMarketMgr;

    @Autowired
    HuobiSpotMarketMgr huobiSpotMarketMgr;

    @Autowired
    HuobiUsdtPerpetualMarketMgr huobiUsdtPerpetualMarketMgr;

    @Autowired
    HuobiCoinPerpetualMarketMgr huobiCoinPerpetualMarketMgr;

    @Autowired
    BybitConfig bybitConfig;

    @Autowired
    BybitCoinPerpetualMarketMgr bybitCoinPerpetualMarketMgr;

    @Autowired
    BybitUsdtPerpetualMarketMgr bybitUsdtPerpetualMarketMgr;

    @Autowired
    BybitCoinDeliveryMarketMgr bybitCoinDeliveryMarketMgr;

    @Autowired
    BybitSpotMarketMgr bybitSpotMarketMgr;

    @Autowired
    DeribitConfig deribitConfig;

    @Autowired
    DeribitUnifyMarketMgr deribitUnifyMarketMgr;

    @Autowired
    BinanceConfig binanceConfig;

    @Autowired
    BinanceUsdtFutureMarketMgr binanceUsdtFutureMarketMgr;

    @Autowired
    BinanceCoinFutureMarketMgr binanceCoinFutureMarketMgr;

    @Autowired
    BinanceSpotMarketMgr binanceSpotMarketMgr;


    @Autowired
    OkxConfig okxConfig;

    @Autowired
    OkxUnifyMarketMgr okxUnifyMarketMgr;


    @Autowired
    FtxConfig ftxConfig;

    @Autowired
    FtxUnifyMarketMgr ftxUnifyMarketMgr;

    public void start() {

        Set<String> okxUnifySymbols = new HashSet<>();
        for (String symbol : okxConfig.getSubSymbols()) {
            String  key = new StringBuilder(WxbcType.EXCHANGE_OKX).append("|").append(symbol).toString();
            WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
            if (null != wxbcSymbol) {
                if (WxbcType.SYMBOL_UNIFY == wxbcSymbol.getKeySymbolType()) {
                    okxUnifySymbols.add(symbol);
                }
            }
        }
        okxUnifyMarketMgr.subMarket(okxUnifySymbols);



        Set<String> ftxUnifySymbols = new HashSet<>();
        for (String symbol : ftxConfig.getSubSymbols()) {
            String  key = new StringBuilder(WxbcType.EXCHANGE_FTX).append("|").append(symbol).toString();
            WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
            if (null != wxbcSymbol) {
                if (WxbcType.SYMBOL_UNIFY == wxbcSymbol.getKeySymbolType()) {
                    ftxUnifySymbols.add(symbol);
                }
            }
        }
        ftxUnifyMarketMgr.subMarket(ftxUnifySymbols);

        Set<String> binanceUsdtFutureSymbols = new HashSet<>();
        for (String symbol : binanceConfig.getSubSymbols()) {
            String  key = new StringBuilder(WxbcType.EXCHANGE_BINANCE).append("|").append(symbol).toString();
            WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
            if (null != wxbcSymbol) {
                if (WxbcType.SYMBOL_USDT_FUTURE == wxbcSymbol.getKeySymbolType()) {
                    binanceUsdtFutureSymbols.add(symbol);
                }
            }
        }
        binanceUsdtFutureMarketMgr.subMarket(binanceUsdtFutureSymbols);

        Set<String> binanceCoinFutureSymbols = new HashSet<>();
        for (String symbol : binanceConfig.getSubSymbols()) {
            String  key = new StringBuilder(WxbcType.EXCHANGE_BINANCE).append("|").append(symbol).toString();
            WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
            if (null != wxbcSymbol) {
                if (WxbcType.SYMBOL_COIN_FUTURE == wxbcSymbol.getKeySymbolType()) {
                    binanceCoinFutureSymbols.add(symbol);
                }
            }
        }
        binanceCoinFutureMarketMgr.subMarket(binanceCoinFutureSymbols);


        Set<String> binanceSpotSymbols = new HashSet<>();
        for (String symbol : binanceConfig.getSubSymbols()) {
            String  key = new StringBuilder(WxbcType.EXCHANGE_BINANCE).append("|").append(symbol).toString();
            WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
            if (null != wxbcSymbol) {
                if (WxbcType.SYMBOL_SPOT == wxbcSymbol.getKeySymbolType()) {
                    binanceSpotSymbols.add(symbol);
                }
            }
        }
        binanceSpotMarketMgr.subMarket(binanceSpotSymbols);


        Set<String> deribitUnifySymbols = new HashSet<>();
        for (String symbol : deribitConfig.getSubSymbols()) {
            String  key = new StringBuilder(WxbcType.EXCHANGE_DERIBIT).append("|").append(symbol).toString();
            WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
            if (null != wxbcSymbol) {
                if (WxbcType.SYMBOL_UNIFY == wxbcSymbol.getKeySymbolType()) {
                    deribitUnifySymbols.add(symbol);
                }
            }
        }
        deribitUnifyMarketMgr.subMarket(deribitUnifySymbols);

        Set<String> bitmexFutureSubSymbols = new HashSet<>();
        for (String symbol : bitmexConfig.getSubSymbols()) {
            if (symbol.contains("@")) {
                bitmexFutureSubSymbols.add(symbol);
            }
        }
        bitmexFutureMarketMgr.subMarket(bitmexFutureSubSymbols);

        Set<String> huobiDeliverySubSymbols = new HashSet<>();
        for (String symbol : huobiConfig.getSubSymbols()) {
            if (symbol.contains("USD@") && !symbol.contains("@P")) {
                huobiDeliverySubSymbols.add(symbol);
            }
        }
        huobiDeliveryMarketMgr.subMarket(huobiDeliverySubSymbols);

        Set<String> huobiSpotSubSymbols = new HashSet<>();
        for (String symbol : huobiConfig.getSubSymbols()) {
            if (!symbol.contains("@")) {
                huobiSpotSubSymbols.add(symbol);
            }
        }
        huobiSpotMarketMgr.subMarket(huobiSpotSubSymbols);

        Set<String> huobiUsdtPerpetualSubSymbols = new HashSet<>();
        for (String symbol : huobiConfig.getSubSymbols()) {
            if (symbol.contains("USDT@P")) {
                huobiUsdtPerpetualSubSymbols.add(symbol);
            }
        }
        huobiUsdtPerpetualMarketMgr.subMarket(huobiUsdtPerpetualSubSymbols);

        Set<String> huobiCoinPerpetualSubSymbols = new HashSet<>();
        for (String symbol : huobiConfig.getSubSymbols()) {
            if (symbol.contains("USD@P")) {
                huobiCoinPerpetualSubSymbols.add(symbol);
            }
        }
        huobiCoinPerpetualMarketMgr.subMarket(huobiCoinPerpetualSubSymbols);

        Set<String> bybitCoinPerpetualSubSymbols = new HashSet<>();
        for (String symbol : bybitConfig.getSubSymbols()) {
            if (symbol.contains("USD@P")) {
                bybitCoinPerpetualSubSymbols.add(symbol);
            }
        }
        bybitCoinPerpetualMarketMgr.subMarket(bybitCoinPerpetualSubSymbols);

        Set<String> bybitUsdtPerpetualSubSymbols = new HashSet<>();
        for (String symbol : bybitConfig.getSubSymbols()) {
            if (symbol.contains("USDT@P")) {
                bybitUsdtPerpetualSubSymbols.add(symbol);
            }
        }
        bybitUsdtPerpetualMarketMgr.subMarket(bybitUsdtPerpetualSubSymbols);

        Set<String> bybitCoinDeliverySubSymbols = new HashSet<>();
        for (String symbol : bybitConfig.getSubSymbols()) {
            if (symbol.contains("USD@") && !symbol.contains("@P")) {
                bybitCoinDeliverySubSymbols.add(symbol);
            }
        }
        bybitCoinDeliveryMarketMgr.subMarket(bybitCoinDeliverySubSymbols);

        Set<String> bybitSpotSubSymbols = new HashSet<>();
        for (String symbol : bybitConfig.getSubSymbols()) {
            if (!symbol.contains("@")) {
                bybitSpotSubSymbols.add(symbol);
            }
        }
        bybitSpotMarketMgr.subMarket(bybitSpotSubSymbols);
    }

    public boolean subMarket(String exchange, String symbol) {
        if (null == symbol) return false;
        if (WxbcType.EXCHANGE_BITMEX.equals(exchange)) {
            if (symbol.contains("@")) {
                bitmexFutureMarketMgr.subMarket(symbol);
                return true;
            }
        } else if (WxbcType.EXCHANGE_HUOBI.equals(exchange)) {
            if (!symbol.contains("@")) {
                huobiSpotMarketMgr.subMarket(symbol);
                return true;
            } else if (symbol.contains("USD@") && !symbol.contains("@P")) {
                huobiDeliveryMarketMgr.subMarket(symbol);
                return true;
            } else if (symbol.contains("USDT@P")) {
                huobiUsdtPerpetualMarketMgr.subMarket(symbol);
                return true;
            } else if (symbol.contains("USD@P")) {
                huobiCoinPerpetualMarketMgr.subMarket(symbol);
                return true;
            }
        } else if (WxbcType.EXCHANGE_BYBIT.equals(exchange)) {
            if (symbol.contains("USD@P")) {
                bybitCoinPerpetualMarketMgr.subMarket(symbol);
                return true;
            } else if (symbol.contains("USDT@P")) {
                bybitUsdtPerpetualMarketMgr.subMarket(symbol);
                return true;
            } else if (symbol.contains("USD@") && !symbol.contains("@P")) {
                bybitCoinDeliveryMarketMgr.subMarket(symbol);
                return true;
            } else if (!symbol.contains("@")) {
                bybitSpotMarketMgr.subMarket(symbol);
                return true;
            }
        } else if (WxbcType.EXCHANGE_DERIBIT.equals(exchange)) {
            String  key = new StringBuilder(WxbcType.EXCHANGE_DERIBIT).append("|").append(symbol).toString();
            WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
            if (null != wxbcSymbol) {
                if (WxbcType.SYMBOL_UNIFY == wxbcSymbol.getKeySymbolType()) {
                    deribitUnifyMarketMgr.subMarket(symbol);
                    return true;
                }
            }
        } else if (WxbcType.EXCHANGE_BINANCE.equals(exchange)) {
            String  key = new StringBuilder(WxbcType.EXCHANGE_BINANCE).append("|").append(symbol).toString();
            WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
            if (null != wxbcSymbol) {
                if (WxbcType.SYMBOL_SPOT == wxbcSymbol.getKeySymbolType()) {
                    binanceSpotMarketMgr.subMarket(symbol);
                } else if (WxbcType.SYMBOL_USDT_FUTURE == wxbcSymbol.getKeySymbolType()) {
                    binanceUsdtFutureMarketMgr.subMarket(symbol);
                } else if (WxbcType.SYMBOL_COIN_FUTURE == wxbcSymbol.getKeySymbolType()) {
                    binanceCoinFutureMarketMgr.subMarket(symbol);
                }
            }
        } else if (WxbcType.EXCHANGE_OKX.equals(exchange)) {
            String  key = new StringBuilder(WxbcType.EXCHANGE_OKX).append("|").append(symbol).toString();
            WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
            if (null != wxbcSymbol) {
                if (WxbcType.SYMBOL_UNIFY == wxbcSymbol.getKeySymbolType()) {
                    okxUnifyMarketMgr.subMarket(symbol);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean unsubMarket(String exchange, String symbol) {
        if (null == symbol) return false;
        if (WxbcType.EXCHANGE_BITMEX.equals(exchange)) {
            if (symbol.contains("@")) {
                bitmexFutureMarketMgr.unsubMarket(symbol);
                return true;
            }
        } else if (WxbcType.EXCHANGE_HUOBI.equals(exchange)) {
            if (!symbol.contains("@")) {
                huobiSpotMarketMgr.unsubMarket(symbol);
                return true;
            } else if (symbol.contains("USD@") && !symbol.contains("@P")) {
                huobiDeliveryMarketMgr.unsubMarket(symbol);
                return true;
            } else if (symbol.contains("USDT@P")) {
                huobiUsdtPerpetualMarketMgr.unsubMarket(symbol);
                return true;
            } else if (symbol.contains("USD@P")) {
                huobiCoinPerpetualMarketMgr.unsubMarket(symbol);
                return true;
            }
        } else if (WxbcType.EXCHANGE_BYBIT.equals(exchange)) {
            if (symbol.contains("USD@P")) {
                bybitCoinPerpetualMarketMgr.unsubMarket(symbol);
                return true;
            } else if (symbol.contains("USDT@P")) {
                bybitUsdtPerpetualMarketMgr.unsubMarket(symbol);
                return true;
            } else if (symbol.contains("USD@") && !symbol.contains("@P")) {
                bybitCoinDeliveryMarketMgr.unsubMarket(symbol);
                return true;
            } else if (!symbol.contains("@")) {
                bybitSpotMarketMgr.unsubMarket(symbol);
                return true;
            }
        }  else if (WxbcType.EXCHANGE_DERIBIT.equals(exchange)) {
            String  key = new StringBuilder(WxbcType.EXCHANGE_DERIBIT).append("|").append(symbol).toString();
            WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
            if (null != wxbcSymbol) {
                if (WxbcType.SYMBOL_UNIFY == wxbcSymbol.getKeySymbolType()) {
                    deribitUnifyMarketMgr.unsubMarket(symbol);
                    return true;
                }
            }
        } else if (WxbcType.EXCHANGE_BINANCE.equals(exchange)) {
            String  key = new StringBuilder(WxbcType.EXCHANGE_BINANCE).append("|").append(symbol).toString();
            WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
            if (null != wxbcSymbol) {
                if (WxbcType.SYMBOL_SPOT == wxbcSymbol.getKeySymbolType()) {
                    binanceSpotMarketMgr.unsubMarket(symbol);
                } else if (WxbcType.SYMBOL_USDT_FUTURE == wxbcSymbol.getKeySymbolType()) {
                    binanceUsdtFutureMarketMgr.unsubMarket(symbol);
                } else if (WxbcType.SYMBOL_COIN_FUTURE == wxbcSymbol.getKeySymbolType()) {
                    binanceCoinFutureMarketMgr.unsubMarket(symbol);
                }
            }
        } else if (WxbcType.EXCHANGE_OKX.equals(exchange)) {
            String  key = new StringBuilder(WxbcType.EXCHANGE_OKX).append("|").append(symbol).toString();
            WxbcSymbol wxbcSymbol = symbolMgr.selectSymbol(key);
            if (null != wxbcSymbol) {
                if (WxbcType.SYMBOL_UNIFY == wxbcSymbol.getKeySymbolType()) {
                    okxUnifyMarketMgr.unsubMarket(symbol);
                    return true;
                }
            }
        }
        return false;
    }

    public void publishDepth(long seq, String exchange, String symbol, LocalDateTime updateTime, LocalDateTime arrivedTime, TreeMap<String, BigDecimal> bidDepth, TreeMap<String, BigDecimal> askDepth) {

//        System.out.println(exchange + " " + symbol + " " + bidDepth.firstKey() + " " + askDepth.firstKey());
        String topic = null;
        JSONObject jsonObject = null;
        for (int i = 0; i < 3; i++) {
            try {
                jsonObject = new JSONObject();
                jsonObject.put("Msg_seq", seq);
                jsonObject.put("Exchange", exchange);
                jsonObject.put("Symbol", symbol);
                jsonObject.put("Time", DateTimeUtils.fmtDate(updateTime));
                jsonObject.put("TimeArrive", DateTimeUtils.fmtDate(arrivedTime));
                jsonObject.put("TimePublish", DateTimeUtils.fmtDate(DateTimeUtils.utcnow()));
                jsonObject.put("BidDepth", bidDepth);
                jsonObject.put("AskDepth", askDepth);
                topic = new StringBuilder("DEPTHx|").append(symbol).append(".").append(exchange).toString();
                String msg = jsonObject.toJSONString();
                redisTemplate.convertAndSend(topic, msg);
                redisTemplate.opsForValue().set(topic, msg);
                break;
            } catch (Exception e) {
                logger.error(new StringBuilder("PublishDepth failed: ").append(topic).append("  ").append(e.getMessage()).append(" ").append(jsonObject.toJSONString()).toString());
                e.printStackTrace();
            }
        }
    }

    public boolean updateCrossingSnapshot(long seq, String exchange, String symbol, LocalDateTime updateTime, LocalDateTime arrivedTime, TreeMap<String, BigDecimal> bidDepth, TreeMap<String, BigDecimal> askDepth) {
        String key = new StringBuilder().append(symbol).append(".").append(exchange).toString();
        JSONObject jsonObject = new JSONObject();
        for (int i = 0; i < 3; i++) {
            try {
                jsonObject.put("Msg_seq", seq);
                jsonObject.put("Exchange", exchange);
                jsonObject.put("Symbol", symbol);
                jsonObject.put("Time", DateTimeUtils.fmtDate(updateTime));
                jsonObject.put("TimeArrive", DateTimeUtils.fmtDate(arrivedTime));
                jsonObject.put("TimePublish", DateTimeUtils.fmtDate(DateTimeUtils.utcnow()));
                jsonObject.put("BidDepth", bidDepth);
                jsonObject.put("AskDepth", askDepth);
                String msg = jsonObject.toJSONString();
                redisTemplate.opsForHash().put("CROSSING_SNAPSHOT", key, msg);
                return true;
            } catch (Exception e) {
                logger.error(new StringBuilder("UpdateCrossingSnapshot failed: ").append(key).append("  ").append(e.getMessage()).append(" ").append(jsonObject.toJSONString()).toString());
                e.printStackTrace();
            }
        }
        return false;
    }

    public void publishDelta(long seq, String exchange, String symbol, LocalDateTime updateTime, LocalDateTime arrivedTime, TreeMap<String, BigDecimal> bidMap, TreeMap<String, BigDecimal> askMap) {
        String topic = null;
        JSONObject jsonObject = null;
        for (int i = 0; i < 3; i++) {
            try {
                jsonObject = new JSONObject();
                jsonObject.put("Msg_seq", seq);
                jsonObject.put("Exchange", exchange);
                jsonObject.put("Symbol", symbol);
                jsonObject.put("Time", DateTimeUtils.fmtDate(updateTime));
                jsonObject.put("TimeArrive", DateTimeUtils.fmtDate(arrivedTime));
                jsonObject.put("TimePublish", DateTimeUtils.fmtDate(DateTimeUtils.utcnow()));
                jsonObject.put("BidUpdate", bidMap);
                jsonObject.put("AskUpdate", askMap);
                topic = new StringBuilder("UPDATEx|").append(symbol).append(".").append(exchange).toString();
                redisTemplate.convertAndSend(topic, jsonObject.toJSONString());
                break;
            } catch (Exception e) {
                logger.error(new StringBuilder("PublishDelta failed: ").append(topic).append("  ").append(e.getMessage()).append(" ").append(jsonObject.toJSONString()).toString());
                e.printStackTrace();
            }
        }
    }


    public void publishTrade(String exchange, String symbol, LocalDateTime updateTime, LocalDateTime arrivedTime, String direction, BigDecimal px, BigDecimal qty) {
//        System.out.println("publish trade: " + exchange + " " + symbol + " " + direction + " " + px.toString() + " " + qty.toString());
        String topic = null;
        JSONObject jsonObject = null;
        for (int i = 0; i < 3; i++) {
            try {
                jsonObject = new JSONObject();
                jsonObject.put("Exchange", exchange);
                jsonObject.put("Symbol", symbol);
                jsonObject.put("Time", DateTimeUtils.fmtDate(updateTime));
                jsonObject.put("TimeArrive", DateTimeUtils.fmtDate(arrivedTime));
                jsonObject.put("TimePublish", DateTimeUtils.fmtDate(DateTimeUtils.utcnow()));
                jsonObject.put("Direction", direction);
                jsonObject.put("LastPx", px);
                jsonObject.put("Qty", qty);
                topic = new StringBuilder("TRADEx|").append(symbol).append(".").append(exchange).toString();
                redisTemplate.convertAndSend(topic, jsonObject.toJSONString());
                break;
            } catch (Exception e) {
                logger.error(new StringBuilder("PublishTrade failed: ").append(topic).append("  ").append(e.getMessage()).append(" ").append(jsonObject.toJSONString()).toString());
                e.printStackTrace();
            }
        }
    }
}
