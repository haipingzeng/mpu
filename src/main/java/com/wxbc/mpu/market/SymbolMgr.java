package com.wxbc.mpu.market;



import com.alibaba.fastjson.JSONObject;
import com.wxbc.mpu.struct.WxbcSymbol;
import com.wxbc.mpu.struct.WxbcType;
import com.wxbc.mpu.utils.DateTimeUtils;
import com.wxbc.mpu.utils.WxbcUtils;
import io.netty.channel.ChannelOption;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.DefaultUriBuilderFactory;
import reactor.netty.http.client.HttpClient;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Component
public class SymbolMgr {
    private final static Logger logger = LoggerFactory.getLogger(SymbolMgr.class);


    private Map<String, WxbcSymbol> mqtSymbolMap = new HashMap<>();

    private Map<String, WxbcSymbol> exchSymbolMap = new HashMap<>();



    public WxbcSymbol selectSymbol(String mqtKey) {
        return mqtSymbolMap.get(mqtKey);
    }

    public WxbcSymbol selectSymbolByExchKey(String exchKey) {
        return exchSymbolMap.get(exchKey);
    }

    public Map<String, WxbcSymbol> selectSymbols() {
        return mqtSymbolMap;
    }

    WebClient webClient;

    private ScheduledExecutorService eventloop = new ScheduledThreadPoolExecutor(1, new BasicThreadFactory.Builder().namingPattern("SymbolMgr").daemon(true).build());


    SymbolMgr() {
        if (null == webClient) {
            DefaultUriBuilderFactory uriBuilderFactory = new DefaultUriBuilderFactory();
            uriBuilderFactory.setEncodingMode(DefaultUriBuilderFactory.EncodingMode.NONE);
            ReactorClientHttpConnector reactorClientHttpConnector = new ReactorClientHttpConnector(HttpClient.create().option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000).wiretap(true));
            webClient = WebClient.builder().exchangeStrategies(ExchangeStrategies.builder().codecs(configurer ->{ configurer.defaultCodecs().maxInMemorySize(10 * 1024 * 1024); }).build()).uriBuilderFactory(uriBuilderFactory).clientConnector(reactorClientHttpConnector).build();
        }
        loadSymbols();
        eventloop.scheduleWithFixedDelay(()->{
            logger.info("============ START LOAD SYMBOLS ==============");
            loadSymbols();
            logger.info("============ END LOAD SYMBOLS   ==============");
        }, 1, 1, TimeUnit.HOURS);
    }

    public void loadSymbols() {

        loadDeribitSymbols();
        loadFtxSymbols();
        loadBinanceSymbols();
        loadOkxSymbols();
    }

    private void loadFtxSymbols() {
        // spot
        while (true) {
            try {
                JSONObject jmsg = webClient.get().uri("https://ftx.com/api/markets").retrieve().bodyToMono(JSONObject.class).block();
                for (JSONObject info : jmsg.getJSONArray("result").toJavaList(JSONObject.class)) {
                    if (info.getBoolean("enabled")) {
                        String exchSymbol = info.getString("name");
                        if (exchSymbol.contains("/")) {
                            String names[] = exchSymbol.split("/");
                            String symbol = new StringBuilder().append(names[0]).append("_").append(names[1]).toString();
                            WxbcSymbol mqtSymbol = new WxbcSymbol();
                            mqtSymbol.setSymbol(symbol);
                            mqtSymbol.setExchSymbol(exchSymbol);
                            mqtSymbol.setExchange(WxbcType.EXCHANGE_FTX);
                            mqtSymbol.setSymbolType(WxbcType.SYMBOL_SPOT);
                            mqtSymbol.setKeySymbolType(WxbcType.SYMBOL_UNIFY);
                            mqtSymbol.setPxIncrement(info.getBigDecimal("priceIncrement"));
                            mqtSymbol.setPxScale(WxbcUtils.calScale(mqtSymbol.getPxIncrement()));
                            mqtSymbol.setQtyIncrement(info.getBigDecimal("sizeIncrement"));
                            mqtSymbol.setQtyScale(WxbcUtils.calScale(mqtSymbol.getQtyIncrement()));
                            String key = new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getSymbol()).toString();
                            mqtSymbolMap.put(key, mqtSymbol);
                            exchSymbolMap.put(new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getKeySymbolType()).append("|").append(mqtSymbol.getExchSymbol()).toString(), mqtSymbol);
                        }
                    }
                }

                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
            WxbcUtils.sleep(5000);
        }

        // future
        while (true) {
            try {
                JSONObject jmsg = webClient.get().uri("https://ftx.com/api/futures").retrieve().bodyToMono(JSONObject.class).block();
                for (JSONObject info : jmsg.getJSONArray("result").toJavaList(JSONObject.class)) {
                    if (!info.getBoolean("enabled"))  continue;
                    String exchSymbol = info.getString("name");
                    if (!exchSymbol.contains("-")) continue;
                    String names[] = exchSymbol.split("-");
                    if (names.length > 2) continue;
                    String symbol = null;
                    if ("PERP".equals(names[1])) {
                        symbol = new StringBuilder().append(names[0]).append("_USD@P").toString();
                    } else {
                        String expirys[] = info.getString("expiry").split("-");
                        symbol = new StringBuilder().append(names[0]).append("_USD@").append(expirys[0].substring(2, 4)).append(names[1]).toString();
                    }
                    WxbcSymbol mqtSymbol = new WxbcSymbol();
                    mqtSymbol.setSymbol(symbol);
                    mqtSymbol.setExchSymbol(exchSymbol);
                    mqtSymbol.setExchange(WxbcType.EXCHANGE_FTX);
                    mqtSymbol.setSymbolType(WxbcType.SYMBOL_FUTURE);
                    mqtSymbol.setKeySymbolType(WxbcType.SYMBOL_UNIFY);
                    mqtSymbol.setFaceValue(BigDecimal.ONE);
                    mqtSymbol.setPxIncrement(info.getBigDecimal("priceIncrement"));
                    mqtSymbol.setPxScale(WxbcUtils.calScale(mqtSymbol.getPxIncrement()));
                    mqtSymbol.setQtyIncrement(info.getBigDecimal("sizeIncrement"));
                    mqtSymbol.setQtyScale(WxbcUtils.calScale(mqtSymbol.getQtyIncrement()));
                    String key = new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getSymbol()).toString();
                    mqtSymbolMap.put(key, mqtSymbol);
                    exchSymbolMap.put(new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getKeySymbolType()).append("|").append(mqtSymbol.getExchSymbol()).toString(), mqtSymbol);
                }
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
            WxbcUtils.sleep(5000);
        }
    }

    private void loadBinanceSymbols() {

        // spot
        while (true) {
            try {
                JSONObject jmsg = webClient.get().uri("https://api.binance.com/api/v3/exchangeInfo").retrieve().bodyToMono(JSONObject.class).block();
                for (JSONObject info : jmsg.getJSONArray("symbols").toJavaList(JSONObject.class)) {
                    if (info.getBoolean("isSpotTradingAllowed")) {
                        String exchSymbol = info.getString("symbol");
                        String symbol = new StringBuilder().append(info.getString("baseAsset")).append("_").append(info.getString("quoteAsset")).toString();

                        WxbcSymbol mqtSymbol = new WxbcSymbol();
                        mqtSymbol.setSymbol(symbol);
                        mqtSymbol.setExchSymbol(exchSymbol);
                        mqtSymbol.setExchange(WxbcType.EXCHANGE_BINANCE);
                        mqtSymbol.setSymbolType(WxbcType.SYMBOL_SPOT);
                        mqtSymbol.setKeySymbolType(WxbcType.SYMBOL_SPOT);
                        for (JSONObject filter : info.getJSONArray("filters").toJavaList(JSONObject.class)) {
                            if ("PRICE_FILTER".equals(filter.getString("filterType"))) {
                                mqtSymbol.setPxIncrement(filter.getBigDecimal("tickSize"));
                                mqtSymbol.setPxScale(WxbcUtils.calScale(mqtSymbol.getPxIncrement()));
                            }
                            if ("LOT_SIZE".equals(filter.getString("filterType"))) {
                                mqtSymbol.setQtyIncrement(filter.getBigDecimal("stepSize"));
                                mqtSymbol.setQtyScale(WxbcUtils.calScale(mqtSymbol.getQtyIncrement()));
                            }
                        }
                        String key = new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getSymbol()).toString();
                        mqtSymbolMap.put(key, mqtSymbol);
                        exchSymbolMap.put(new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getKeySymbolType()).append("|").append(mqtSymbol.getExchSymbol()).toString(), mqtSymbol);
                    }
                }
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
            WxbcUtils.sleep(5000);
        }

        // usdt_future
        while (true) {
            try {
                JSONObject jmsg = webClient.get().uri("https://fapi.binance.com/fapi/v1/exchangeInfo").retrieve().bodyToMono(JSONObject.class).block();
                for (JSONObject info : jmsg.getJSONArray("symbols").toJavaList(JSONObject.class)) {
                    if ("USDT".equals(info.getString("quoteAsset")) && "TRADING".equals(info.getString("status"))) {
                        String exchSymbol = info.getString("symbol");
                        String symbol = null;
                        if ("PERPETUAL".equals(info.getString("contractType"))) {
                            symbol = new StringBuilder().append(info.getString("baseAsset")).append("_").append(info.getString("quoteAsset")).append("@P").toString();
                        } else {
                            LocalDateTime deliveryDate = DateTimeUtils.millisToDateTime(info.getLong("deliveryDate"));
                            symbol = new StringBuilder().append(info.getString("baseAsset")).append("_").append(info.getString("quoteAsset")).append("@").append(DateTimeUtils.fmtDate(deliveryDate, "yyMMdd")).toString();
                        }
                        WxbcSymbol mqtSymbol = new WxbcSymbol();
                        mqtSymbol.setSymbol(symbol);
                        mqtSymbol.setExchSymbol(exchSymbol);
                        mqtSymbol.setExchange(WxbcType.EXCHANGE_BINANCE);
                        mqtSymbol.setSymbolType(WxbcType.SYMBOL_USDT_FUTURE);
                        mqtSymbol.setKeySymbolType(WxbcType.SYMBOL_USDT_FUTURE);
                        mqtSymbol.setFaceValue(BigDecimal.ONE);
                        for (JSONObject filter : info.getJSONArray("filters").toJavaList(JSONObject.class)) {
                            if ("PRICE_FILTER".equals(filter.getString("filterType"))) {
                                mqtSymbol.setPxIncrement(filter.getBigDecimal("tickSize"));
                                mqtSymbol.setPxScale(WxbcUtils.calScale(mqtSymbol.getPxIncrement()));
                            }
                            if ("LOT_SIZE".equals(filter.getString("filterType"))) {
                                mqtSymbol.setQtyIncrement(filter.getBigDecimal("stepSize"));
                                mqtSymbol.setQtyScale(WxbcUtils.calScale(mqtSymbol.getQtyIncrement()));
                            }
                        }
                        String key = new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getSymbol()).toString();
                        mqtSymbolMap.put(key, mqtSymbol);
                        exchSymbolMap.put(new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getKeySymbolType()).append("|").append(mqtSymbol.getExchSymbol()).toString(), mqtSymbol);
                    }
                }
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
            WxbcUtils.sleep(5000);
        }

        // coin_future
        while (true) {
            try {
                JSONObject jmsg = webClient.get().uri("https://dapi.binance.com/dapi/v1/exchangeInfo").retrieve().bodyToMono(JSONObject.class).block();
                for (JSONObject info : jmsg.getJSONArray("symbols").toJavaList(JSONObject.class)) {
                    if ("USD".equals(info.getString("quoteAsset")) && "TRADING".equals(info.getString("contractStatus"))) {
                        String exchSymbol = info.getString("symbol");
                        String symbol = null;
                        if ("PERPETUAL".equals(info.getString("contractType"))) {
                            symbol = new StringBuilder().append(info.getString("baseAsset")).append("_").append(info.getString("quoteAsset")).append("@P").toString();
                        } else {
                            LocalDateTime deliveryDate = DateTimeUtils.millisToDateTime(info.getLong("deliveryDate"));
                            symbol = new StringBuilder().append(info.getString("baseAsset")).append("_").append(info.getString("quoteAsset")).append("@").append(DateTimeUtils.fmtDate(deliveryDate, "yyMMdd")).toString();
                        }
                        WxbcSymbol mqtSymbol = new WxbcSymbol();
                        mqtSymbol.setSymbol(symbol);
                        mqtSymbol.setExchSymbol(exchSymbol);
                        mqtSymbol.setExchange(WxbcType.EXCHANGE_BINANCE);
                        mqtSymbol.setSymbolType(WxbcType.SYMBOL_COIN_FUTURE);
                        mqtSymbol.setKeySymbolType(WxbcType.SYMBOL_COIN_FUTURE);
                        mqtSymbol.setFaceValue(info.getBigDecimal("contractSize"));
                        for (JSONObject filter : info.getJSONArray("filters").toJavaList(JSONObject.class)) {
                            if ("PRICE_FILTER".equals(filter.getString("filterType"))) {
                                mqtSymbol.setPxIncrement(filter.getBigDecimal("tickSize"));
                                mqtSymbol.setPxScale(WxbcUtils.calScale(mqtSymbol.getPxIncrement()));
                            }
                            if ("LOT_SIZE".equals(filter.getString("filterType"))) {
                                mqtSymbol.setQtyIncrement(filter.getBigDecimal("stepSize"));
                                mqtSymbol.setQtyScale(WxbcUtils.calScale(mqtSymbol.getQtyIncrement()));
                            }
                        }
                        String key = new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getSymbol()).toString();
                        mqtSymbolMap.put(key, mqtSymbol);
                        exchSymbolMap.put(new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getKeySymbolType()).append("|").append(mqtSymbol.getExchSymbol()).toString(), mqtSymbol);
                    }
                }
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
            WxbcUtils.sleep(5000);
        }
    }


    private void loadOkxSymbols() {
        // spot
        while (true) {
            try {
                JSONObject jmsg = webClient.get().uri("https://www.okx.com/api/v5/public/instruments?instType=SPOT").retrieve().bodyToMono(JSONObject.class).block();
                if ("0".equals(jmsg.getString("code"))) {
                    for (JSONObject info : jmsg.getJSONArray("data").toJavaList(JSONObject.class)) {

                        String exchSymbol = info.getString("instId");
                        String[] fields = exchSymbol.split("-");
                        String symbol = new StringBuilder().append(fields[0]).append("_").append(fields[1]).toString();

                        WxbcSymbol mqtSymbol = new WxbcSymbol();
                        mqtSymbol.setSymbol(symbol);
                        mqtSymbol.setExchSymbol(exchSymbol);
                        mqtSymbol.setExchange(WxbcType.EXCHANGE_OKX);
                        mqtSymbol.setSymbolType(WxbcType.SYMBOL_SPOT);
                        mqtSymbol.setKeySymbolType(WxbcType.SYMBOL_UNIFY);
                        mqtSymbol.setPxIncrement(info.getBigDecimal("tickSz"));
                        mqtSymbol.setPxScale(WxbcUtils.calScale(mqtSymbol.getPxIncrement()));
                        mqtSymbol.setQtyIncrement(info.getBigDecimal("lotSz"));
                        mqtSymbol.setQtyScale(WxbcUtils.calScale(mqtSymbol.getQtyIncrement()));
                        String key = new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getSymbol()).toString();
                        mqtSymbolMap.put(key, mqtSymbol);
                        exchSymbolMap.put(new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getKeySymbolType()).append("|").append(mqtSymbol.getExchSymbol()).toString(), mqtSymbol);
                    }
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            WxbcUtils.sleep(5000);
        }

        // swap
        while (true) {
            try {
                JSONObject jmsg = webClient.get().uri("https://www.okx.com/api/v5/public/instruments?instType=SWAP").retrieve().bodyToMono(JSONObject.class).block();
                if ("0".equals(jmsg.getString("code"))) {
                    for (JSONObject info : jmsg.getJSONArray("data").toJavaList(JSONObject.class)) {

                        String exchSymbol = info.getString("instId");
                        String[] fields = exchSymbol.split("-");
                        String symbol = new StringBuilder().append(fields[0]).append("_").append(fields[1]).append("@P").toString();

                        WxbcSymbol mqtSymbol = new WxbcSymbol();
                        mqtSymbol.setSymbol(symbol);
                        mqtSymbol.setExchSymbol(exchSymbol);
                        mqtSymbol.setSettleCurrency(info.getString("settleCcy"));
                        mqtSymbol.setExchange(WxbcType.EXCHANGE_OKX);
                        mqtSymbol.setSymbolType(WxbcType.SYMBOL_PERPETUAL);
                        mqtSymbol.setKeySymbolType(WxbcType.SYMBOL_UNIFY);
                        mqtSymbol.setFaceValue(info.getBigDecimal("ctVal"));
                        mqtSymbol.setPxIncrement(info.getBigDecimal("tickSz"));
                        mqtSymbol.setPxScale(WxbcUtils.calScale(mqtSymbol.getPxIncrement()));
                        mqtSymbol.setQtyIncrement(info.getBigDecimal("lotSz"));
                        mqtSymbol.setQtyScale(WxbcUtils.calScale(mqtSymbol.getQtyIncrement()));
                        String key = new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getSymbol()).toString();
                        mqtSymbolMap.put(key, mqtSymbol);
                        exchSymbolMap.put(new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getKeySymbolType()).append("|").append(mqtSymbol.getExchSymbol()).toString(), mqtSymbol);
                    }
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            WxbcUtils.sleep(5000);
        }

        // future
        while (true) {
            try {
                JSONObject jmsg = webClient.get().uri("https://www.okx.com/api/v5/public/instruments?instType=FUTURES").retrieve().bodyToMono(JSONObject.class).block();
                if ("0".equals(jmsg.getString("code"))) {
                    for (JSONObject info : jmsg.getJSONArray("data").toJavaList(JSONObject.class)) {

                        String exchSymbol = info.getString("instId");

                        String[] fields = exchSymbol.split("-");
                        String symbol = new StringBuilder().append(fields[0]).append("_").append(fields[1]).append("@").append(fields[2]).toString();

                        WxbcSymbol mqtSymbol = new WxbcSymbol();
                        mqtSymbol.setSymbol(symbol);
                        mqtSymbol.setExchSymbol(exchSymbol);
                        mqtSymbol.setSettleCurrency(info.getString("settleCcy"));
                        mqtSymbol.setExchange(WxbcType.EXCHANGE_OKX);
                        mqtSymbol.setSymbolType(WxbcType.SYMBOL_DELIVERY);
                        mqtSymbol.setKeySymbolType(WxbcType.SYMBOL_UNIFY);
                        mqtSymbol.setFaceValue(info.getBigDecimal("ctVal"));
                        mqtSymbol.setPxIncrement(info.getBigDecimal("tickSz"));
                        mqtSymbol.setPxScale(WxbcUtils.calScale(mqtSymbol.getPxIncrement()));
                        mqtSymbol.setQtyIncrement(info.getBigDecimal("lotSz"));
                        mqtSymbol.setQtyScale(WxbcUtils.calScale(mqtSymbol.getQtyIncrement()));
                        String key = new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getSymbol()).toString();
                        mqtSymbolMap.put(key, mqtSymbol);
                        exchSymbolMap.put(new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getKeySymbolType()).append("|").append(mqtSymbol.getExchSymbol()).toString(), mqtSymbol);
                    }
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            WxbcUtils.sleep(5000);
        }
    }


    private void loadDeribitSymbols() {
        // BTC
        while (true) {
            try {
                JSONObject jmsg = webClient.get().uri("https://www.deribit.com/api/v2/public/get_instruments?currency=BTC").retrieve().bodyToMono(JSONObject.class).block();
                if (jmsg.containsKey("result")) {
                    for (JSONObject info : jmsg.getJSONArray("result").toJavaList(JSONObject.class)) {
                        if (info.getBoolean("is_active")) {
                            WxbcSymbol mqtSymbol = new WxbcSymbol();
                            String exchSymbol = info.getString("instrument_name");
                            String symbol = null;
                            if ("perpetual".equals(info.getString("settlement_period"))) {
                                symbol = new StringBuilder().append(info.getString("base_currency")).append("_").append(info.getString("quote_currency")).append("@P").toString();
                                mqtSymbol.setSymbolType(WxbcType.SYMBOL_PERPETUAL);
                                mqtSymbol.setKeySymbolType(WxbcType.SYMBOL_UNIFY);
                                mqtSymbol.setSymbol(symbol);
                            } else if ("future".equals(info.getString("kind"))) {
                                LocalDateTime dt = DateTimeUtils.millisToUtcDateTime(info.getLong("expiration_timestamp"));
                                symbol = new StringBuilder().append(info.getString("base_currency")).append("_").append(info.getString("quote_currency")).append("@").append(DateTimeUtils.fmtDate(dt, "yyMMdd")).toString();
                                mqtSymbol.setSymbolType(WxbcType.SYMBOL_DELIVERY);
                                mqtSymbol.setKeySymbolType(WxbcType.SYMBOL_UNIFY);
                                mqtSymbol.setSymbol(symbol);
                            } else if ("option".equals(info.getString("kind"))) {
                                LocalDateTime dt = DateTimeUtils.millisToUtcDateTime(info.getLong("expiration_timestamp"));
                                String[] fields = exchSymbol.split("-");
                                symbol = new StringBuilder().append(info.getString("base_currency")).append("_USD").append("@").append(DateTimeUtils.fmtDate(dt, "yyMMdd")).append("_").append(fields[2]).append(fields[3]).toString();
                                mqtSymbol.setSymbolType(WxbcType.SYMBOL_OPTION);
                                mqtSymbol.setKeySymbolType(WxbcType.SYMBOL_UNIFY);
                                mqtSymbol.setSymbol(symbol);
                            }
                            mqtSymbol.setExchSymbol(exchSymbol);
                            mqtSymbol.setExchange(WxbcType.EXCHANGE_DERIBIT);

                            mqtSymbol.setPxIncrement(info.getBigDecimal("tick_size"));
                            mqtSymbol.setPxScale(WxbcUtils.calScale(mqtSymbol.getPxIncrement()));
                            mqtSymbol.setQtyIncrement(info.getBigDecimal("min_trade_amount"));
                            mqtSymbol.setQtyScale(WxbcUtils.calScale(mqtSymbol.getQtyIncrement()));
                            mqtSymbol.setFaceValue(info.getBigDecimal("contract_size"));
                            String key = new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getSymbol()).toString();
                            mqtSymbolMap.put(key, mqtSymbol);
                            exchSymbolMap.put(new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getKeySymbolType()).append("|").append(mqtSymbol.getExchSymbol()).toString(), mqtSymbol);
                        }
                    }
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            WxbcUtils.sleep(5000);
        }

        // ETH
        while (true) {
            try {
                JSONObject jmsg = webClient.get().uri("https://www.deribit.com/api/v2/public/get_instruments?currency=ETH").retrieve().bodyToMono(JSONObject.class).block();
                if (jmsg.containsKey("result")) {
                    for (JSONObject info : jmsg.getJSONArray("result").toJavaList(JSONObject.class)) {
                        if (info.getBoolean("is_active")) {
                            WxbcSymbol mqtSymbol = new WxbcSymbol();
                            String exchSymbol = info.getString("instrument_name");
                            String symbol = null;
                            if ("perpetual".equals(info.getString("settlement_period"))) {
                                symbol = new StringBuilder().append(info.getString("base_currency")).append("_").append(info.getString("quote_currency")).append("@P").toString();
                                mqtSymbol.setSymbolType(WxbcType.SYMBOL_PERPETUAL);
                                mqtSymbol.setKeySymbolType(WxbcType.SYMBOL_UNIFY);
                                mqtSymbol.setSymbol(symbol);
                            } else if ("future".equals(info.getString("kind"))) {
                                LocalDateTime dt = DateTimeUtils.millisToUtcDateTime(info.getLong("expiration_timestamp"));
                                symbol = new StringBuilder().append(info.getString("base_currency")).append("_").append(info.getString("quote_currency")).append("@").append(DateTimeUtils.fmtDate(dt, "yyMMdd")).toString();
                                mqtSymbol.setSymbolType(WxbcType.SYMBOL_DELIVERY);
                                mqtSymbol.setKeySymbolType(WxbcType.SYMBOL_UNIFY);
                                mqtSymbol.setSymbol(symbol);
                            } else if ("option".equals(info.getString("kind"))) {
                                LocalDateTime dt = DateTimeUtils.millisToUtcDateTime(info.getLong("expiration_timestamp"));
                                String[] fields = exchSymbol.split("-");
                                symbol = new StringBuilder().append(info.getString("base_currency")).append("_USD").append("@").append(DateTimeUtils.fmtDate(dt, "yyMMdd")).append("_").append(fields[2]).append(fields[3]).toString();
                                mqtSymbol.setSymbolType(WxbcType.SYMBOL_OPTION);
                                mqtSymbol.setKeySymbolType(WxbcType.SYMBOL_UNIFY);
                                mqtSymbol.setSymbol(symbol);
                            }
                            mqtSymbol.setExchSymbol(exchSymbol);
                            mqtSymbol.setExchange(WxbcType.EXCHANGE_DERIBIT);

                            mqtSymbol.setPxIncrement(info.getBigDecimal("tick_size"));
                            mqtSymbol.setPxScale(WxbcUtils.calScale(mqtSymbol.getPxIncrement()));
                            mqtSymbol.setQtyIncrement(info.getBigDecimal("min_trade_amount"));
                            mqtSymbol.setQtyScale(WxbcUtils.calScale(mqtSymbol.getQtyIncrement()));
                            mqtSymbol.setFaceValue(info.getBigDecimal("contract_size"));
                            String key = new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getSymbol()).toString();
                            mqtSymbolMap.put(key, mqtSymbol);
                            exchSymbolMap.put(new StringBuilder(mqtSymbol.getExchange()).append("|").append(mqtSymbol.getKeySymbolType()).append("|").append(mqtSymbol.getExchSymbol()).toString(), mqtSymbol);
                        }
                    }
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            WxbcUtils.sleep(5000);
        }
    }
}
