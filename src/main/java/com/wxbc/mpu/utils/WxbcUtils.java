package com.wxbc.mpu.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.atomic.AtomicLong;

public class WxbcUtils {

    private static volatile AtomicLong sequence = new AtomicLong(System.currentTimeMillis() * 10000L);

    public static Long nextSequence() {
        return WxbcUtils.sequence.incrementAndGet();
    }

    public static <T extends Comparable> T max(T ... n) {
        T r = null;
        for (T t : n) {
            if (null == r) r = t;
            else {
                if (t.compareTo(r) ==  1) r = t;
            }
        }
        return r;
    }

    public static <T extends Comparable> T min(T ... n) {
        T r = null;
        for (T t : n) {
            if (null == r) r = t;
            else {
                if (t.compareTo(r) == -1) r = t;
            }
        }
        return r;
    }

    public static <T extends Number> T abs(T t) {
        if (t instanceof Double) {
            Double v = (Double) t;
            if (v < 0) v = -v;
            return (T) v;
        } else if (t instanceof Integer) {
            Integer v = (Integer) t;
            if (v < 0) v = -v;
            return (T) v;
        } else if (t instanceof BigDecimal) {
            BigDecimal v = (BigDecimal) t;
            if (v.compareTo(BigDecimal.ZERO) < 0) v = v.multiply(new BigDecimal(-1));
            return (T) v;
        } else if (t instanceof Long) {
            Long v = (Long) t;
            if (v < 0) v = -v;
            return (T) v;
        }
        return t;
    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int calScale(BigDecimal num) {
        BigDecimal tmpNum = num.add(BigDecimal.ZERO);
        if (tmpNum.compareTo(BigDecimal.ONE) >= 0 && tmpNum.compareTo(BigDecimal.TEN) < 0) {
            return 0;
        }
        int ret = 0;
        while (tmpNum.compareTo(BigDecimal.ONE) < 0) {
            ret += 1;
            tmpNum = tmpNum.multiply(BigDecimal.TEN);
        }
        while (tmpNum.compareTo(BigDecimal.TEN) >= 0) {
            ret -= 1;
            tmpNum = tmpNum.divide(BigDecimal.TEN, 12, RoundingMode.HALF_UP);
        }
        return ret;
    }

}


