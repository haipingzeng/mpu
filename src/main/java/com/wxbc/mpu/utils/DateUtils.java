package com.wxbc.mpu.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
//    private static final Calendar calendar = Calendar.getInstance();
//
//    public static final int ZONE_OFFSET = calendar.get(Calendar.ZONE_OFFSET);
//
//    public static final int DST_OFFSET = calendar.get(Calendar.DST_OFFSET);
//
//    public static Date now() {
//        Date date = new Date(System.currentTimeMillis());
//        return date;
//    }
//
//    public static Date utcnow() {
//        Date date = new Date(System.currentTimeMillis() - ZONE_OFFSET - DST_OFFSET);
//        return date;
//    }
//
//    public static long utcnowMillis() {
//        return System.currentTimeMillis() - ZONE_OFFSET - DST_OFFSET;
//    }
//
//    public static long millis() {
//        return System.currentTimeMillis();
//    }
//
//    // "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
//    public static String fmtDate(Date date, String fmt) {
//        SimpleDateFormat sdf = new SimpleDateFormat(fmt);
//        return sdf.format(date);
//    }
//
//    public static Date parseDate(String date, String fmt) {
//        SimpleDateFormat sdf = new SimpleDateFormat(fmt);
//        try {
//            return sdf.parse(date);
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
//    public static Date millisToDate(Long millis) {
//        Date date = new Date();
//        date.setTime(millis);
//        return date;
//    }
//
//    public static Date test() {
//        Calendar calendar = Calendar.getInstance();
//        calendar.add(Calendar.DAY_OF_MONTH, 14);
//        return null;
//    }
//
//    public static Date lastFridayOfMonth(Date date) {
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(date);
//        int leastMax = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
//
//        while (leastMax > 0) {
//            calendar.set(Calendar.DAY_OF_MONTH, leastMax);
//            if (calendar.get(Calendar.DAY_OF_WEEK) == 6) {
//                calendar.set(Calendar.HOUR_OF_DAY, 8);
//                calendar.set(Calendar.MINUTE, 0);
//                calendar.set(Calendar.SECOND, 0);
//                calendar.set(Calendar.MILLISECOND, 0);
//                return calendar.getTime();
//            }
//            leastMax -= 1;
//        }
//        return null;
//    }
//
//    public static Date shiftFriday(Date date, int shift) {
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(date);
//        calendar.add(Calendar.DAY_OF_MONTH, (13 - calendar.get(Calendar.DAY_OF_WEEK))%7 + 7*shift);
//        calendar.set(Calendar.HOUR_OF_DAY, 8);
//        calendar.set(Calendar.MINUTE, 0);
//        calendar.set(Calendar.SECOND, 0);
//        calendar.set(Calendar.MILLISECOND, 0);
//        return calendar.getTime();
//    }
//
//    public static Date calPeriodDate(Date srcDate, long period) {
//        Date destDate = millisToDate(new Double(Math.ceil(Math.ceil(srcDate.getTime()/1000.0)/period)).longValue()*period*1000);
//        return destDate;
//    }
//
//    public static Date calShiftPeriodDate(Date srcDate, long period, long shiftPeriod) {
//        Date destDate = millisToDate((new Double(Math.ceil(Math.ceil(srcDate.getTime()/1000.0)/period)).longValue() + shiftPeriod)*period*1000);
//        return destDate;
//    }
//
//    public static long calDiffPeriods(Date preDate, Date curDate, long period) {
//        if (preDate == null || curDate == null || preDate.getTime() > curDate.getTime()) {
//            return 0;
//        }
//        long curPeriods = new Double(Math.ceil(Math.ceil(curDate.getTime()/1000.0)/period)).longValue();
//        long prePeriods = new Double(Math.ceil(Math.ceil(preDate.getTime()/1000.0)/period)).longValue();
//        return  curPeriods - prePeriods;
//    }

}
