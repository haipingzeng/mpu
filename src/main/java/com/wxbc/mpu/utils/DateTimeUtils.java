package com.wxbc.mpu.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;

public class DateTimeUtils {

    public static DateTimeFormatter STD_FMT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS");

    public static String[] MONTHS = new String[]{"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};

    // "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

    public static LocalDateTime now() {
        return LocalDateTime.now();
    }

    public static LocalDateTime utcnow() {
        return LocalDateTime.now(ZoneOffset.UTC);
    }

    public static Long toMillis(LocalDateTime localDateTime) {
         return localDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    public static Long toMillis(LocalDateTime localDateTime, ZoneId zoneId) {
        return localDateTime.atZone(zoneId).toInstant().toEpochMilli();
    }

    public static Long toMillisOfUtc(LocalDateTime localDateTime) {
        return localDateTime.atZone(ZoneOffset.UTC).toInstant().toEpochMilli();
    }

    public static Long currentMillis() {
        return utcnow().atZone(ZoneOffset.UTC).toInstant().toEpochMilli();
    }

    public static Long currentMicros() {
        return utcnow().atZone(ZoneOffset.UTC).toInstant().toEpochMilli() * 1000L + now().getLong(ChronoField.MICRO_OF_SECOND) % 1000L;
    }

    public static Long currentNanos() {
        return utcnow().atZone(ZoneOffset.UTC).toInstant().toEpochMilli() * 1000000L + now().getLong(ChronoField.NANO_OF_SECOND) % 1000000L;
    }

    public static LocalDateTime millisToDateTime(Long milliseconds) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(milliseconds), ZoneId.systemDefault());
    }

    public static LocalDateTime millisToDateTime(Long milliseconds, ZoneId zoneId) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(milliseconds), zoneId);
    }

    public static LocalDateTime millisToUtcDateTime(Long milliseconds) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(milliseconds), ZoneOffset.UTC);
    }

    public static LocalDateTime microsToDateTime(Long microseconds) {
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(microseconds / 1000000L, microseconds % 1000000L * 1000), ZoneId.systemDefault());
    }

    public static LocalDateTime microsToDateTime(Long microseconds, ZoneId zoneId) {
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(microseconds / 1000000L, microseconds % 1000000L * 1000), zoneId);
    }

    public static LocalDateTime microsToUtcDateTime(Long microseconds) {
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(microseconds / 1000000L, microseconds % 1000000L * 1000), ZoneOffset.UTC);
    }


    // You could use a long/Long for a count of nanoseconds to represent moments for the next two centuries, up to 2262-04-11T23:47:16.854775807Z
    public static LocalDateTime nanosToDateTime(Long microseconds) {
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(microseconds / 1000000000L, microseconds % 1000000000L), ZoneId.systemDefault());
    }

    // You could use a long/Long for a count of nanoseconds to represent moments for the next two centuries, up to 2262-04-11T23:47:16.854775807Z
    public static LocalDateTime nanosToDateTime(Long microseconds, ZoneId zoneId) {
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(microseconds / 1000000000L, microseconds % 1000000000L), zoneId);
    }

    // You could use a long/Long for a count of nanoseconds to represent moments for the next two centuries, up to 2262-04-11T23:47:16.854775807Z
    public static LocalDateTime nanosToUtcDateTime(Long microseconds) {
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(microseconds / 1000000000L, microseconds % 1000000000L), ZoneOffset.UTC);
    }

    public static String fmtDate(LocalDateTime dateTime) {
        return dateTime.format(STD_FMT);
    }

    public static String fmtDate(LocalDateTime dateTime, String fmt) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(fmt);
        return dateTime.format(formatter);
    }

    public static LocalDateTime parseDate(String dateTime) {
        return LocalDateTime.parse(dateTime, STD_FMT);
    }

    public static LocalDateTime parseDate(String dateTime, String fmt) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(fmt);
        return LocalDateTime.parse(dateTime, formatter);
    }
}
