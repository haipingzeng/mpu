package com.wxbc.mpu.wss;

import com.wxbc.mpu.utils.DateTimeUtils;
import com.wxbc.mpu.utils.DateUtils;
import com.wxbc.mpu.utils.WxbcUtils;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.compression.ZlibCodecFactory;
import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.handler.codec.http.websocketx.extensions.WebSocketClientExtensionHandler;
import io.netty.handler.codec.http.websocketx.extensions.compression.DeflateFrameClientExtensionHandshaker;
import io.netty.handler.codec.http.websocketx.extensions.compression.PerMessageDeflateClientExtensionHandshaker;
import io.netty.handler.codec.http.websocketx.extensions.compression.WebSocketClientCompressionHandler;
import io.netty.handler.proxy.HttpProxyHandler;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.CharsetUtil;
import io.netty.util.internal.SocketUtils;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.*;

import static io.netty.handler.codec.http.websocketx.extensions.compression.PerMessageDeflateServerExtensionHandshaker.MAX_WINDOW_SIZE;

public class WssClient {

    private final static Logger logger = LoggerFactory.getLogger(WssClient.class);

    protected String wssClientName = UUID.randomUUID().toString();

    private String wssUrl;

    private  boolean allowClientNoContext = false;

    private boolean requestedServerNoContext = false;

    private EventLoopGroup group = new NioEventLoopGroup();

    private Channel channel = null;

    private ScheduledExecutorService eventloop = null; //Executors.newScheduledThreadPool(1);

    private LocalDateTime lastReconnectTime = null;

    private Boolean running = false;

    public WssClient() {
        eventloop = new ScheduledThreadPoolExecutor(1, new BasicThreadFactory.Builder().namingPattern(wssClientName + " WssClient").daemon(true).build());

    }

    public WssClient(String wssClientName) {
        this.wssClientName = wssClientName;
        eventloop = new ScheduledThreadPoolExecutor(1, new BasicThreadFactory.Builder().namingPattern(wssClientName + " WssClient").daemon(true).build());
    }

    public void setAllowClientNoContext(boolean allowClientNoContext) {
        this.allowClientNoContext = allowClientNoContext;
    }

    public void setRequestedServerNoContext(boolean requestedServerNoContext) {
        this.requestedServerNoContext = requestedServerNoContext;
    }

    protected void onOpen(Channel channel) {

    }

    protected void onMessage(Channel channel, String msg) {

    }

    protected void onMessage(Channel channel, byte[] msg) {

    }

    protected void onClose(Channel channel) {

    }

    protected void onThrowable(Throwable exception) {
        logger.error(wssClientName + " onThrowable: " + exception.toString());
    }

    private class WssClientChannelHandler extends SimpleChannelInboundHandler<Object> {

        private final WebSocketClientHandshaker handshaker;

        private ChannelPromise handshakeFuture;

        private StringBuilder frameBuffer = null;

        public WssClientChannelHandler(WebSocketClientHandshaker handshaker) {
            this.handshaker = handshaker;
        }

        public ChannelFuture handshakeFuture() {
            return handshakeFuture;
        }

        @Override
        public void handlerAdded(ChannelHandlerContext ctx) {
            handshakeFuture = ctx.newPromise();
        }

        @Override
        public void channelActive(ChannelHandlerContext ctx) {
            logger.info(wssClientName + " channelActive");
            handshaker.handshake(ctx.channel());
        }

        @Override
        public void channelInactive(ChannelHandlerContext ctx) {
            logger.info(wssClientName + " channelInactive");
            onClose(ctx.channel());
            ctx.channel().close();
            ctx.close();
        }

        @Override
        public void channelRead0(ChannelHandlerContext ctx, Object msg){
            try {
                Channel channel = ctx.channel();
                if (!handshaker.isHandshakeComplete()) {
                    handshaker.finishHandshake(channel, (FullHttpResponse) msg);
                    handshakeFuture.setSuccess();
                    onOpen(channel);
                    return;
                }

                if (msg instanceof FullHttpResponse) {
                    FullHttpResponse response = (FullHttpResponse) msg;
                    logger.warn(wssClientName + "Unexpected FullHttpResponse (getStatus=" + response.status() + ", content=" + response.content().toString(CharsetUtil.UTF_8) + ')');
                    return;
                }

                WebSocketFrame frame = (WebSocketFrame) msg;
                if (frame instanceof BinaryWebSocketFrame) {
                    byte[] content = new byte[frame.content().readableBytes()];
                    frame.content().getBytes(0, content);
                    onMessage(channel, content);
                } else if (frame instanceof TextWebSocketFrame) {
                    if (frame.isFinalFragment()) {
                        onMessage(channel, ((TextWebSocketFrame) frame).text());
                    } else {
                        frameBuffer = new StringBuilder();
                        frameBuffer.append(((TextWebSocketFrame) frame).text());
                    }
                } else if (frame instanceof ContinuationWebSocketFrame) {
                    if (frameBuffer != null) {
                        frameBuffer.append(((ContinuationWebSocketFrame) frame).text());
                        if (frame.isFinalFragment()) {
                            onMessage(channel, frameBuffer.toString());
                            frameBuffer = null;
                        }
                    }
                } else if (frame instanceof PingWebSocketFrame){
                    channel.writeAndFlush(new PongWebSocketFrame());
                } else if (frame instanceof CloseWebSocketFrame) {
                    System.out.println("WebSocket Client received closing");
                    ctx.channel().close();
                } else {
                    logger.error(wssClientName + " frame: " + frame.toString());
                }
            } catch (WebSocketHandshakeException e) {
                e.printStackTrace();
                logger.warn(wssClientName + " " + wssUrl + " failed to connect");
                ctx.channel().close();
                ctx.close();
                handshakeFuture.setFailure(e);
            } catch (Exception e) {
                logger.warn(wssClientName + " " + wssUrl + " ChannelHandler channelRead0 catch exception " + e.getMessage());
            }
        }

        @Override
        public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
            logger.error(wssClientName + " " + wssUrl + " ChannelHandler exceptionCaught " + cause.getMessage());
            onThrowable(cause);
            if (!handshakeFuture.isDone()) {
                handshakeFuture.setFailure(cause);
            }
            ctx.channel().close();
        }
    }

    public void reconnect() {

        logger.info(wssClientName + " " + wssUrl + " reconnect ");
        try {
            URI uri = new URI(System.getProperty("url", wssUrl));
            String scheme = uri.getScheme() == null ? "ws" : uri.getScheme();
            final String host = uri.getHost() == null ? "127.0.0.1" : uri.getHost();
            final int port;
            if (uri.getPort() == -1) {
                if ("ws".equalsIgnoreCase(scheme)) port = 80;
                else if ("wss".equalsIgnoreCase(scheme)) port = 443;
                else port = -1;
            } else port = uri.getPort();

            if (!"ws".equalsIgnoreCase(scheme) && !"wss".equalsIgnoreCase(scheme)) {
                return;
            }
            final boolean ssl = "wss".equalsIgnoreCase(scheme);
            final SslContext sslCtx;
            if (ssl)
                sslCtx = SslContextBuilder.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build();
            else sslCtx = null;

            final WssClientChannelHandler handler = new WssClientChannelHandler(WebSocketClientHandshakerFactory.newHandshaker(uri, WebSocketVersion.V13, null, true, new DefaultHttpHeaders(), 165536));
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 3000)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) {
                            ChannelPipeline p = ch.pipeline();
                            if (sslCtx != null) p.addLast(sslCtx.newHandler(ch.alloc(), host, port));
                            p.addLast(new IdleStateHandler(10, 10, 10, TimeUnit.SECONDS));
//                            p.addLast(new HttpProxyHandler(SocketUtils.socketAddress("127.0.0.1", 7890)));
                            if (allowClientNoContext) {
                                if (requestedServerNoContext) {
                                    p.addLast(new HttpClientCodec(), new HttpObjectAggregator(8192), WssCncSncCompressionHandler.INSTANCE, handler);
                                } else {
                                    p.addLast(new HttpClientCodec(), new HttpObjectAggregator(8192), WssCncCompressionHandler.INSTANCE, handler);
                                }
                            } else {
                                if (requestedServerNoContext) {

                                } else {
                                    p.addLast(new HttpClientCodec(), new HttpObjectAggregator(8192), WebSocketClientCompressionHandler.INSTANCE, handler);
                                }
                            }
                        }
                    });
            channel = bootstrap.connect(uri.getHost(), port).sync().channel();
            if (!running) {
                channel.close();
            } else {
                channel.closeFuture().sync();
            }
        } catch (Exception e) {
            onThrowable(e);
        } finally {
            if (running) {
                LocalDateTime utcnow = DateTimeUtils.utcnow();
                if (lastReconnectTime == null || (DateTimeUtils.toMillisOfUtc(utcnow) - DateTimeUtils.toMillisOfUtc(lastReconnectTime)) > 1000) {
                    lastReconnectTime = utcnow;
                    eventloop.schedule(() -> reconnect(), 0, TimeUnit.SECONDS);
                } else {
                    lastReconnectTime = utcnow;
                    eventloop.schedule(() -> reconnect(), 1, TimeUnit.SECONDS);
                }
            }
        }
    }

    public void connect(String wssUrl) {
        this.wssUrl = wssUrl;
        connect(0);
    }

    private void connect(int delay) {
        if (!running) {
            running = true;
            lastReconnectTime = DateTimeUtils.utcnow();
            eventloop.schedule(() -> reconnect(), delay, TimeUnit.SECONDS);
        }
    }

    public void disconnect() {
        running = false;
        if (channel != null) {
            channel.close();
        }
    }

    public void shutdownGracefully() {
        this.disconnect();
        group.shutdownGracefully();
        eventloop.shutdown();
        logger.info(wssClientName + " shutdown ");
    }

    public Boolean getRunning() {
        return running;
    }

    public void setRunning(Boolean running) {
        this.running = running;
    }
}
