package com.wxbc.mpu.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.wxbc.mpu")
@SpringBootApplication
public class MpuApplication {

    public static void main(String[] args) {
        SpringApplication.run(MpuApplication.class, args);
    }

}
