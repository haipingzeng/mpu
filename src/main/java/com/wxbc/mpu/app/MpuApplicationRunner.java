package com.wxbc.mpu.app;

import com.wxbc.mpu.market.MarketContext;
import com.wxbc.mpu.market.SymbolMgr;
import com.wxbc.mpu.utils.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

@Component
public class MpuApplicationRunner implements ApplicationRunner {

    private final static Logger logger = LoggerFactory.getLogger(MpuApplicationRunner.class);

    @Autowired
    MarketContext marketContext;

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    SymbolMgr symbolMgr;

    @Override
    public void run(ApplicationArguments args) {
        logger.info(">>>>>>>>>>>>>>>>>> MPU START UP!");
        marketContext.start();
    }
}
