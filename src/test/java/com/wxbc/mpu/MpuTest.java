package com.wxbc.mpu;

import com.wxbc.mpu.utils.DateTimeUtils;
import com.wxbc.mpu.utils.DateUtils;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.*;

public class MpuTest {

    @Test
    public void testSplit() {
        String symbol = "BTC_USD@P";
        String fields[] = symbol.split("_|@");
        System.out.println(fields[0]);
        System.out.println(fields[1]);
        System.out.println(fields[2]);
    }

    @Test
    public void testLastFriDayOfMonth() {
//        Date date = DateUtils.parseDate("2021-06-05", "yyyy-MM-dd");
//        Date fri = DateUtils.lastFridayOfMonth(date);
//        System.out.println(fri);
    }

    @Test
    public void testList() {
        List<String> strList = new ArrayList<>();
        strList.add("adsffa");
        strList.add("fjoajew");
        strList.add("ojfeiqfa");
        for (String str: strList) {
            strList.clear();
            return;
        }
        System.out.println("dfoajfajfodsf");
    }

    @Test
    public void testLong() {
        Long l1 = new Long("12");
        Long l2 = new Long("12");

        Map<Long, String> map = new HashMap<>();
        map.put(l1, "12");
        map.put(l2, "13");
        System.out.println((l1.equals(l2)));
        System.out.println((l1 == 12));
        System.out.println((l2 == 12));
        System.out.println((l1 == l2));
        System.out.println((l1.longValue() == l2.longValue()));
    }

    @Test
    public void testStringCompare() {
        String s1 = "4321.09";
        String s2 = "4321.090";
        System.out.println(s1.compareTo(s2));
    }

    @Test
    public void testLocalDateTime() {
        LocalDateTime d1 = DateTimeUtils.now();
        LocalDateTime d2 = DateTimeUtils.utcnow();
        System.out.println(d1.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        System.out.println(d2.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
    }
}
